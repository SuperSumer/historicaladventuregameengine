﻿using Microsoft.Xna.Framework;

namespace DataTypes
{
    public class ObjectPopulatedSceneData
    {
        /* -------- Public Fields -------- */
        public SceneObjectReference[] SceneObjectReferences;
        public SceneObjectData[] SceneObjects;
        public Color LightColor;
        public Color SkyColor;
        public Color GroundColor;
        public int HorizonY;
        public string CloudTextureIDOrPath;
        public int CloudMovementSpeed;
        public string HorizonTextureIDOrPath;
    }
}
