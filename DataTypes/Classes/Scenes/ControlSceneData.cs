﻿using Microsoft.Xna.Framework;

namespace DataTypes
{
    public class ControlSceneData
    {
        /* -------- Public Fields -------- */
        public string ControlMessage;
        public string NextSceneID;
        public Color BackgroundColor;
        public Color TextColor;
    }
}
