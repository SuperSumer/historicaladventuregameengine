﻿namespace DataTypes
{
    public class ConversationSceneData
    {
        /* -------- Public Fields -------- */
        public string ReturnSceneID;
        public float ForcedFocalDepth;
        public ConversationTreeData ConversationTree;
    }
}
