﻿using Microsoft.Xna.Framework;

namespace DataTypes
{
    public class SymbolDecodingSceneData
    {
        /* -------- Public Fields -------- */
        public string ReturnSceneID;
        public string BackgroundTextureIDOrPath;
        public string DecodeTextureIDOrPath;
        public Point DecodePosition;
        public DecodeSymbolData[] DecodeSymbols;
        public int ScorePerDecode;
    }
}
