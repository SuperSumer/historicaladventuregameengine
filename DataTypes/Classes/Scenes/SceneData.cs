﻿using System.Xml.Serialization;

namespace DataTypes
{
    public class SceneData
    {
        /* -------- Public Fields -------- */
        [XmlAttribute]
        public string ID;
        public string Type;
        public int Width;
        public int Height;
        public StageDirectionData[] ActivationStageDirections;
        public StageDirectionData[] DeactivationStageDirections;
        public string AmbientSoundEffectIDOrPath;
        // Inherited data
        public ControlSceneData ControlSceneData;
        public ObjectPopulatedSceneData ObjectPopulatedSceneData;
        public AdventureSceneData AdventureSceneData;
        public ConversationSceneData ConversationSceneData;
        public CutsceneData CutsceneData;
        public SymbolDecodingSceneData SymbolDecodingSceneData;
    }
}
