﻿using System.Xml.Serialization;
using Microsoft.Xna.Framework;

namespace DataTypes
{
    public class SceneObjectData
    {
        /* -------- Public Fields -------- */
        [XmlAttribute]
        public string ID;
        public string Type;
        public Point Position;
        public float Depth;
        public float Scale;
        public StageDirectionData[] ObjectStageDirections;
        public bool StartInvisible;
        public bool StartInactive;
        // Inherited data
        public StaticSceneObjectData StaticSceneObjectData;
        public AnimatedSceneObjectData AnimatedSceneObjectData;
        public IconSceneObjectData IconSceneObjectData;
        public PlayerCharacterSceneObjectData PlayerCharacterSceneObjectData;
    }
}
