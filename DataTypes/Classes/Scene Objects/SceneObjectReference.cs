﻿using System.Xml.Serialization;
using Microsoft.Xna.Framework;

namespace DataTypes
{
    public class SceneObjectReference
    {
        /* -------- Public Fields -------- */
        [XmlAttribute]
        public string ID;
        public Point Position;
        public float Depth;
        public float Scale;
    }
}
