﻿namespace DataTypes
{
    public class AnimatedSceneObjectData
    {
        /* -------- Public Fields -------- */
        public AnimationData Animation;
        public string AnimationID;
    }
}
