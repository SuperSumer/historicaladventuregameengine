﻿using System;

namespace DataTypes
{
    public class PlayerCharacterSceneObjectData
    {
        /* -------- Public Fields -------- */
        public string TextureIDOrPath;
        public string LastSceneID;
    }
}
