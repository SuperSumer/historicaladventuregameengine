﻿namespace DataTypes
{
    public class IconSceneObjectData
    {
        /* -------- Public Fields -------- */
        public string IconType;
        public StageDirectionData[] ActivationStageDirections;
    }
}
