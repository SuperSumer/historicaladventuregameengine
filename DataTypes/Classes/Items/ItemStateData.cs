﻿namespace DataTypes
{
    public class ItemStateData
    {
        /* -------- Public Fields -------- */
        public string ID;
        public string TextureIDOrPath;
    }
}
