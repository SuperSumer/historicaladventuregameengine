﻿using System.Xml.Serialization;

namespace DataTypes
{
    public class ItemData
    {
        /* -------- Public Fields -------- */
        [XmlAttribute]
        public string ID;
        public string Type;
        // Inherited data
        public StaticItemData StaticItemData;
        public MultiStateItemData MultiStateItemData;
        public NumericalItemData NumericalItemData;
        public TranslatorItemData TranslatorItemData;
        public ActivatableItemData ActivatableItemData;
    }
}
