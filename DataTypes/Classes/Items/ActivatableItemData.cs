﻿namespace DataTypes
{
    public class ActivatableItemData
    {
        /* -------- Public Fields -------- */
        public bool Activatable;
        public string ActivatableTextureIDOrPath;
        public string MouseOverTextureIDOrPath;
        public string NonActivatableTextureIDOrPath;
        public StageDirectionData[] StageDirections;
    }
}
