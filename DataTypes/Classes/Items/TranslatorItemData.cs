﻿namespace DataTypes
{
    public class TranslatorItemData
    {
        /* -------- Public Fields -------- */
        public string InactiveTextureIDOrPath;
        public string ActiveTextureIDOrPath;
    }
}
