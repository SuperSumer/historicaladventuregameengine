﻿namespace DataTypes
{
    public class NumericalItemStateData
    {
        /* -------- Public Fields -------- */
        public int Number;
        public string TextureIDOrPath;
    }
}
