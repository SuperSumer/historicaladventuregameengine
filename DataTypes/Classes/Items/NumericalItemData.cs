﻿namespace DataTypes
{
    public class NumericalItemData
    {
        /* -------- Public Fields -------- */
        public string DefaultTextureIDOrPath;
        public NumericalItemStateData[] States;
    }
}
