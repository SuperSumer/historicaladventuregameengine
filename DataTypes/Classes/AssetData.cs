﻿namespace DataTypes
{
    public class AssetData
    {
        /* -------- Public Fields -------- */
        public string ID;
        public string ContentPath;
    }
}
