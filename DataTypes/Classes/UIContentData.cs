﻿using Microsoft.Xna.Framework;

namespace DataTypes
{
    public class UIContentData
    {
        /* -------- Public Fields -------- */
        public string CursorTextureIDOrPath;
        public string InformationIconTextureIDOrPath;
        public string MoveIconTextureIDOrPath;
        public string PickUpIconTextureIDOrPath;
        public string TalkIconTextureIDOrPath;
        public string CuneiformIconTextureIDOrPath;
        public string InfoMessageTextureIDOrPath;
        public string InfoMessageSpriteFontPath;
        public Color InfoMessageTextColor;
        public int InfoMessageBoxWidth;
        public int InfoMessageBoxHeight;
        public string InfoNodeTextureIDOrPath;
        public string InfoNodeSpriteFontPath;
        public Color InfoNodeTextColor;
        public int InfoNodeBoxWidth;
        public int InfoNodeBoxHeight;
        public string IconActivateSoundEffectIDOrPath;
        public string RightChoiceSoundEffectIDOrPath;
        public string WrongChoiceSoundEffectIDOrPath;
        public string ScoreIconTextureIDOrPath;
        public string ScoreSpriteFontPath;
    }
}
