﻿namespace DataTypes
{
    public class DecodeSymbolData
    {
        /* -------- Public Fields -------- */
        public string Text;
        public string SymbolTextureIDOrPath;
        public string DecodeTextureIDOrPath;
    }
}
