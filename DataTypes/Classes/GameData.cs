﻿namespace DataTypes
{
    public class GameData
    {
        /* -------- Public Fields -------- */
        public string ID;
        public string[] TextureListFilenames;
        public string[] SoundEffectListFilenames;
        public ArtStyleData ArtStyle;
        public string[] AnimationListFilenames;
        public ItemData[] Items;
        public string[] InitialInventoryItemIDs;
        public string[] SceneObjectListFilenames;
        public string[] SceneListFilenames;
        public string InitialSceneID;
        public InfoNodeData[] InfoNodes;
        public int ScorePerInfoNode;
        public UIContentData UIContent;
    }
}
