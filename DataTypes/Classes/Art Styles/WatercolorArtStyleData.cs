﻿namespace DataTypes
{
    public class WatercolorArtStyleData
    {
        /* -------- Public Fields -------- */
        public float PaperSizeMultiplier;
        public float WobbleOffsetXMultiplier;
        public float WobbleOffsetYMultiplier;
        public float EdgeDarkeningMultiplier;
        public float NoiseMultiplier;
        public float PerlinMultiplier;
        public float AmbientOcclusionMultiplier;
        public float PaperAlpha;
        public float PaintAlpha;
    }
}
