﻿namespace DataTypes
{
    public class AnimationData
    {
        /* -------- Public Fields -------- */
        public string ID;
        public string PlayType;
        public AnimationFrameData[] Frames;
    }
}
