﻿namespace DataTypes
{
    public class AnimationFrameData
    {
        /* -------- Public Fields -------- */
        public string TextureIDOrPath;
        public float FrameTime;
    }
}
