﻿namespace DataTypes
{
    public class TimerData
    {
        /* -------- Public Fields -------- */
        public string ID;
        public float Duration;
        public string TextureIDOrPath;
        public StageDirectionData[] StageDirections;
    }
}
