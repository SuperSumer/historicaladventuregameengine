﻿namespace DataTypes
{
    public class SetGameFlagData
    {
        /* -------- Public Fields -------- */
        public string FlagID;
        public bool FlagValue;
    }
}
