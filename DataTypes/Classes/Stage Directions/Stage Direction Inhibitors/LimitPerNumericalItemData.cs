﻿namespace DataTypes
{
    public class LimitPerNumericalItemData
    {
        /* -------- Public Fields -------- */
        public string ItemID;
        public int MinNumber;
        public int MaxNumber;
    }
}
