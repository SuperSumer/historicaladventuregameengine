﻿namespace DataTypes
{
    public class StageDirectionInhibitorData
    {
        /* -------- Public Fields -------- */
        public string Type;
        // Inherited data
        public LimitPerGameFlagData LimitPerGameFlagData;
        public LimitStageDirectionActivationsData LimitStageDirectionActivationsData;
        public LimitPerNumericalItemData LimitPerNumericalItemData;
    }
}
