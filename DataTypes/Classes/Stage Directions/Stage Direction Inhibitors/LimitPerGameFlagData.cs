﻿namespace DataTypes
{
    public class LimitPerGameFlagData
    {
        /* -------- Public Fields -------- */
        public string FlagID;
        public bool RequiredFlagValue;
    }
}
