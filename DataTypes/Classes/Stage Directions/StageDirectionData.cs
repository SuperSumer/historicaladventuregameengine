﻿namespace DataTypes
{
    public class StageDirectionData
    {
        /* -------- Public Fields -------- */
        public string Type;
        public StageDirectionInhibitorData[] Inhibitors;
        // Inherited data
        public SetGameFlagData SetGameFlagData;
        public ShowInfoNodeData ShowInfoNodeData;
        public ShowInfoMessageData ShowInfoMessageData;
        public RequestSceneChangeData RequestSceneChangeData;
        public AlterItemData AlterItemData;
        public AddTimerData AddTimerData;
        public RemoveTimerData RemoveTimerData;
        public WaitForDurationData WaitForDurationData;
        public PlaySoundData PlaySoundData;
        public SetSceneObjectFlagData SetSceneObjectFlagData;
        public SetItemStateData SetItemStateData;
        public SetItemFlagData SetItemFlagData;
        public IncreaseScoreData IncreaseScoreData;
        public SetPlayerCharacterShowingData SetPlayerCharacterShowingData;
    }
}
