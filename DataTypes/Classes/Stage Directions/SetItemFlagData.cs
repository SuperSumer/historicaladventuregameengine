﻿namespace DataTypes
{
    public class SetItemFlagData
    {
        /* -------- Public Fields -------- */
        public string ItemID;
        public bool Flag;
    }
}
