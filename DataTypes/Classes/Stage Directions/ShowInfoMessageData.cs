﻿namespace DataTypes
{
    public class ShowInfoMessageData
    {
        /* -------- Public Fields -------- */
        public string InfoMessage;
        public string TextureIDOrPath;
    }
}
