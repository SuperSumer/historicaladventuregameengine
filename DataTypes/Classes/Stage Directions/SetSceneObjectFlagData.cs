﻿namespace DataTypes
{
    public class SetSceneObjectFlagData
    {
        /* -------- Public Fields -------- */
        public string SceneID;
        public string SceneObjectID;
        public bool Flag;
    }
}
