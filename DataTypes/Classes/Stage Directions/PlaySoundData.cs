﻿namespace DataTypes
{
    public class PlaySoundData
    {
        /* -------- Public Fields -------- */
        public string SoundEffectIDOrPath;
        public string SceneID;
    }
}
