﻿namespace DataTypes
{
    public class SetItemStateData
    {
        /* -------- Public Fields -------- */
        public string ItemID;
        public string StateID;
    }
}
