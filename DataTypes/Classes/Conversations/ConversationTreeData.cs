﻿namespace DataTypes
{
    public class ConversationTreeData
    {
        /* -------- Public Fields -------- */
        public string Name;
        public NPCSpeechBranchData[] NPCBranches;
    }
}
