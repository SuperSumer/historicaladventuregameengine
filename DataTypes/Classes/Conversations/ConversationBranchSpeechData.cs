﻿namespace DataTypes
{
    public class ConversationBranchSpeechData
    {
        /* -------- Public Fields -------- */
        public string Title;
        public string Text;
        public string SoundEffectIDOrPath;
    }
}
