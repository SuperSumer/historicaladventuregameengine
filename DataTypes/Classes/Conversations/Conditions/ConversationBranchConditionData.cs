﻿namespace DataTypes
{
    public class ConversationBranchConditionData
    {
        /* -------- Public Fields -------- */
        public string Type;
        public bool Reversed;
        // Inherited data
        public NumericalItemIsAtValueData NumericalItemIsAtValueData;
        public FlagIsAtValueData FlagIsAtValueData;
    }
}
