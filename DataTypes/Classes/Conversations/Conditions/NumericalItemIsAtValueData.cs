﻿namespace DataTypes
{
    public class NumericalItemIsAtValueData
    {
        /* -------- Public Fields -------- */
        public string ItemID;
        public int Value;
    }
}
