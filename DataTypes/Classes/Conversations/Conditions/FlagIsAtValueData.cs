﻿namespace DataTypes
{
    public class FlagIsAtValueData
    {
        /* -------- Public Fields -------- */
        public string FlagID;
        public bool Value;
    }
}
