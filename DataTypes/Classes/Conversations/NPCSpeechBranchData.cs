﻿namespace DataTypes
{
    public class NPCSpeechBranchData
    {
        /* -------- Public Fields -------- */
        public ConversationBranchSpeechData[] Speeches;
        public ConversationBranchConditionData[] Conditions;
        public StageDirectionData[] ActivationStageDirections;
        public PlayerSpeechBranchData[] PlayerBranches;
    }
}
