﻿namespace DataTypes
{
    public class PlayerSpeechBranchData
    {
        /* -------- Public Fields -------- */
        public ConversationBranchSpeechData[] Speeches;
        public ConversationBranchConditionData[] Conditions;
        public StageDirectionData[] ActivationStageDirections;
        public NPCSpeechBranchData[] NPCBranches;
    }
}
