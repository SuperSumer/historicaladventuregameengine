﻿namespace DataTypes
{
    public class InfoNodeData
    {
        /* -------- Public Fields -------- */
        public string ID;
        public string Title;
        public string Body;
    }
}
