﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D xDepthMap;
float xFocalDepth;

SamplerState textureSampler
{
	Filter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

struct VertexShaderInput
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

	output.Position = input.Position;
	output.TexCoord = input.TexCoord;

	return output;
}

float4 LinearFocusPS(VertexShaderOutput input) : COLOR
{
	float4 depthSample = xDepthMap.Sample(textureSampler, input.TexCoord);
	float depth = depthSample.r;

	// Linear focus
	float focus = abs(depth - xFocalDepth);

	return float4(focus, focus, focus, 1.0f);
}

technique LinearFocus
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL LinearFocusPS();
	}
};