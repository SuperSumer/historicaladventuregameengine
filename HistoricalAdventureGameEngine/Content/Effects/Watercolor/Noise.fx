﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

#define NOISE_MEAN				0.5f
#define PERLIN_MEAN				0.5f
#define AMBIENT_OCCLUSION_MEAN	0.5f

Texture2D xTexture;
Texture2D xNoiseMap;
Texture2D xPerlinMap;
Texture2D xAmbientOcclusionMap;
float xNoiseMultiplier;
float xPerlinMultiplier;
float xAmbientOcclusionMultiplier;

SamplerState TextureSampler
{
	Filter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
};

struct VertexShaderInput
{
	float4 Position : SV_POSITION;
	float2 TexCoord1 : TEXCOORD0;
	float2 TexCoord2 : TEXCOORD1;
	float2 TexCoord3 : TEXCOORD2;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float2 TexCoord1 : TEXCOORD0;
	float2 TexCoord2 : TEXCOORD1;
	float2 TexCoord3 : TEXCOORD2;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

	output.Position = input.Position;
	output.TexCoord1 = input.TexCoord1;
	output.TexCoord2 = input.TexCoord2;
	output.TexCoord3 = input.TexCoord3;

	return output;
}

float4 NoisePS(VertexShaderOutput input) : COLOR
{
	float4 textureSample = xTexture.Sample(TextureSampler, input.TexCoord1);
	float4 noiseSample = xNoiseMap.Sample(TextureSampler, input.TexCoord2);
	float4 perlinSample = xPerlinMap.Sample(TextureSampler, input.TexCoord2);
	float2 tex2 = input.TexCoord3;
	tex2.xy = tex2.xy % 1.0f;
	float4 ambientOcclusionSample = xAmbientOcclusionMap.Sample(TextureSampler, tex2);

	float noiseDensity = (noiseSample.r - NOISE_MEAN) * xNoiseMultiplier;
	float perlinDensity = (perlinSample.r - PERLIN_MEAN) * xPerlinMultiplier;
	float ambOccDensity = (AMBIENT_OCCLUSION_MEAN - ambientOcclusionSample.r) * xAmbientOcclusionMultiplier;
	float density = 1.0f + noiseDensity + perlinDensity + ambOccDensity;

	float4 col = textureSample;
	col.rgb = col.rgb - (col.rgb - col.rgb * col.rgb) * (density - 1.0f);

	return col;
}

technique Noise
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL NoisePS();
	}
};