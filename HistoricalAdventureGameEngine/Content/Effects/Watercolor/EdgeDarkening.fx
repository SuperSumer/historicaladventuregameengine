﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D xTexture;
float xEdgeDarkenMultiplier;
int xWidth;
int xHeight;

SamplerState TextureSampler
{
	Filter = Point;
	AddressU = Clamp;
	AddressV = Clamp;
};

struct VertexShaderInput
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

	output.Position = input.Position;
	output.TexCoord = input.TexCoord;

	return output;
}

float4 EdgeDarkeningPS(VertexShaderOutput input) : COLOR
{
	float uOffset = 1.0f / (float)xWidth;
	float vOffset = 1.0f / (float)xHeight;

	float4 centerSample = xTexture.Sample(TextureSampler, input.TexCoord);
	float4 upSample = xTexture.Sample(TextureSampler, input.TexCoord + float2(0.0f, vOffset));
	float4 downSample = xTexture.Sample(TextureSampler, input.TexCoord - float2(0.0f, vOffset));
	float4 leftSample = xTexture.Sample(TextureSampler, input.TexCoord - float2(uOffset, 0.0f));
	float4 rightSample = xTexture.Sample(TextureSampler, input.TexCoord + float2(uOffset, 0.0f));

	float3 gradVec;
	gradVec.rgb = abs(upSample.rgb - downSample.rgb) + abs(leftSample.rgb - rightSample.rgb);
	float grad = (gradVec.r + gradVec.g + gradVec.b) / 3.0f;

	float density = 1.0f + grad * xEdgeDarkenMultiplier;
	float4 col = centerSample;
	col.rgb = col.rgb - (col.rgb - col.rgb * col.rgb) * (density - 1.0f);

	return col;
}

technique EdgeDarkening
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL EdgeDarkeningPS();
	}
};