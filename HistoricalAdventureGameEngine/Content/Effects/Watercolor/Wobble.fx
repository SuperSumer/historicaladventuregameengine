﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D xTexture;
Texture2D xNormalMap;
float xOffsetXMultiplier;
float xOffsetYMultiplier;

SamplerState TextureSampler
{
	Filter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
};

struct VertexShaderInput
{
	float4 Position : SV_POSITION;
	float2 TexCoord1 : TEXCOORD0;
	float2 TexCoord2 : TEXCOORD1;
	float2 TexCoord3 : TEXCOORD2;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float2 TexCoord1 : TEXCOORD0;
	float2 TexCoord2 : TEXCOORD1;
	float2 TexCoord3 : TEXCOORD2;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

	output.Position = input.Position;
	output.TexCoord1 = input.TexCoord1;
	output.TexCoord2 = input.TexCoord2;
	output.TexCoord3 = input.TexCoord3;

	return output;
}

float4 WobblePS(VertexShaderOutput input) : COLOR
{
	float2 tex2 = input.TexCoord3;
	tex2.xy = tex2.xy % 1.0f;
	float4 normalSample = xNormalMap.Sample(TextureSampler, tex2);
	float2 offset = float2(normalSample.r, normalSample.g);
	offset.xy = offset.xy * 2.0f - 1.0f;
	offset.x *= -xOffsetXMultiplier;
	offset.y *= -xOffsetYMultiplier;

	float4 colorSample = xTexture.Sample(TextureSampler, input.TexCoord1 + offset);

	return colorSample;
}

technique Wobble
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL WobblePS();
	}
};