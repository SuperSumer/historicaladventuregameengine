﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D xTexture;
Texture2D xMask;
Texture2D xVignette;
float xThreshold;

SamplerState TextureSampler
{
	Filter = Point;
	AddressU = Clamp;
	AddressV = Clamp;
};

struct VertexShaderInput
{
	float4 Position : SV_POSITION;
	float2 TexCoord1 : TEXCOORD0;
	float2 TexCoord2 : TEXCOORD1;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float2 TexCoord1 : TEXCOORD0;
	float2 TexCoord2 : TEXCOORD1;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

	output.Position = input.Position;
	output.TexCoord1 = input.TexCoord1;
	output.TexCoord2 = input.TexCoord2;

	return output;
}

float4 MaskPS(VertexShaderOutput input) : COLOR
{
	float mask = xMask.Sample(TextureSampler, input.TexCoord2).r;
	float vignette = xVignette.Sample(TextureSampler, input.TexCoord2).r;
	mask *= vignette;

	float4 col = xTexture.Sample(TextureSampler, input.TexCoord1);

	return (mask >= xThreshold) ? col : float4(0.0f, 0.0f, 0.0f, 0.0f);
}

technique Mask
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL MaskPS();
	}
};