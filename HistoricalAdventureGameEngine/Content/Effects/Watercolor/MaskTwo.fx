﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D xTexture1;
Texture2D xTexture2;
Texture2D xMask;
Texture2D xVignette;
float xThreshold;

SamplerState TextureSampler
{
	Filter = Point;
	AddressU = Clamp;
	AddressV = Clamp;
};

struct VertexShaderInput
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

	output.Position = input.Position;
	output.TexCoord = input.TexCoord;

	return output;
}

float4 MaskTwoPS(VertexShaderOutput input) : COLOR
{
	float mask = xMask.Sample(TextureSampler, input.TexCoord).r;
	float vignette = xVignette.Sample(TextureSampler, input.TexCoord).r;
	mask *= vignette;

	float4 col1 = xTexture1.Sample(TextureSampler, input.TexCoord);
	float4 col2 = xTexture2.Sample(TextureSampler, input.TexCoord);

	return (mask >= xThreshold) ? col2 : col1;
}

technique MaskTwo
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL MaskTwoPS();
	}
};