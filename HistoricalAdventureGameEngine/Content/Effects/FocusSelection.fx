﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D xFrame;
Texture2D xBlurFrame;
Texture2D xFocusMap;
float xBlurMultiplier;

SamplerState textureSampler
{
	Filter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

struct VertexShaderInput
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

	output.Position = input.Position;
	output.TexCoord = input.TexCoord;

	return output;
}

float4 FocusBlurPS(VertexShaderOutput input) : COLOR
{
	float4 frameSample = xFrame.Sample(textureSampler, input.TexCoord);
	float4 blurFrameSample = xBlurFrame.Sample(textureSampler, input.TexCoord);
	float4 focusSample = xFocusMap.Sample(textureSampler, input.TexCoord);

	float blur = focusSample.r * xBlurMultiplier;

	float4 output;
	if (blur >= 1.0f)
		output = blurFrameSample;
	else
		output = blur * blurFrameSample + (1.0f - blur) * frameSample;

	return output;
}

technique FocusBlur
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL FocusBlurPS();
	}
};