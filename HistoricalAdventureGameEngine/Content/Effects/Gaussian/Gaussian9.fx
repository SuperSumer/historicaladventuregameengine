﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

// Gaussian values
#define GAUSS_VAL_0 0.243496135f
#define GAUSS_VAL_1 0.202531264f
#define GAUSS_VAL_2 0.116544578f
#define GAUSS_VAL_3 0.046397232f
#define GAUSS_VAL_4 0.012778859f

Texture2D xTexture;
int xWidth;
int xHeight;

SamplerState textureSampler
{
	Filter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

struct VertexShaderInput
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

	output.Position = input.Position;
	output.TexCoord = input.TexCoord;

	return output;
}

float4 HorizontalPS(VertexShaderOutput input) : COLOR
{
	float uOffset = 1.0f / (float)xWidth;

	float4 sampleNeg4 = xTexture.Sample(textureSampler, float2(input.TexCoord.x - 4 * uOffset, input.TexCoord.y));
	float4 sampleNeg3 = xTexture.Sample(textureSampler, float2(input.TexCoord.x - 3 * uOffset, input.TexCoord.y));
	float4 sampleNeg2 = xTexture.Sample(textureSampler, float2(input.TexCoord.x - 2 * uOffset, input.TexCoord.y));
	float4 sampleNeg1 = xTexture.Sample(textureSampler, float2(input.TexCoord.x - 1 * uOffset, input.TexCoord.y));
	float4 sample0 = xTexture.Sample(textureSampler, input.TexCoord);
	float4 samplePos1 = xTexture.Sample(textureSampler, float2(input.TexCoord.x + 1 * uOffset, input.TexCoord.y));
	float4 samplePos2 = xTexture.Sample(textureSampler, float2(input.TexCoord.x + 2 * uOffset, input.TexCoord.y));
	float4 samplePos3 = xTexture.Sample(textureSampler, float2(input.TexCoord.x + 3 * uOffset, input.TexCoord.y));
	float4 samplePos4 = xTexture.Sample(textureSampler, float2(input.TexCoord.x + 4 * uOffset, input.TexCoord.y));

	float4 output =
		sampleNeg4 * GAUSS_VAL_4 +
		sampleNeg3 * GAUSS_VAL_3 +
		sampleNeg2 * GAUSS_VAL_2 +
		sampleNeg1 * GAUSS_VAL_1 +
		sample0 * GAUSS_VAL_0 +
		samplePos1 * GAUSS_VAL_1 +
		samplePos2 * GAUSS_VAL_2 +
		samplePos3 * GAUSS_VAL_3 +
		samplePos4 * GAUSS_VAL_4;

	return output;
}

float4 VerticalPS(VertexShaderOutput input) : COLOR
{
	float vOffset = 1.0f / (float)xHeight;

	float4 sampleNeg4 = xTexture.Sample(textureSampler, float2(input.TexCoord.x, input.TexCoord.y - 4 * vOffset));
	float4 sampleNeg3 = xTexture.Sample(textureSampler, float2(input.TexCoord.x, input.TexCoord.y - 3 * vOffset));
	float4 sampleNeg2 = xTexture.Sample(textureSampler, float2(input.TexCoord.x, input.TexCoord.y - 2 * vOffset));
	float4 sampleNeg1 = xTexture.Sample(textureSampler, float2(input.TexCoord.x, input.TexCoord.y - 1 * vOffset));
	float4 sample0 = xTexture.Sample(textureSampler, input.TexCoord);
	float4 samplePos1 = xTexture.Sample(textureSampler, float2(input.TexCoord.x, input.TexCoord.y + 1 * vOffset));
	float4 samplePos2 = xTexture.Sample(textureSampler, float2(input.TexCoord.x, input.TexCoord.y + 2 * vOffset));
	float4 samplePos3 = xTexture.Sample(textureSampler, float2(input.TexCoord.x, input.TexCoord.y + 3 * vOffset));
	float4 samplePos4 = xTexture.Sample(textureSampler, float2(input.TexCoord.x, input.TexCoord.y + 4 * vOffset));

	float4 output =
	sampleNeg4 * GAUSS_VAL_4 +
	sampleNeg3 * GAUSS_VAL_3 +
	sampleNeg2 * GAUSS_VAL_2 +
	sampleNeg1 * GAUSS_VAL_1 +
	sample0 * GAUSS_VAL_0 +
	samplePos1 * GAUSS_VAL_1 +
	samplePos2 * GAUSS_VAL_2 +
	samplePos3 * GAUSS_VAL_3 +
	samplePos4 * GAUSS_VAL_4;

	return output;
}

technique Horizontal
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL HorizontalPS();
	}
};

technique Vertical
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL VerticalPS();
	}
};