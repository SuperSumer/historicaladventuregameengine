﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    static class Debug
    {
        /* -------- Private Fields -------- */
        private static List<DebugMessage> messages = new List<DebugMessage>();
        private static int messageIdx = 0;
        private static bool showingDebug = false;
        private static Texture2D pixTex;
        private static SpriteFont spriteFont;

        /* -------- Constants -------- */
        private const int MAX_MESSAGES = 100;
        private const int MESSAGES_VIEWED_AT_ONCE = 10;
        private const int BAR_SIZE = 15;
        private const int MESSAGE_HEIGHT = 14;
        private const int MESSAGE_GAP = 2;

        /* -------- Public Methods -------- */
        public static void Initialize()
        {
            pixTex = ContentLoader.LoadTexture("Textures/WhitePixel");
            spriteFont = ContentLoader.LoadSpriteFont("Sprite Fonts/Arial12");
        }

        public static void Update(InputComponent i)
        {
            // Showing the debug
            if (i.KeyboardButtonIsPressing(Keys.Insert))
                showingDebug = !showingDebug;

            // Clearing the debug
            if (i.KeyboardButtonIsPressing(Keys.Delete))
            {
                messages.Clear();
                messageIdx = 0;
            }

            // Scrolling the debug
            if (i.KeyboardButtonIsPressing(Keys.PageUp))
                messageIdx = Mathf.ClampLower(messageIdx - MESSAGES_VIEWED_AT_ONCE, 0);
            if (i.KeyboardButtonIsPressing(Keys.PageDown))
                messageIdx = Mathf.Clamp(messageIdx + MESSAGES_VIEWED_AT_ONCE, 0, messages.Count - MESSAGES_VIEWED_AT_ONCE);
            if (i.KeyboardButtonIsPressing(Keys.Home))
                messageIdx = 0;
            if (i.KeyboardButtonIsPressing(Keys.End))
                messageIdx = Mathf.ClampLower(messages.Count - MESSAGES_VIEWED_AT_ONCE, 0);
        }
        public static void Draw(ComponentContainer cc)
        {
            if (!cc.SettingsComponent.ShowDebug || !showingDebug)
                return;

            cc.GraphicsComponent.GraphicsDevice.SetRenderTarget(null);
            SpriteBatch sb = cc.GraphicsComponent.SpriteBatch;
            sb.Begin();
            SBDrawBox(sb, cc.GraphicsComponent);
            SBDrawMessages(sb);
            SBDrawBar(sb, cc.GraphicsComponent);
            sb.End();
        }

        public static void Log(string msg)
        {
            LogMessage(msg, eDebugMessageType.LOG);
        }
        public static void LogWarning(string msg)
        {
            LogMessage(msg, eDebugMessageType.WARNING);
            showingDebug = true;
        }
        public static void LogError(string msg)
        {
            LogMessage(msg, eDebugMessageType.ERROR);
            showingDebug = true;
        }

        /* -------- Private Methods -------- */
        private static void LogMessage(string msg, eDebugMessageType type)
        {
            messages.Add(new DebugMessage(msg, type));
            if (messages.Count > MAX_MESSAGES)
                messages.RemoveAt(0);
            if (messages.Count > MESSAGES_VIEWED_AT_ONCE)
                messageIdx++;
        }

        private static void SBDrawBox(SpriteBatch sb, GraphicsComponent g)
        {
            int rectHeight = MESSAGE_HEIGHT * MESSAGES_VIEWED_AT_ONCE + MESSAGE_GAP * (MESSAGES_VIEWED_AT_ONCE + 1);
            int rectWidth = g.ScreenWidth;

            Rectangle rect = new Rectangle(0, 0, rectWidth, rectHeight);
            sb.Draw(pixTex, rect, Color.Black);
        }
        private static void SBDrawMessages(SpriteBatch sb)
        {
            int x = MESSAGE_GAP;
            int y = MESSAGE_GAP;

            int endIdx = Mathf.ClampUpper(messageIdx + MESSAGES_VIEWED_AT_ONCE, messages.Count - 1);
            for (int i = messageIdx; i <= endIdx; i++)
            {
                DebugMessage msg = messages[i];
                Vector2 pos = new Vector2(x, y);
                Color col = Color.White;
                if (msg.Type == eDebugMessageType.WARNING)
                    col = Color.Yellow;
                if (msg.Type == eDebugMessageType.ERROR)
                    col = Color.Red;

                sb.DrawString(spriteFont, msg.Text, pos, col);

                y += MESSAGE_HEIGHT + MESSAGE_GAP;
            }
        }
        private static void SBDrawBar(SpriteBatch sb, GraphicsComponent g)
        {
            int panelHeight = MESSAGE_HEIGHT * MESSAGES_VIEWED_AT_ONCE + MESSAGE_GAP * (MESSAGES_VIEWED_AT_ONCE + 1);
            int rectX = g.ScreenWidth - BAR_SIZE;

            if (messages.Count <= 1)
            {
                sb.Draw(pixTex, new Rectangle(rectX, 0, BAR_SIZE, panelHeight), Color.Gray);
                return;
            }

            int maxSeeableIdx = Mathf.ClampUpper(messageIdx + MESSAGES_VIEWED_AT_ONCE, messages.Count - 1);
            int maxIdx = messages.Count - 1;

            int yTop = Mathf.Round(panelHeight * (float)messageIdx / maxIdx);
            int yBottom = Mathf.Round(panelHeight * (float)maxSeeableIdx / maxIdx);

            Rectangle rect = new Rectangle(rectX, yTop, BAR_SIZE, yBottom - yTop);
            sb.Draw(pixTex, rect, Color.Gray);
        }
    }
}
