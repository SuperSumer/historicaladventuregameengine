﻿namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eDebugMessageType
    {
        LOG,
        WARNING,
        ERROR
    }

    class DebugMessage
    {
        /* -------- Properties -------- */
        public string Text { get; private set; }
        public eDebugMessageType Type { get; private set; }

        /* -------- Constructors -------- */
        public DebugMessage(string inText, eDebugMessageType inType)
        {
            Text = inText;
            Type = inType;
        }
    }
}
