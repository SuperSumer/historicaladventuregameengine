﻿using System;

namespace HistoricalAdventureGameEngine
{
#if WINDOWS || LINUX
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            Logging.DisplayParticipantIDMessageBox();

            using (var game = new HistoricalAdventureGameEngine())
            {
#if DEBUG
                game.Run();
#else
                try
                {
                    game.Run();
                }
                catch (Exception e)
                {
                    FatalErrorHandler.ActivateFatalError(e.Message);
                }
#endif
            }

            FatalErrorHandler.OnProgramTerminated();
        }
    }
#endif
}
