﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace HistoricalAdventureGameEngine
{
    public class HistoricalAdventureGameEngine : Game
    {
        /* -------- Private Fields -------- */
        private GraphicsDeviceManager graphics;
        private ComponentContainer componentContainer;

        /* -------- Constructors -------- */
        public HistoricalAdventureGameEngine()
        {
            IsFixedTimeStep = false;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            componentContainer = new ComponentContainer(this);
        }
        ~HistoricalAdventureGameEngine()
        {
            Content.Dispose();
        }

        /* -------- Public Methods -------- */
        public void ExitGame()
        {
            Logging.SaveLog();
            Exit();
        }

        /* -------- Private Methods -------- */
        protected override void Initialize()
        {
            // Initialize utils
            ContentLoader.Initialize(Content);
            GraphicsUtil.Initialize(GraphicsDevice);

            // Initialize debug
            Debug.Initialize();
            Debug.Log($"Starting Historical Adventure Game Engine at {DateTime.Now.ToShortTimeString()} on {DateTime.Now.ToShortDateString()}");

            componentContainer.Initialize(GraphicsDevice);
            
            base.Initialize();
        }
        protected override void LoadContent()
        {
            ContentLoader.LoadContent();

            componentContainer.LoadContent();

            // Apply resolution
            if (componentContainer.SettingsComponent.ScreenWidth > 0 && componentContainer.SettingsComponent.ScreenHeight > 0)
            {
                graphics.PreferredBackBufferWidth = componentContainer.SettingsComponent.ScreenWidth;
                graphics.PreferredBackBufferHeight = componentContainer.SettingsComponent.ScreenHeight;
                graphics.IsFullScreen = componentContainer.SettingsComponent.FullScreen;
                graphics.ApplyChanges();
            }
        }
        protected override void Update(GameTime gameTime)
        {
            if (FatalErrorHandler.FatalErrorEncountered)
                ExitGame();
            else
            {
                GameTimeWrapper gtw = new GameTimeWrapper(gameTime);
                componentContainer.Update(gtw);
                Debug.Update(componentContainer.InputComponent);

                if (componentContainer.InputComponent.KeyboardCombinationIsPressing(Keys.LeftControl, Keys.Q))
                    ExitGame();
            }
            
            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            componentContainer.Draw();
            Debug.Draw(componentContainer);
            
            base.Draw(gameTime);
        }
    }
}
