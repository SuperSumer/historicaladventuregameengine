﻿using System.Windows.Forms;

namespace HistoricalAdventureGameEngine
{
    static class FatalErrorHandler
    {
        /* -------- Properties -------- */
        public static bool FatalErrorEncountered { get; private set; }

        /* -------- Private Fields -------- */
        private static string errorMessage;

        /* -------- Public Methods -------- */
        public static void ActivateFatalError(string message)
        {
            FatalErrorEncountered = true;
            errorMessage = message;
        }
        public static void OnProgramTerminated()
        {
            if (FatalErrorEncountered && errorMessage != null)
                MessageBox.Show($"Fatal Error Encountered!\nError Details: \"{errorMessage}\"");
        }
    }
}
