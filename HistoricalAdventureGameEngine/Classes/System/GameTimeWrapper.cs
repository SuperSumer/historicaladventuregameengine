﻿using Microsoft.Xna.Framework;

namespace HistoricalAdventureGameEngine
{
    class GameTimeWrapper
    {
        /* -------- Properties -------- */
        public float ElapsedSeconds { get; private set; }

        /* -------- Constructors -------- */
        public GameTimeWrapper(GameTime gt)
        {
            ElapsedSeconds = (float)gt.ElapsedGameTime.TotalSeconds;
        }
    }
}
