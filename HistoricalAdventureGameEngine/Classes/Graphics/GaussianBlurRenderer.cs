﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class GaussianBlurRenderer
    {
        /* -------- Properties -------- */
        public RenderTarget2D Output { get; private set; }

        /* -------- Private Fields -------- */
        private int numRenderTargets;
        private RenderTarget2D[] primaryRenderTargets;
        private RenderTarget2D[] secondaryRenderTargets;
        private Effect gaussianEffect;

        /* -------- Constructors -------- */
        public GaussianBlurRenderer(int width, int height, GraphicsDevice device, GaussianBlurSettings settings)
        {
            // Create render targets
            Output = new RenderTarget2D(device, width, height);
            numRenderTargets = settings.NumDownsamples + 1;
            primaryRenderTargets = new RenderTarget2D[numRenderTargets];
            secondaryRenderTargets = new RenderTarget2D[numRenderTargets];
            int thisWidth = width;
            int thisHeight = height;
            for (int i = 0; i < numRenderTargets; i++)
            {
                primaryRenderTargets[i] = new RenderTarget2D(device, thisWidth, thisHeight);
                secondaryRenderTargets[i] = new RenderTarget2D(device, thisWidth, thisHeight);
                thisWidth /= 2;
                thisHeight /= 2;
            }

            // Load effect
            switch (settings.KernelSize)
            {
                case 3:
                    gaussianEffect = ContentLoader.LoadEffect("Effects/Gaussian/Gaussian3");
                    break;
                case 5:
                    gaussianEffect = ContentLoader.LoadEffect("Effects/Gaussian/Gaussian5");
                    break;
                case 7:
                    gaussianEffect = ContentLoader.LoadEffect("Effects/Gaussian/Gaussian7");
                    break;
                case 9:
                    gaussianEffect = ContentLoader.LoadEffect("Effects/Gaussian/Gaussian9");
                    break;
                default:
                    FatalErrorHandler.ActivateFatalError($"Invalid Gaussian kernel size {settings.KernelSize} selected");
                    break;
            }
        }
        ~GaussianBlurRenderer()
        {
            for (int i = 0; i < numRenderTargets; i++)
            {
                primaryRenderTargets[i].Dispose();
                secondaryRenderTargets[i].Dispose();
            }
            Output.Dispose();
        }

        /* -------- Public Methods -------- */
        public void Render(Texture2D input, GraphicsDevice device, SpriteBatch sb)
        {
            DrawInputIntoRTs(input, device, sb);
            PerformHorizontalPass(device);
            PerformVerticalPass(device);
            DrawToOutput(device, sb);
        }

        /* -------- Private Methods -------- */
        private void DrawInputIntoRTs(Texture2D input, GraphicsDevice device, SpriteBatch sb)
        {
            for (int i = 0; i < numRenderTargets; i++)
            {
                RenderTarget2D rt = primaryRenderTargets[i];
                device.SetRenderTarget(rt);
                device.Clear(Color.Transparent);
                sb.Begin();
                sb.Draw(input, new Rectangle(0, 0, rt.Width, rt.Height), Color.White);
                sb.End();
            }
        }
        private void PerformHorizontalPass(GraphicsDevice device)
        {
            gaussianEffect.CurrentTechnique = gaussianEffect.Techniques["Horizontal"];

            for (int i = 0; i < numRenderTargets; i++)
            {
                Texture2D texture = primaryRenderTargets[i];
                RenderTarget2D rt = secondaryRenderTargets[i];
                gaussianEffect.Parameters["xTexture"].SetValue(texture);
                gaussianEffect.Parameters["xWidth"].SetValue(texture.Width);
                gaussianEffect.Parameters["xHeight"].SetValue(texture.Height);

                device.SetRenderTarget(rt);
                device.SetVertexBuffer(GraphicsUtil.FullTargetVertexBuffer);
                device.Indices = GraphicsUtil.FullTargetIndexBuffer;
                device.BlendState = BlendState.AlphaBlend;
                device.Clear(Color.Transparent);

                foreach (EffectPass pass in gaussianEffect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 2);
                }
            }
        }
        private void PerformVerticalPass(GraphicsDevice device)
        {
            gaussianEffect.CurrentTechnique = gaussianEffect.Techniques["Vertical"];

            for (int i = 0; i < numRenderTargets; i++)
            {
                Texture2D texture = secondaryRenderTargets[i];
                RenderTarget2D rt = primaryRenderTargets[i];
                gaussianEffect.Parameters["xTexture"].SetValue(texture);
                gaussianEffect.Parameters["xWidth"].SetValue(texture.Width);
                gaussianEffect.Parameters["xHeight"].SetValue(texture.Height);

                device.SetRenderTarget(rt);
                device.SetVertexBuffer(GraphicsUtil.FullTargetVertexBuffer);
                device.Indices = GraphicsUtil.FullTargetIndexBuffer;
                device.BlendState = BlendState.AlphaBlend;
                device.Clear(Color.Transparent);

                foreach (EffectPass pass in gaussianEffect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 2);
                }
            }
        }
        private void DrawToOutput(GraphicsDevice device, SpriteBatch sb)
        {
            // Use Sprite-Batch alpha blending to create the average of the blurred render targets
            Rectangle rect = new Rectangle(0, 0, Output.Width, Output.Height);
            device.SetRenderTarget(Output);
            device.Clear(Color.Transparent);
            sb.Begin();
            for (int i = 0; i < numRenderTargets; i++)
            {
                float alpha = 1f / (i + 1f);
                Color tint = Color.White * alpha;
                sb.Draw(primaryRenderTargets[i], rect, tint);
            }
            sb.End();
        }
    }
}
