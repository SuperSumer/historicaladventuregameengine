﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class DepthMap
    {
        /* -------- Properties -------- */
        public int Width { get; private set; }
        public int Height { get; private set; }
        public RenderTarget2D RenderTarget { get; private set; }

        /* -------- Private Fields -------- */
        private Color[] pixels;

        /* -------- Constructors -------- */
        public DepthMap(int inWidth, int inHeight, GraphicsDevice device)
        {
            Width = inWidth;
            Height = inHeight;

            RenderTarget = new RenderTarget2D(device, Width, Height);
            pixels = new Color[Width * Height];
        }
        ~DepthMap()
        {
            RenderTarget.Dispose();
        }

        /* -------- Public Methods -------- */
        public void CopyPixelData()
        {
            RenderTarget.GetData(pixels);
        }
        public float GetDepth(Point pos)
        {
            pos.X = Mathf.Clamp(pos.X, 0, Width - 1);
            pos.Y = Mathf.Clamp(pos.Y, 0, Height - 1);

            int idx = pos.X + pos.Y * Width;
            Color pixel = pixels[idx];
            float depth = pixel.R / 255f;

            return depth;
        }
    }
}
