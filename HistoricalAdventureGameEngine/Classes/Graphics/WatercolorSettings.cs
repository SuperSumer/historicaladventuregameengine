﻿using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class WatercolorSettings
    {
        /* -------- Properties -------- */
        public float PaperSizeMultiplier { get; set; }
        public float WobbleOffsetXMultiplier { get; set; }
        public float WobbleOffsetYMultiplier { get; set; }
        public float EdgeDarkeningMultiplier { get; set; }
        public float NoiseMultiplier { get; set; }
        public float PerlinMultiplier { get; set; }
        public float AmbientOcclusionMultiplier { get; set; }
        public float PaperAlpha { get; set; }
        public float PaintAlpha { get; set; }

        /* -------- Static Methods -------- */
        public static WatercolorSettings CreateFromData(WatercolorArtStyleData data)
        {
            WatercolorSettings settings = new WatercolorSettings();
            settings.PaperSizeMultiplier = data.PaperSizeMultiplier;
            settings.WobbleOffsetXMultiplier = data.WobbleOffsetXMultiplier;
            settings.WobbleOffsetYMultiplier = data.WobbleOffsetYMultiplier;
            settings.EdgeDarkeningMultiplier = data.EdgeDarkeningMultiplier;
            settings.NoiseMultiplier = data.NoiseMultiplier;
            settings.PerlinMultiplier = data.PerlinMultiplier;
            settings.AmbientOcclusionMultiplier = data.AmbientOcclusionMultiplier;
            settings.PaperAlpha = data.PaperAlpha;
            settings.PaintAlpha = data.PaintAlpha;

            return settings;
        }
    }
}
