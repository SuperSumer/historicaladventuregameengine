﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    struct VertexPositionTwoTexture : IVertexType
    {
        /* -------- Properties -------- */
        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }
        public Vector2 TextureCoordinate1
        {
            get { return textureCoordinate1; }
            set { textureCoordinate1 = value; }
        }
        public Vector2 TextureCoordinate2
        {
            get { return textureCoordinate2; }
            set { textureCoordinate2 = value; }
        }

        /* -------- Private Fields -------- */
        private Vector3 position;
        private Vector2 textureCoordinate1;
        private Vector2 textureCoordinate2;

        /* -------- Constructors -------- */
        public VertexPositionTwoTexture(Vector3 inPosition, Vector2 inTextureCoordinate1, Vector2 inTextureCoordinate2)
        {
            position = inPosition;
            textureCoordinate1 = inTextureCoordinate1;
            textureCoordinate2 = inTextureCoordinate2;
        }

        /* -------- Vertex Declaration -------- */
        public readonly static VertexDeclaration VertexDeclaration = new VertexDeclaration
        (
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(12, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(20, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 1)
        );
        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }
    }
}
