﻿namespace HistoricalAdventureGameEngine
{
    class GaussianBlurSettings
    {
        /* -------- Properties -------- */
        public int NumDownsamples { get; private set; }
        public int KernelSize { get; private set; }

        /* -------- Constructors -------- */
        public GaussianBlurSettings(int inNumDownsamples, int inKernelSize)
        {
            NumDownsamples = inNumDownsamples;
            KernelSize = inKernelSize;
        }
    }
}
