﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace HistoricalAdventureGameEngine
{
    class WatercolorRenderer
    {
        /* -------- Properties -------- */
        public Texture2D Output => outputRT;

        /* -------- Private Fields -------- */
        private int width;
        private int height;
        private WatercolorSettings settings;
        // Render targets
        private RenderTarget2D originalImageRT;
        private RenderTarget2D smoothedImageRT;
        private RenderTarget2D wobbledImageRT;
        private RenderTarget2D edgeDarkenedImageRT;
        private RenderTarget2D noisedImageRT;
        private RenderTarget2D outputRT;
        // Graphics objects
        private Texture2D noiseTex;
        private Texture2D perlinTex;
        private Texture2D perlinBlurTex;
        private Texture2D vignetteTex;
        private Texture2D paperTex;
        private Texture2D paperNormalTex;
        private Texture2D paperBumpTex;
        private Effect maskEffect;
        private Effect wobbleEffect;
        private Effect edgeDarkeningEffect;
        private Effect noiseEffect;
        // Buffers
        private VertexBuffer oneTextureVertexBuffer;
        private VertexBuffer twoTextureVertexBuffer;
        private VertexBuffer threeTextureVertexBuffer;
        private IndexBuffer indexBuffer;

        /* -------- Constructors -------- */
        public WatercolorRenderer(int inWidth, int inHeight, WatercolorSettings inSettings, GraphicsDevice device)
        {
            width = inWidth;
            height = inHeight;
            settings = inSettings;

            originalImageRT = new RenderTarget2D(device, inWidth, inHeight);
            smoothedImageRT = new RenderTarget2D(device, inWidth, inHeight);
            wobbledImageRT = new RenderTarget2D(device, inWidth, inHeight);
            edgeDarkenedImageRT = new RenderTarget2D(device, inWidth, inHeight);
            noisedImageRT = new RenderTarget2D(device, inWidth, inHeight);
            outputRT = new RenderTarget2D(device, inWidth, inHeight);

            noiseTex = ContentLoader.LoadTexture("Textures/Watercolor/Noise");
            perlinTex = ContentLoader.LoadTexture("Textures/Watercolor/Perlin");
            perlinBlurTex = ContentLoader.LoadTexture("Textures/Watercolor/PerlinBlurred");
            vignetteTex = ContentLoader.LoadTexture("Textures/Watercolor/Vignette");
            paperTex = ContentLoader.LoadTexture("Textures/Watercolor/Texture");
            paperNormalTex = ContentLoader.LoadTexture("Textures/Watercolor/NormalLowRes");
            paperBumpTex = ContentLoader.LoadTexture("Textures/Watercolor/Bump");

            maskEffect = ContentLoader.LoadEffect("Effects/Watercolor/Mask");
            wobbleEffect = ContentLoader.LoadEffect("Effects/Watercolor/Wobble");
            edgeDarkeningEffect = ContentLoader.LoadEffect("Effects/Watercolor/EdgeDarkening");
            noiseEffect = ContentLoader.LoadEffect("Effects/Watercolor/Noise");

            CreateBuffers(device);
        }

        /* -------- Public Methods -------- */
        public void Render(Texture2D input, float animation, GraphicsDevice device, SpriteBatch sb)
        {
            RenderMaskedImage(input, animation, device);
            RenderSmoothedImage(device, sb);
            RenderWobbledImage(device);
            RenderEdgeDarkenedImage(device);
            RenderNoisedImage(device);
            RenderOutput(device, sb);
        }

        /* -------- Private Methods -------- */
        private void CreateBuffers(GraphicsDevice device)
        {
            VertexPositionTexture[] oneTextureVertices = new VertexPositionTexture[4];
            oneTextureVertices[0] = new VertexPositionTexture(new Vector3(-1f, 1f, 0f), new Vector2(0f, 0f));
            oneTextureVertices[1] = new VertexPositionTexture(new Vector3(1f, 1f, 0f), new Vector2(1f, 0f));
            oneTextureVertices[2] = new VertexPositionTexture(new Vector3(-1f, -1f, 0f), new Vector2(0f, 1f));
            oneTextureVertices[3] = new VertexPositionTexture(new Vector3(1f, -1f, 0f), new Vector2(1f, 1f));
            oneTextureVertexBuffer = new VertexBuffer(device, VertexPositionTexture.VertexDeclaration, oneTextureVertices.Length, BufferUsage.WriteOnly);
            oneTextureVertexBuffer.SetData(oneTextureVertices);
            
            VertexPositionTwoTexture[] twoTextureVertices = new VertexPositionTwoTexture[4];
            twoTextureVertices[0] = new VertexPositionTwoTexture(new Vector3(-1f, 1f, 0f), new Vector2(0f, 0f), GraphicsUtil.GetFittedTopLeftTextureCoord(width, height));
            twoTextureVertices[1] = new VertexPositionTwoTexture(new Vector3(1f, 1f, 0f), new Vector2(1f, 0f), GraphicsUtil.GetFittedTopRightTextureCoord(width, height));
            twoTextureVertices[2] = new VertexPositionTwoTexture(new Vector3(-1f, -1f, 0f), new Vector2(0f, 1f), GraphicsUtil.GetFittedBottomLeftTextureCoord(width, height));
            twoTextureVertices[3] = new VertexPositionTwoTexture(new Vector3(1f, -1f, 0f), new Vector2(1f, 1f), GraphicsUtil.GetFittedBottomRightTextureCoord(width, height));
            twoTextureVertexBuffer = new VertexBuffer(device, VertexPositionTwoTexture.VertexDeclaration, twoTextureVertices.Length, BufferUsage.WriteOnly);
            twoTextureVertexBuffer.SetData(twoTextureVertices);

            float paperU = (width / (float)paperTex.Width) / settings.PaperSizeMultiplier;
            float paperV = (height / (float)paperTex.Height) / settings.PaperSizeMultiplier;
            VertexPositionThreeTexture[] threeTextureVertices = new VertexPositionThreeTexture[4];
            threeTextureVertices[0] = new VertexPositionThreeTexture(new Vector3(-1f, 1f, 0f), new Vector2(0f, 0f), GraphicsUtil.GetFittedTopLeftTextureCoord(width, height), new Vector2(0f, 0f));
            threeTextureVertices[1] = new VertexPositionThreeTexture(new Vector3(1f, 1f, 0f), new Vector2(1f, 0f), GraphicsUtil.GetFittedTopRightTextureCoord(width, height), new Vector2(paperU, 0f));
            threeTextureVertices[2] = new VertexPositionThreeTexture(new Vector3(-1f, -1f, 0f), new Vector2(0f, 1f), GraphicsUtil.GetFittedBottomLeftTextureCoord(width, height), new Vector2(0f, paperV));
            threeTextureVertices[3] = new VertexPositionThreeTexture(new Vector3(1f, -1f, 0f), new Vector2(1f, 1f), GraphicsUtil.GetFittedBottomRightTextureCoord(width, height), new Vector2(paperU, paperV));
            threeTextureVertexBuffer = new VertexBuffer(device, VertexPositionThreeTexture.VertexDeclaration, threeTextureVertices.Length, BufferUsage.WriteOnly);
            threeTextureVertexBuffer.SetData(threeTextureVertices);

            int[] indices = new int[]
            {
                0, 1, 2,
                2, 1, 3
            };
            indexBuffer = new IndexBuffer(device, IndexElementSize.ThirtyTwoBits, indices.Length, BufferUsage.WriteOnly);
            indexBuffer.SetData(indices);
        }

        private void RenderMaskedImage(Texture2D input, float animation, GraphicsDevice device)
        {
            device.SetRenderTarget(originalImageRT);
            device.BlendState = BlendState.AlphaBlend;
            device.Clear(Color.Transparent);

            device.SetVertexBuffer(twoTextureVertexBuffer);
            device.Indices = indexBuffer;

            maskEffect.CurrentTechnique = maskEffect.Techniques["Mask"];
            maskEffect.Parameters["xTexture"].SetValue(input);
            maskEffect.Parameters["xMask"].SetValue(perlinBlurTex);
            maskEffect.Parameters["xVignette"].SetValue(vignetteTex);
            maskEffect.Parameters["xThreshold"].SetValue(1f - animation);

            foreach (EffectPass pass in maskEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 2);
            }
        }
        private void RenderSmoothedImage(GraphicsDevice device, SpriteBatch sb)
        {
            device.SetRenderTarget(smoothedImageRT);
            device.Clear(Color.Transparent);

            sb.Begin();
            Rectangle renderRect = new Rectangle(0, 0, width, height);
            sb.Draw(originalImageRT, renderRect, Color.White);
            sb.End();
        }
        private void RenderWobbledImage(GraphicsDevice device)
        {
            device.SetRenderTarget(wobbledImageRT);
            device.BlendState = BlendState.AlphaBlend;
            device.Clear(Color.Transparent);

            device.SetVertexBuffer(threeTextureVertexBuffer);
            device.Indices = indexBuffer;

            wobbleEffect.CurrentTechnique = wobbleEffect.Techniques["Wobble"];
            wobbleEffect.Parameters["xTexture"].SetValue(smoothedImageRT);
            wobbleEffect.Parameters["xNormalMap"].SetValue(paperNormalTex);
            wobbleEffect.Parameters["xOffsetXMultiplier"].SetValue(settings.WobbleOffsetXMultiplier);
            wobbleEffect.Parameters["xOffsetYMultiplier"].SetValue(settings.WobbleOffsetYMultiplier);

            foreach (EffectPass pass in wobbleEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 2);
            }
        }
        private void RenderEdgeDarkenedImage(GraphicsDevice device)
        {
            device.SetRenderTarget(edgeDarkenedImageRT);
            device.BlendState = BlendState.AlphaBlend;
            device.Clear(Color.Transparent);

            device.SetVertexBuffer(oneTextureVertexBuffer);
            device.Indices = indexBuffer;

            edgeDarkeningEffect.CurrentTechnique = edgeDarkeningEffect.Techniques["EdgeDarkening"];
            edgeDarkeningEffect.Parameters["xTexture"].SetValue(wobbledImageRT);
            edgeDarkeningEffect.Parameters["xEdgeDarkenMultiplier"].SetValue(settings.EdgeDarkeningMultiplier);
            edgeDarkeningEffect.Parameters["xWidth"].SetValue(width);
            edgeDarkeningEffect.Parameters["xHeight"].SetValue(height);

            foreach (EffectPass pass in edgeDarkeningEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 2);
            }
        }
        private void RenderNoisedImage(GraphicsDevice device)
        {
            device.SetRenderTarget(noisedImageRT);
            device.BlendState = BlendState.AlphaBlend;
            device.Clear(Color.Transparent);

            device.SetVertexBuffer(threeTextureVertexBuffer);
            device.Indices = indexBuffer;

            noiseEffect.CurrentTechnique = noiseEffect.Techniques["Noise"];
            noiseEffect.Parameters["xTexture"].SetValue(edgeDarkenedImageRT);
            noiseEffect.Parameters["xNoiseMap"].SetValue(noiseTex);
            noiseEffect.Parameters["xPerlinMap"].SetValue(perlinTex);
            noiseEffect.Parameters["xAmbientOcclusionMap"].SetValue(paperBumpTex);
            noiseEffect.Parameters["xNoiseMultiplier"].SetValue(settings.NoiseMultiplier);
            noiseEffect.Parameters["xPerlinMultiplier"].SetValue(settings.PerlinMultiplier);
            noiseEffect.Parameters["xAmbientOcclusionMultiplier"].SetValue(settings.AmbientOcclusionMultiplier);

            foreach (EffectPass pass in noiseEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 2);
            }
        }
        private void RenderOutput(GraphicsDevice device, SpriteBatch sb)
        {
            device.SetRenderTarget(outputRT);
            device.Clear(Color.White);

            Color paperTint = Color.White * settings.PaperAlpha;
            Color paintTint = Color.White * settings.PaintAlpha;

            sb.Begin();

            // Draw paper textures
            int paperWidth = (int)(paperTex.Width * settings.PaperSizeMultiplier);
            int paperHeight = (int)(paperTex.Height * settings.PaperSizeMultiplier);
            int numPaperX = (int)Math.Ceiling(width / (float)paperWidth);
            int numPaperY = (int)Math.Ceiling(height / (float)paperHeight);
            for (int x = 0; x < numPaperX; x++)
                for (int y = 0; y < numPaperY; y++)
                    sb.Draw(paperTex, new Rectangle(x * paperWidth, y * paperHeight, paperWidth, paperHeight), paperTint);

            Rectangle renderRect = new Rectangle(0, 0, width, height);
            sb.Draw(noisedImageRT, renderRect, paintTint);
            sb.End();
        }
    }
}
