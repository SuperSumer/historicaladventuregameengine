﻿using System;
using System.Collections.Generic;

namespace HistoricalAdventureGameEngine
{
    static class RandomUtil
    {
        /* -------- Private Fields -------- */
        private static Random rand = new Random(DateTime.UtcNow.GetHashCode());

        /* -------- Public Methods -------- */
        public static int GetRandom(int min, int max)
        {
            if (min == max)
                return min;

            return rand.Next(min, max + 1);
        }

        public static void RandomizeOrder<T>(List<T> list)
        {
            if (list.Count < 2)
                return;

            for (int i = 0; i < list.Count; i++)
            {
                T temp = list[i];
                int swapIdx = GetRandom(0, list.Count - 1);
                list[i] = list[swapIdx];
                list[swapIdx] = temp;
            }
        }
    }
}
