﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    static class GraphicsUtil
    {
        /* -------- Properties -------- */
        public static VertexBuffer FullTargetVertexBuffer { get; private set; }
        public static IndexBuffer FullTargetIndexBuffer { get; private set; }

        /* -------- Public Methods -------- */
        public static void Initialize(GraphicsDevice device)
        {
            CreateBuffers(device);
        }
        public static Color ElementWiseMultiplyColors(Color c1, Color c2)
        {
            float r = (c1.R / 255f) * (c2.R / 255f);
            float g = (c1.G / 255f) * (c2.G / 255f);
            float b = (c1.B / 255f) * (c2.B / 255f);
            float a = (c1.A / 255f) * (c2.A / 255f);

            return new Color(r, g, b, a);
        }

        public static Vector2 GetFittedTopLeftTextureCoord(int width, int height) => new Vector2(0.5f - 0.5f * width / Mathf.Max(width, height), 0.5f - 0.5f * height / Mathf.Max(width, height));
        public static Vector2 GetFittedTopRightTextureCoord(int width, int height) => new Vector2(0.5f + 0.5f * width / Mathf.Max(width, height), 0.5f - 0.5f * height / Mathf.Max(width, height));
        public static Vector2 GetFittedBottomLeftTextureCoord(int width, int height) => new Vector2(0.5f - 0.5f * width / Mathf.Max(width, height), 0.5f + 0.5f * height / Mathf.Max(width, height));
        public static Vector2 GetFittedBottomRightTextureCoord(int width, int height) => new Vector2(0.5f + 0.5f * width / Mathf.Max(width, height), 0.5f + 0.5f * height / Mathf.Max(width, height));

        /* -------- Private Methods -------- */
        private static void CreateBuffers(GraphicsDevice device)
        {
            VertexPositionTexture[] vertices = new VertexPositionTexture[4];
            vertices[0] = new VertexPositionTexture(new Vector3(-1f, 1f, 0f), new Vector2(0f, 0f));
            vertices[1] = new VertexPositionTexture(new Vector3(1f, 1f, 0f), new Vector2(1f, 0f));
            vertices[2] = new VertexPositionTexture(new Vector3(-1f, -1f, 0f), new Vector2(0f, 1f));
            vertices[3] = new VertexPositionTexture(new Vector3(1f, -1f, 0f), new Vector2(1f, 1f));
            FullTargetVertexBuffer = new VertexBuffer(device, VertexPositionTexture.VertexDeclaration, vertices.Length, BufferUsage.WriteOnly);
            FullTargetVertexBuffer.SetData(vertices);

            int[] indices = new int[]
            {
                0, 1, 2,
                2, 1, 3
            };
            FullTargetIndexBuffer = new IndexBuffer(device, IndexElementSize.ThirtyTwoBits, indices.Length, BufferUsage.WriteOnly);
            FullTargetIndexBuffer.SetData(indices);
        }
    }
}
