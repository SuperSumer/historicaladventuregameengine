﻿using System;
using Microsoft.Xna.Framework;

namespace HistoricalAdventureGameEngine
{
    static class Mathf
    {
        /* -------- Properties -------- */
        public static float Pi => (float)Math.PI;
        public static float TwoPi => 2f * Pi;
        public static float PiByTwo => Pi / 2f;
        public static float PiByFour => Pi / 4f;

        /* -------- Public Methods -------- */
        public static float Clamp(float val, float min, float max)
        {
            if (val < min)
                return min;
            if (val > max)
                return max;
            return val;
        }
        public static int Clamp(int val, int min, int max)
        {
            if (val < min)
                return min;
            if (val > max)
                return max;
            return val;
        }
        public static float ClampUpper(float val, float max)
        {
            if (val > max)
                return max;
            return val;
        }
        public static int ClampUpper(int val, int max)
        {
            if (val > max)
                return max;
            return val;
        }
        public static float ClampLower(float val, float min)
        {
            if (val < min)
                return min;
            return val;
        }
        public static int ClampLower(int val, int min)
        {
            if (val < min)
                return min;
            return val;
        }
        public static float Clamp01(float val) => Clamp(val, 0f, 1f);

        public static int Round(float val)
        {
            float remainder = val % 1f;
            if (remainder < 0.5f)
                return Floor(val);
            else
                return Ceiling(val);
        }
        public static Point Round(Vector2 vec) => new Point(Round(vec.X), Round(vec.Y));
        public static int Floor(float val) => (int)val;
        public static Point Floor(Vector2 vec) => new Point(Floor(vec.X), Floor(vec.Y));
        public static int Ceiling(float val)
        {
            if (val > 0f)
                return (int)val + 1;
            else
                return (int)val - 1;
        }
        public static Point Ceiling(Vector2 vec) => new Point(Ceiling(vec.X), Ceiling(vec.Y));

        public static Vector2 CastVector(Point point) => new Vector2(point.X, point.Y);

        public static float Lerp(float start, float end, float factor, bool limitToRange = false)
        {
            if (limitToRange)
                factor = Clamp01(factor);

            return start + factor * (end - start);
        }
        public static float GetLerpFactor(float start, float end, float val, bool limitToRange = false)
        {
            if (start == end)
                return 0f;

            float factor = (val - start) / (end - start);
            if (limitToRange)
                factor = Clamp01(factor);

            return factor;
        }

        public static float Abs(float val) => val < 0f ? -val : val;
        public static float Abs(int val) => val < 0 ? -val : val;

        public static float Max(params float[] vals)
        {
            if (vals.Length == 0)
                throw new ArgumentException("No values passed to find maximum of");

            float max = vals[0];
            for (int i = 1; i < vals.Length; i++)
                if (vals[i] > max)
                    max = vals[i];

            return max;
        }
        public static int Max(params int[] vals)
        {
            if (vals.Length == 0)
                throw new ArgumentException("No values passed to find maximum of");

            int max = vals[0];
            for (int i = 1; i < vals.Length; i++)
                if (vals[i] > max)
                    max = vals[i];

            return max;
        }
        public static float Min(params float[] vals)
        {
            if (vals.Length == 0)
                throw new ArgumentException("No values passed to find minimum of");

            float min = vals[0];
            for (int i = 1; i < vals.Length; i++)
                if (vals[i] < min)
                    min = vals[i];

            return min;
        }
        public static int Min(params int[] vals)
        {
            if (vals.Length == 0)
                throw new ArgumentException("No values passed to find minimum of");

            int min = vals[0];
            for (int i = 1; i < vals.Length; i++)
                if (vals[i] < min)
                    min = vals[i];

            return min;
        }

        public static float Sqrt(float val) => (float)Math.Sqrt(val);

        public static float PointToPointDistance(Point p1, Point p2)
        {
            int x = p1.X - p2.X;
            int y = p1.Y - p2.Y;
            float dist = Sqrt(x * x + y * y);

            return dist;
        }
    }
}
