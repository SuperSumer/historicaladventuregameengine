﻿using System;

namespace HistoricalAdventureGameEngine
{
    static class EnumUtil
    {
        /* -------- Public Methods -------- */
        public static T[] GetVals<T>() where T : struct
        {
            T[] array = Enum.GetValues(typeof(T)) as T[];
            if (array == null)
                return new T[0];
            return array;
        }

        public static T Parse<T>(string str, bool ignoreCase = true) where T : struct
        {
            T result;
            if (Enum.TryParse<T>(str, ignoreCase, out result))
                return result;
            return default(T);
        }
    }
}
