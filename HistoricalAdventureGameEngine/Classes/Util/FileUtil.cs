﻿using System;
using System.IO;

namespace HistoricalAdventureGameEngine
{
    static class FileUtil
    {
        /* -------- Public Methods -------- */
        public static string GetExecutablePath() => AppDomain.CurrentDomain.BaseDirectory;

        public static StreamReader ReadFile(string path)
        {
            try
            {
                return new StreamReader(path);
            }
            catch
            {
                return null;
            }
        }
    }
}
