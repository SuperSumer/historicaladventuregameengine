﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    static class StringUtil
    {
        /* -------- Public Methods -------- */
        public static string InsertLineBreaksForWidth(string text, SpriteFont sf, int width)
        {
            List<string> lines = new List<string>(text.Split('\n'));
            List<string> newLines = new List<string>();

            foreach (string line in lines)
            {
                string newLine = "";
                string longerNewLine;

                List<string> words = new List<string>(line.Split(' '));
                foreach (string word in words)
                {
                    longerNewLine = (newLine == "") ? word : newLine + " " + word;

                    if (sf.MeasureString(longerNewLine).X > width)
                    {
                        // The line would be too long if the word were added
                        if (newLine == "")
                        {
                            // It's the first word, add it anyway
                            newLines.Add(longerNewLine);
                        }
                        else
                        {
                            // Add the shorter line, start a new line, starting with the excess word
                            newLines.Add(newLine);
                            newLine = word;
                        }
                    }
                    else
                        newLine = longerNewLine;
                }
                newLines.Add(newLine);
            }

            string result = "";
            foreach (string line in newLines)
            {
                if (result != "")
                    result += '\n';
                result += line;
            }
            return result;
        }
    }
}
