﻿using System.IO;
using System.Xml.Serialization;

namespace HistoricalAdventureGameEngine
{
    static class XmlUtil
    {
        /* -------- Public Methods -------- */
        public static T DeserializeFromXml<T>(string path) where T : class
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));
            try
            {
                using (StreamReader reader = new StreamReader(path))
                    return ser.Deserialize(reader) as T;
            }
            catch
            {
                return null;
            }
        }
        public static bool SerializeToXml<T>(string path, T obj) where T : class
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));
            try
            {
                using (StreamWriter writer = new StreamWriter(path))
                {
                    ser.Serialize(writer, obj);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
        public static string GetXmlFilePath(string filename) => Path.Combine(FileUtil.GetExecutablePath(), "Content", "XML", filename);
    }
}
