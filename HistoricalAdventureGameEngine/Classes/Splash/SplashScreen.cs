﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class SplashScreen
    {
        /* -------- Properties -------- */
        public Texture2D Texture { get; private set; }
        public Color BackgroundColor { get; private set; }

        /* -------- Constructors -------- */
        public SplashScreen(Texture2D inTexture, Color inBackgroundColor)
        {
            Texture = inTexture;
            BackgroundColor = inBackgroundColor;
        }

        /* -------- Public Methods -------- */
        public void Draw(GraphicsComponent g, float alpha)
        {
            g.GraphicsDevice.SetRenderTarget(null);
            g.GraphicsDevice.Clear(BackgroundColor);

            g.SpriteBatch.Begin();
            Color tint = Color.White * alpha;
            Rectangle rect = new Rectangle(g.HalfScreenWidth - Texture.Width / 2, g.HalfScreenHeight - Texture.Height / 2, Texture.Width, Texture.Height);
            g.SpriteBatch.Draw(Texture, rect, tint);
            g.SpriteBatch.End();
        }
    }
}
