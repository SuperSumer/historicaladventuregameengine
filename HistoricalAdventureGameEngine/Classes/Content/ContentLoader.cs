﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    static class ContentLoader
    {
        /* -------- Private Fields -------- */
        private static ContentManager contentManager;
        private static Dictionary<string, string> textureDict = new Dictionary<string, string>();
        private static Dictionary<string, string> soundEffectDict = new Dictionary<string, string>();
        // Default assets
        private static Texture2D defaultTexture;
        private static SoundEffect defaultSoundEffect;
        private static SpriteFont defaultSpriteFont;

        /* -------- Public Methods -------- */
        public static void Initialize(ContentManager cm)
        {
            contentManager = cm;
        }
        public static void LoadContent()
        {
            defaultTexture = contentManager.Load<Texture2D>("Default Assets/MagentaChecker");
            defaultSoundEffect = contentManager.Load<SoundEffect>("Default Assets/ErrorSound");
            defaultSpriteFont = contentManager.Load<SpriteFont>("Default Assets/Arial16");
        }

        public static void AddTextureListItem(AssetData ali)
        {
            if (ali == null)
                return;
            textureDict.Add(ali.ID, ali.ContentPath);
        }
        public static void AddSoundEffectListItem(AssetData ali)
        {
            if (ali == null)
                return;
            soundEffectDict.Add(ali.ID, ali.ContentPath);
        }

        public static Texture2D LoadTexture(string idOrPath)
        {
            if (idOrPath == null)
                return defaultTexture;

            // Check if it's an ID in the dictionary
            if (textureDict.ContainsKey(idOrPath))
            {
                string path = textureDict[idOrPath];
                try
                {
                    Texture2D tex = contentManager.Load<Texture2D>(path);
                    if (tex != null)
                        return tex;
                }
                catch {}
            }

            // Check if it's a direct path
            try
            {
                Texture2D tex = contentManager.Load<Texture2D>(idOrPath);
                if (tex != null)
                    return tex;
            }
            catch { }

            // Return default texture
            Debug.LogWarning($"Could not load texture with ID/path {idOrPath}, loading default texture instead");
            return defaultTexture;
        }
        public static SoundEffect LoadSoundEffect(string idOrPath)
        {
            if (idOrPath == null)
                return defaultSoundEffect;

            // Check if it's an ID in the dictionary
            if (soundEffectDict.ContainsKey(idOrPath))
            {
                string path = soundEffectDict[idOrPath];
                try
                {
                    SoundEffect sfx = contentManager.Load<SoundEffect>(path);
                    if (sfx != null)
                        return sfx;
                }
                catch { }
            }

            // Check if it's a direct path
            try
            {
                SoundEffect sfx = contentManager.Load<SoundEffect>(idOrPath);
                if (sfx != null)
                    return sfx;
            }
            catch { }

            // Return default sound effect
            Debug.LogWarning($"Could not load sound effect with ID/path {idOrPath}, loading default sound effect instead");
            return defaultSoundEffect;
        }
        public static SpriteFont LoadSpriteFont(string path)
        {
            if (path == null)
                return defaultSpriteFont;

            // Check if it's a direct path
            try
            {
                SpriteFont sf = contentManager.Load<SpriteFont>(path);
                if (sf != null)
                    return sf;
            }
            catch { }

            // Return default sprite font
            Debug.LogWarning($"Could not load sprite font with path {path}, loading default sprite font instead");
            return defaultSpriteFont;
        }
        public static Effect LoadEffect(string path)
        {
            if (path == null)
                return null;

            // Check if it's a direct path
            try
            {
                Effect effect = contentManager.Load<Effect>(path);
                if (effect != null)
                    return effect;
            }
            catch { }

            // Return null
            Debug.LogWarning($"Could not load effect with path {path}, passing null instead");
            return null;
        }

        public static void LoadTextureAssetLists(string[] filenames)
        {
            foreach (string filename in filenames)
            {
                string filePath = XmlUtil.GetXmlFilePath(filename);
                AssetData[] assetList = XmlUtil.DeserializeFromXml<AssetData[]>(filePath);
                if (assetList == null)
                    continue;

                foreach (AssetData assetItem in assetList)
                    textureDict.Add(assetItem.ID, assetItem.ContentPath);
            }
        }
        public static void LoadSoundEffectAssetLists(string[] filenames)
        {
            foreach (string filename in filenames)
            {
                string filePath = XmlUtil.GetXmlFilePath(filename);
                AssetData[] assetList = XmlUtil.DeserializeFromXml<AssetData[]>(filePath);
                if (assetList == null)
                    continue;

                foreach (AssetData assetItem in assetList)
                    soundEffectDict.Add(assetItem.ID, assetItem.ContentPath);
            }
        }
    }
}
