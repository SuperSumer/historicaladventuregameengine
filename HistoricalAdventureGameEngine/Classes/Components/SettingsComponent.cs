﻿using System.IO;

namespace HistoricalAdventureGameEngine
{
    class SettingsComponent
    {
        /* -------- Properties -------- */
        // Program
        public int ScreenWidth { get; private set; } = 0;
        public int ScreenHeight { get; private set; } = 0;
        public bool FullScreen { get; private set; } = false;
        public bool ShowSplash { get; private set; } = true;
        public bool ShowDebug { get; private set; } = true;
        public bool UseTouchScreen { get; private set; } = false;
        // Graphics
        public int GaussianDownsamples { get; private set; } = 0;
        public int GaussianKernelSize { get; private set; } = 3;
        public float BlurMultiplier { get; private set; } = 0f;
        public float ObjectAnimationTime { get; private set; } = 0.1f;
        public float SceneAnimationTime { get; private set; } = 0.5f;
        public float ParallaxXMultiplier { get; private set; } = 1.0f;
        public float ParallaxYMultiplier { get; private set; } = 1.0f;
        public bool ShowCursor { get; private set; } = true;
        // Sound
        public float Volume { get; private set; } = 1.0f;
        public float Pan { get; private set; } = 0.0f;
        public float Pitch { get; private set; } = 0.0f;
        // Game
        public string GameFilename { get; private set; } = null;
        // Logging
        public bool RecordLogs { get; private set; } = true;
        public string LogPath { get; private set; } = @"C:\TEMP";

        /* -------- Constants -------- */
        private const string SETTINGS_FILE_NAME = "Settings.txt";

        /* -------- Public Methods -------- */
        public void LoadContent()
        {
            // Read from settings file
            string filePath = Path.Combine(FileUtil.GetExecutablePath(), SETTINGS_FILE_NAME);
            using (StreamReader sr = FileUtil.ReadFile(filePath))
            {
                if (sr == null)
                {
                    FatalErrorHandler.ActivateFatalError("Could not locate and open settings file");
                    return;
                }

                string line = sr.ReadLine();
                while (line != null)
                {
                    // Lines starting with # are comments
                    if (!line.StartsWith("#"))
                    {
                        string[] parts = line.Split(';');
                        if (parts.Length == 2)
                        {
                            string name = parts[0];
                            string value = parts[1];

                            switch (name)
                            {
                                // Cases for parsing the different settings lines
                                case "Resolution":
                                    string[] resParts = value.Split('x');
                                    if (resParts.Length == 2)
                                    {
                                        int width, height;
                                        if (int.TryParse(resParts[0], out width) && int.TryParse(resParts[1], out height))
                                        {
                                            ScreenWidth = width;
                                            ScreenHeight = height;
                                        }
                                    }
                                    break;
                                case "Windowed":
                                    bool windowed;
                                    if (bool.TryParse(value, out windowed))
                                        FullScreen = !windowed;
                                    break;
                                case "ShowSplash":
                                    bool showSplash;
                                    if (bool.TryParse(value, out showSplash))
                                        ShowSplash = showSplash;
                                    break;
                                case "ShowDebug":
                                    bool showDebug;
                                    if (bool.TryParse(value, out showDebug))
                                        ShowDebug = showDebug;
                                    break;
                                case "UseTouchScreen":
                                    bool useTouchScreen;
                                    if (bool.TryParse(value, out useTouchScreen))
                                        UseTouchScreen = useTouchScreen;
                                    break;
                                case "GaussianDownsamples":
                                    int downsamples;
                                    if (int.TryParse(value, out downsamples))
                                        GaussianDownsamples = downsamples;
                                    break;
                                case "GaussianKernelSize":
                                    int kernel;
                                    if (int.TryParse(value, out kernel))
                                        GaussianKernelSize = kernel;
                                    break;
                                case "GameFilename":
                                    GameFilename = value;
                                    break;
                                case "BlurMultiplier":
                                    float blur;
                                    if (float.TryParse(value, out blur))
                                        BlurMultiplier = blur;
                                    break;
                                case "ObjectAnimationTime":
                                    float objectAnimTime;
                                    if (float.TryParse(value, out objectAnimTime))
                                        ObjectAnimationTime = objectAnimTime;
                                    break;
                                case "SceneAnimationTime":
                                    float sceneAnimTime;
                                    if (float.TryParse(value, out sceneAnimTime))
                                        SceneAnimationTime = sceneAnimTime;
                                    break;
                                case "ParallaxXMultiplier":
                                    float parallaxXMultiplier;
                                    if (float.TryParse(value, out parallaxXMultiplier))
                                        ParallaxXMultiplier = parallaxXMultiplier;
                                    break;
                                case "ParallaxYMultiplier":
                                    float parallaxYMultiplier;
                                    if (float.TryParse(value, out parallaxYMultiplier))
                                        ParallaxYMultiplier = parallaxYMultiplier;
                                    break;
                                case "ShowCursor":
                                    bool showCursor;
                                    if (bool.TryParse(value, out showCursor))
                                        ShowCursor = showCursor;
                                    break;
                                case "Volume":
                                    float volume;
                                    if (float.TryParse(value, out volume))
                                        Volume = volume;
                                    break;
                                case "Pan":
                                    float pan;
                                    if (float.TryParse(value, out pan))
                                        Pan = pan;
                                    break;
                                case "Pitch":
                                    float pitch;
                                    if (float.TryParse(value, out pitch))
                                        Pitch = pitch;
                                    break;
                                case "RecordLogs":
                                    bool recordLogs;
                                    if (bool.TryParse(value, out recordLogs))
                                        RecordLogs = recordLogs;
                                    break;
                                case "LogPath":
                                    LogPath = value;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    line = sr.ReadLine();
                }
            }
        }
    }
}
