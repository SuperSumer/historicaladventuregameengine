﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eInputMode
    {
        MOUSE_AND_KEYBOARD,
        TOUCH_PANEL
    }
    enum eMouseButton
    {
        LEFT,
        RIGHT,
        MIDDLE,
        X1,
        X2
    }
    enum eButtonState
    {
        RELEASING,
        RELEASED,
        PRESSING,
        PRESSED
    }

    class InputComponent
    {
        /* -------- Properties -------- */
        public eInputMode Mode { get; private set; }
        public Point MousePos { get; private set; }

        /* -------- Private Fields -------- */
        private MouseState mouseState;
        private KeyboardState keyboardState;
        private Dictionary<eMouseButton, eButtonState> mouseButtonDict = new Dictionary<eMouseButton, eButtonState>();
        private Dictionary<Keys, eButtonState> keyboardButtonDict = new Dictionary<Keys, eButtonState>();
        private bool firstFrame = true;

        /* -------- Constructors -------- */
        public InputComponent()
        {
            foreach (eMouseButton mb in EnumUtil.GetVals<eMouseButton>())
                mouseButtonDict.Add(mb, eButtonState.RELEASED);
            foreach (Keys k in EnumUtil.GetVals<Keys>())
                keyboardButtonDict.Add(k, eButtonState.RELEASED);
        }

        /* -------- Public Methods -------- */
        public void LoadContent(SettingsComponent s)
        {
            Mode = s.UseTouchScreen ? eInputMode.TOUCH_PANEL : eInputMode.MOUSE_AND_KEYBOARD;
        }
        public void Update(ComponentContainer cc)
        {
            switch (Mode)
            {
                case eInputMode.MOUSE_AND_KEYBOARD:
                    UpdateMouseState(cc.SettingsComponent);
                    UpdateKeyboardState();
                    break;
                case eInputMode.TOUCH_PANEL:
                    UpdateTouchPanel(cc);
                    break;
                default:
                    break;
            }
        }
        public bool MouseButtonIsPressing(eMouseButton mb) => mouseButtonDict[mb] == eButtonState.PRESSING;
        public bool MouseButtonIsPressingOrPressed(eMouseButton mb) => mouseButtonDict[mb] == eButtonState.PRESSING || mouseButtonDict[mb] == eButtonState.PRESSED;
        public bool KeyboardButtonIsPressing(Keys k) => keyboardButtonDict[k] == eButtonState.PRESSING;
        public bool KeyboardButtonIsPressed(Keys k) => keyboardButtonDict[k] == eButtonState.PRESSED;
        public bool KeyboardCombinationIsPressing(Keys holdKey, Keys pressKey) => KeyboardButtonIsPressed(holdKey) && KeyboardButtonIsPressing(pressKey);

        /* -------- Private Methods -------- */
        private void UpdateMouseState(SettingsComponent s)
        {
            mouseState = Mouse.GetState();

            int mouseX = Mathf.Clamp(mouseState.Position.X, 0, s.ScreenWidth);
            int mouseY = Mathf.Clamp(mouseState.Position.Y, 0, s.ScreenHeight);
            MousePos = new Point(mouseX, mouseY);
            if (firstFrame)
            {
                firstFrame = false;
                if (s.FullScreen)
                    Mouse.SetPosition(s.ScreenWidth / 2, s.ScreenHeight / 2);
            }
            else if (s.FullScreen)
                Mouse.SetPosition(MousePos.X, MousePos.Y);
            
            foreach (eMouseButton mb in EnumUtil.GetVals<eMouseButton>())
            {
                bool buttonIsDown;
                switch (mb)
                {
                    case eMouseButton.LEFT:
                        buttonIsDown = mouseState.LeftButton == ButtonState.Pressed;
                        break;
                    case eMouseButton.RIGHT:
                        buttonIsDown = mouseState.RightButton == ButtonState.Pressed;
                        break;
                    case eMouseButton.MIDDLE:
                        buttonIsDown = mouseState.MiddleButton == ButtonState.Pressed;
                        break;
                    case eMouseButton.X1:
                        buttonIsDown = mouseState.XButton1 == ButtonState.Pressed;
                        break;
                    case eMouseButton.X2:
                        buttonIsDown = mouseState.XButton2 == ButtonState.Pressed;
                        break;
                    default:
                        buttonIsDown = false;
                        break;
                }

                eButtonState bs = mouseButtonDict[mb];
                switch (bs)
                {
                    case eButtonState.RELEASING:
                        if (buttonIsDown)
                            mouseButtonDict[mb] = eButtonState.PRESSING;
                        else
                            mouseButtonDict[mb] = eButtonState.RELEASED;
                        break;
                    case eButtonState.RELEASED:
                        if (buttonIsDown)
                            mouseButtonDict[mb] = eButtonState.PRESSING;
                        break;
                    case eButtonState.PRESSING:
                        if (buttonIsDown)
                            mouseButtonDict[mb] = eButtonState.PRESSED;
                        else
                            mouseButtonDict[mb] = eButtonState.RELEASING;
                        break;
                    case eButtonState.PRESSED:
                        if (!buttonIsDown)
                            mouseButtonDict[mb] = eButtonState.RELEASING;
                        break;
                }
            }
        }
        private void UpdateKeyboardState()
        {
            keyboardState = Keyboard.GetState();
            foreach (Keys k in EnumUtil.GetVals<Keys>())
            {
                eButtonState bs = keyboardButtonDict[k];
                switch (bs)
                {
                    case eButtonState.RELEASING:
                        if (keyboardState.IsKeyDown(k))
                            keyboardButtonDict[k] = eButtonState.PRESSING;
                        else
                            keyboardButtonDict[k] = eButtonState.RELEASED;
                        break;
                    case eButtonState.RELEASED:
                        if (keyboardState.IsKeyDown(k))
                            keyboardButtonDict[k] = eButtonState.PRESSING;
                        break;
                    case eButtonState.PRESSING:
                        if (keyboardState.IsKeyDown(k))
                            keyboardButtonDict[k] = eButtonState.PRESSED;
                        else
                            keyboardButtonDict[k] = eButtonState.RELEASING;
                        break;
                    case eButtonState.PRESSED:
                        if (keyboardState.IsKeyUp(k))
                            keyboardButtonDict[k] = eButtonState.RELEASING;
                        break;
                }
            }
        }

        private void UpdateTouchPanel(ComponentContainer cc)
        {
            TouchCollection col = TouchPanel.GetState();
            if (col.Count == 0)
            {
                // Not touching the screen
                switch (mouseButtonDict[eMouseButton.LEFT])
                {
                    case eButtonState.RELEASING:
                        mouseButtonDict[eMouseButton.LEFT] = eButtonState.RELEASED;
                        break;
                    case eButtonState.RELEASED:
                        break;
                    case eButtonState.PRESSING:
                        mouseButtonDict[eMouseButton.LEFT] = eButtonState.RELEASING;
                        break;
                    case eButtonState.PRESSED:
                        mouseButtonDict[eMouseButton.LEFT] = eButtonState.RELEASING;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                // Touching the screen
                TouchLocation loc = col[0];
                float xMultiplier = cc.SettingsComponent.FullScreen ? cc.GraphicsComponent.ScreenWidth / (float)cc.GraphicsComponent.DeviceWidth : 1f;
                float YMultiplier = cc.SettingsComponent.FullScreen ? cc.GraphicsComponent.ScreenHeight / (float)cc.GraphicsComponent.DeviceHeight : 1f;
                int mouseX = Mathf.Round(loc.Position.X * xMultiplier);
                int mouseY = Mathf.Round(loc.Position.Y * YMultiplier);
                MousePos = new Point(mouseX, mouseY);

                switch (mouseButtonDict[eMouseButton.LEFT])
                {
                    case eButtonState.RELEASING:
                        mouseButtonDict[eMouseButton.LEFT] = eButtonState.PRESSING;
                        break;
                    case eButtonState.RELEASED:
                        mouseButtonDict[eMouseButton.LEFT] = eButtonState.PRESSING;
                        break;
                    case eButtonState.PRESSING:
                        mouseButtonDict[eMouseButton.LEFT] = eButtonState.PRESSED;
                        break;
                    case eButtonState.PRESSED:
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
