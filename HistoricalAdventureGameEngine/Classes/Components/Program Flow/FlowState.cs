﻿namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eFlowStateType
    {
        SPLASH,
        GAME
    }

    abstract class FlowState
    {
        /* -------- Public Methods -------- */
        public abstract void Update(GameTimeWrapper gtw, ComponentContainer cc);
        public abstract void Draw(ComponentContainer cc);
    }
}
