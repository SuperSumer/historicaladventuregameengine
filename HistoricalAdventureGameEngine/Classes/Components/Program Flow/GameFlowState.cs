﻿namespace HistoricalAdventureGameEngine
{
    class GameFlowState : FlowState
    {
        /* -------- Private Fields -------- */
        private GameManagerContainer gameManagerContainer;

        /* -------- Constructors -------- */
        public GameFlowState(ComponentContainer cc)
        {
            gameManagerContainer = new GameManagerContainer(cc);
        }

        /* -------- Public Methods -------- */
        public override void Update(GameTimeWrapper gtw, ComponentContainer cc)
        {
            gameManagerContainer.Update(gtw, cc);
        }
        public override void Draw(ComponentContainer cc)
        {
            gameManagerContainer.Draw(cc);
        }
    }
}
