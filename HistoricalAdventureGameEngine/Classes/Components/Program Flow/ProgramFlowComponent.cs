﻿namespace HistoricalAdventureGameEngine
{
    class ProgramFlowComponent
    {
        /* -------- Private Fields -------- */
        private FlowState flowState;
        private bool flowStateChangeRequested;
        private eFlowStateType requestedFlowState;
        private HistoricalAdventureGameEngine game;

        /* -------- Constructors -------- */
        public ProgramFlowComponent(HistoricalAdventureGameEngine inGame)
        {
            game = inGame;
        }

        /* -------- Public Methods -------- */
        public void LoadContent(ComponentContainer cc)
        {
            if (cc.SettingsComponent.ShowSplash)
                flowState = new SplashFlowState();
            else
                flowState = new GameFlowState(cc);
        }
        public void Update(GameTimeWrapper gtw, ComponentContainer cc)
        {
            if (flowStateChangeRequested)
                PerformFlowStateChange(cc);

            flowState.Update(gtw, cc);
        }
        public void Draw(ComponentContainer cc)
        {
            flowState.Draw(cc);
        }

        public void RequestFlowStateChange(eFlowStateType type)
        {
            flowStateChangeRequested = true;
            requestedFlowState = type;
        }
        public void RequestGameExit()
        {
            game.ExitGame();
        }

        /* -------- Private Methods -------- */
        private void PerformFlowStateChange(ComponentContainer cc)
        {
            switch (requestedFlowState)
            {
                case eFlowStateType.SPLASH:
                    flowState = new SplashFlowState();
                    break;
                case eFlowStateType.GAME:
                    flowState = new GameFlowState(cc);
                    break;
                default:
                    break;
            }

            flowStateChangeRequested = false;
        }
    }
}
