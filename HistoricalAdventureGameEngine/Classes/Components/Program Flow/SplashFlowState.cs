﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eSplashScreenState
    {
        FADE_IN,
        HOLD,
        FADE_OUT,
        FINAL_FADE
    }

    class SplashFlowState : FlowState
    {
        /* -------- Private Fields -------- */
        private List<SplashScreen> splashScreens = new List<SplashScreen>();
        private int splashScreenIdx = 0;
        private eSplashScreenState splashScreenState = eSplashScreenState.FADE_IN;
        private float timer = 0f;

        /* -------- Constants -------- */
        private const float FADE_IN_TIME = 1.0f;
        private const float HOLD_TIME = 2.0f;
        private const float FADE_OUT_TIME = 1.0f;
        private const float FINAL_FADE_TIME = 2.0f;

        /* -------- Constructors -------- */
        public SplashFlowState()
        {
            // Create splash screens
            splashScreens.Add(new SplashScreen(ContentLoader.LoadTexture("Textures/Logos/UoBLogo"), Color.White));
            splashScreens.Add(new SplashScreen(ContentLoader.LoadTexture("Textures/Logos/VHLogoWithText"), Color.White));
        }

        /* -------- Public Methods -------- */
        public override void Update(GameTimeWrapper gtw, ComponentContainer cc)
        {
            if (!cc.SettingsComponent.ShowSplash)
            {
                cc.ProgramFlowComponent.RequestFlowStateChange(eFlowStateType.GAME);
                return;
            }

            timer += gtw.ElapsedSeconds;
            
            switch (splashScreenState) // splashScreenState changed within the switch-case block, so all cases must return rather than break!
            {
                case eSplashScreenState.FADE_IN:
                    if (timer >= FADE_IN_TIME)
                    {
                        timer = 0f;
                        splashScreenState = eSplashScreenState.HOLD;
                    }
                    return;
                case eSplashScreenState.HOLD:
                    if (timer >= HOLD_TIME)
                    {
                        timer = 0f;
                        splashScreenState = eSplashScreenState.FADE_OUT;
                    }
                    return;
                case eSplashScreenState.FADE_OUT:
                    if (timer >= FADE_OUT_TIME)
                    {
                        timer = 0f;
                        if (splashScreenIdx == splashScreens.Count - 1)
                        {
                            // We were on the last splash screen
                            splashScreenState = eSplashScreenState.FINAL_FADE;
                        }
                        else
                        {
                            // Still more splash screens to go
                            splashScreenState = eSplashScreenState.FADE_IN;
                            splashScreenIdx++;
                        }
                    }
                    return;
                case eSplashScreenState.FINAL_FADE:
                    if (timer >= FINAL_FADE_TIME)
                    {
                        // Finished the splash screens
                        cc.ProgramFlowComponent.RequestFlowStateChange(eFlowStateType.GAME);
                    }
                    return;
            }
        }
        public override void Draw(ComponentContainer cc)
        {
            if (!cc.SettingsComponent.ShowSplash)
            {
                cc.GraphicsComponent.GraphicsDevice.Clear(Color.Black);
                return;
            }

            SplashScreen ss = splashScreens[splashScreenIdx];
            float alpha;
            switch (splashScreenState)
            {
                case eSplashScreenState.FADE_IN:
                    alpha = Mathf.Clamp01(timer / FADE_IN_TIME);
                    ss.Draw(cc.GraphicsComponent, alpha);
                    break;
                case eSplashScreenState.HOLD:
                    alpha = 1f;
                    ss.Draw(cc.GraphicsComponent, alpha);
                    break;
                case eSplashScreenState.FADE_OUT:
                    alpha = 1f - Mathf.Clamp01(timer / FADE_OUT_TIME);
                    ss.Draw(cc.GraphicsComponent, alpha);
                    break;
                case eSplashScreenState.FINAL_FADE:
                    alpha = Mathf.Clamp01(timer / FINAL_FADE_TIME);
                    Color col = Color.Lerp(ss.BackgroundColor, Color.Black, alpha);
                    cc.GraphicsComponent.GraphicsDevice.SetRenderTarget(null);
                    cc.GraphicsComponent.GraphicsDevice.Clear(col);
                    break;
            }
        }
    }
}
