﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class GraphicsComponent
    {
        /* -------- Properties -------- */
        public GraphicsDevice GraphicsDevice { get; private set; }
        public SpriteBatch SpriteBatch { get; private set; }
        public int DeviceWidth { get; private set; }
        public int DeviceHeight { get; private set; }
        public int ScreenWidth { get; private set; }
        public int ScreenHeight { get; private set; }
        public float ScreenAspectRatio => ScreenWidth / (float)ScreenHeight;
        public int HalfScreenWidth => ScreenWidth / 2;
        public int HalfScreenHeight => ScreenHeight / 2;
        
        /* -------- Public Methods -------- */
        public void Initialize(GraphicsDevice inDevice)
        {
            GraphicsDevice = inDevice;

            DeviceWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            DeviceHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
        }
        public void LoadContent(SettingsComponent s)
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            ScreenWidth = s.ScreenWidth;
            ScreenHeight = s.ScreenHeight;
        }
    }
}
