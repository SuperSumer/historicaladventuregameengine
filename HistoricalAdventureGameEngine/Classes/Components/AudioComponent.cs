﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;

namespace HistoricalAdventureGameEngine
{
    class AudioComponent
    {
        /* -------- Private Fields -------- */
        private Dictionary<SoundEffect, SoundEffectInstance> soundEffectDict = new Dictionary<SoundEffect, SoundEffectInstance>();
        // Play parameters
        private float volume;
        private float pitch;
        private float pan;
        
        /* -------- Public Methods -------- */
        public void LoadContent(SettingsComponent s)
        {
            volume = s.Volume;
            pan = s.Pan;
            pitch = s.Pitch;
        }
        public void Update()
        {
            List<SoundEffect> removalList = new List<SoundEffect>();
            foreach (KeyValuePair<SoundEffect, SoundEffectInstance> kvp in soundEffectDict)
            {
                SoundEffectInstance sei = kvp.Value;
                if (sei.State == SoundState.Stopped)
                {
                    sei.Dispose();
                    removalList.Add(kvp.Key);
                }
            }
            foreach (SoundEffect se in removalList)
                soundEffectDict.Remove(se);
        }

        public void PlaySoundEffect_FireAndForget(SoundEffect se)
        {
            SoundEffectInstance sei;
            if (soundEffectDict.ContainsKey(se))
                sei = soundEffectDict[se];
            else
            {
                sei = se.CreateInstance();
                soundEffectDict.Add(se, sei);
            }

            sei.Volume = volume;
            sei.Pitch = pitch;
            sei.Pan = pan;
            sei.IsLooped = false;

            sei.Play();
        }
        public void PlaySoundEffect_Tracked(SoundEffect se, bool loop)
        {
            SoundEffectInstance sei;
            if (soundEffectDict.ContainsKey(se))
                sei = soundEffectDict[se];
            else
            {
                sei = se.CreateInstance();
                soundEffectDict.Add(se, sei);
            }

            sei.Volume = volume;
            sei.Pitch = pitch;
            sei.Pan = pan;
            sei.IsLooped = loop;

            sei.Play();
        }
        public void StopSoundEffect(SoundEffect se)
        {
            if (!soundEffectDict.ContainsKey(se))
                return;

            SoundEffectInstance sei = soundEffectDict[se];
            sei.Stop();
			sei.Dispose();
            soundEffectDict.Remove(se);
        }
        public void StopAllSoundEffects()
        {
            foreach (KeyValuePair<SoundEffect, SoundEffectInstance> kvp in soundEffectDict)
            {
                SoundEffectInstance sei = kvp.Value;
                sei.Stop();
                sei.Dispose();
            }

            soundEffectDict.Clear();
        }
        public void SetSoundEffectVolume(SoundEffect se, float newVolume)
        {
            if (!soundEffectDict.ContainsKey(se))
                return;

            SoundEffectInstance sei = soundEffectDict[se];
            sei.Volume = newVolume * volume;
        }
    }
}
