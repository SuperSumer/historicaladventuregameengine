﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class ComponentContainer
    {
        /* -------- Properties -------- */
        public GraphicsComponent GraphicsComponent { get; private set; } = new GraphicsComponent();
        public SettingsComponent SettingsComponent { get; private set; } = new SettingsComponent();
        public AudioComponent AudioComponent { get; private set; } = new AudioComponent();
        public InputComponent InputComponent { get; private set; } = new InputComponent();
        public ProgramFlowComponent ProgramFlowComponent { get; private set; }

        /* -------- Constructors -------- */
        public ComponentContainer(HistoricalAdventureGameEngine inGame)
        {
            ProgramFlowComponent = new ProgramFlowComponent(inGame);
        }

        /* -------- Public Methods -------- */
        public void Initialize(GraphicsDevice inDevice)
        {
            GraphicsComponent.Initialize(inDevice);
        }
        public void LoadContent()
        {
            SettingsComponent.LoadContent();
            InputComponent.LoadContent(SettingsComponent);
            GraphicsComponent.LoadContent(SettingsComponent);
            AudioComponent.LoadContent(SettingsComponent);
            ProgramFlowComponent.LoadContent(this);
        }
        public void Update(GameTimeWrapper gtw)
        {
            InputComponent.Update(this);
            ProgramFlowComponent.Update(gtw, this);
            AudioComponent.Update();
        }
        public void Draw()
        {
            ProgramFlowComponent.Draw(this);
        }
    }
}
