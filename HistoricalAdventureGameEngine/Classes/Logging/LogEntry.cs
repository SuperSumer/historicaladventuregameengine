﻿using System;

namespace HistoricalAdventureGameEngine
{
    class LogEntry
    {
        /* -------- Properties -------- */
        public DateTime Time { get; private set; }
        public string Text { get; private set; }

        /* -------- Constructors -------- */
        public LogEntry(string inText)
        {
            Time = DateTime.Now;
            Text = inText;
        }

        /* -------- Public Methods -------- */
        public string GetLogLine() => Time.ToLongTimeString() + " - " + Text;
    }
}
