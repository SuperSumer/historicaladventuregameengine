﻿using System;
using System.IO;
using Microsoft.VisualBasic;

namespace HistoricalAdventureGameEngine
{
    static class Logging
    {
        /* -------- Private Fields -------- */
        private static string participantID;
        private static bool recordLogs;
        private static string logPath;
        private static Log log;

        /* -------- Public Methods -------- */
        public static void Initialize(GameManagerContainer gmc, ComponentContainer cc)
        {
            recordLogs = cc.SettingsComponent.RecordLogs;
            logPath = cc.SettingsComponent.LogPath;

            if (recordLogs)
                log = new Log(gmc);
        }

        public static void Log(string msg)
        {
            if (!recordLogs)
                return;

            log.AddEntry(new LogEntry(msg));
        }
        public static void LogPlayStartTime()
        {
            if (!recordLogs)
                return;

            log.SetPlayStartTime();
        }
        public static void LogPlayEndTime()
        {
            if (!recordLogs)
                return;

            log.SetPlayEndTime();
        }
        public static void LogNewInfoNodeFound(InfoNode infoNode)
        {
            log?.IncrementInfoNodesCollected();

            string msg = $"Info node (id={infoNode.ID}) found for first time";
            Log(msg);
        }
        public static void LogInfoNodeRefound(InfoNode infoNode)
        {
            string msg = $"Info node (id={infoNode.ID}) found for subsequent time";
            Log(msg);
        }
        public static void LogIconActivated(IconSceneObject iso)
        {
            string msg = $"Icon scene object (id={iso.ID}) was activated";
            Log(msg);
        }
        public static void LogSceneChange(Scene oldScene, Scene newScene)
        {
            string msg;
            if (oldScene == null)
                msg = $"Changing from no scene to scene (id={newScene.ID})";
            else
                msg = $"Changing from scene (id={oldScene.ID}) to scene (id={newScene.ID})";
            Log(msg);
        }

        public static void DisplayParticipantIDMessageBox()
        {
            string input = Interaction.InputBox("Please enter participant ID", "Participant ID");
            if (string.IsNullOrEmpty(input))
                participantID = "UNKNOWN";
            else
                participantID = input;
        }

        public static void SaveLog()
        {
            if (!recordLogs)
                return;

            // Create directory
            string directoryPath = Path.Combine(logPath, participantID);
            if (!Directory.Exists(directoryPath))
            {
                try
                {
                    Directory.CreateDirectory(directoryPath);
                }
                catch (Exception e)
                {
                    Debug.LogWarning($"Tried to create new directory for saving log at {directoryPath} but caught exception {e.Message}");
                    return;
                }
            }

            // Create log file
            string fileName = log.GetFileName();
            string filePath = Path.Combine(directoryPath, fileName);
            try
            {
                using (StreamWriter writer = new StreamWriter(filePath, false))
                {
                    log.WriteLogToStream(writer);
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Tried to write log file at {filePath} but caught exception {e.Message}");
            }
        }
    }
}
