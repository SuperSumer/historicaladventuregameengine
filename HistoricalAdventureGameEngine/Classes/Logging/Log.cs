﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HistoricalAdventureGameEngine
{
    class Log
    {
        /* -------- Private Fields -------- */
        private DateTime startTime;
        private DateTime playStartTime;
        private DateTime playEndTime;
        private bool playStartTimeSet = false;
        private bool playEndTimeSet = false;
        private int numInfoNodes;
        private int infoNodesCollected = 0;
        private List<LogEntry> entries = new List<LogEntry>();

        /* -------- Constructors -------- */
        public Log(GameManagerContainer gmc)
        {
            startTime = DateTime.Now;
            numInfoNodes = gmc.InfoNodeManager.NumInfoNodes;
        }

        /* -------- Public Methods -------- */
        public void AddEntry(LogEntry entry)
        {
            entries.Add(entry);
        }
        public void SetPlayStartTime()
        {
            playStartTime = DateTime.Now;
            playStartTimeSet = true;
        }
        public void SetPlayEndTime()
        {
            playEndTime = DateTime.Now;
            playEndTimeSet = true;
        }
        public void IncrementInfoNodesCollected()
        {
            infoNodesCollected++;
        }

        public string GetFileName()
        {
            string fileName = "";
            fileName += startTime.Year.ToString("D4");
            fileName += "_";
            fileName += startTime.Month.ToString("D2");
            fileName += "_";
            fileName += startTime.Day.ToString("D2");
            fileName += "_";
            fileName += startTime.Hour.ToString("D2");
            fileName += "_";
            fileName += startTime.Minute.ToString("D2");
            fileName += "-Log.txt";

            return fileName;
        }

        public void WriteLogToStream(StreamWriter writer)
        {
            writer.WriteLine(GetFileName());
            writer.WriteLine();
            if (playStartTimeSet && playEndTimeSet)
                writer.WriteLine($"Play started at {playStartTime.ToShortTimeString()}, ended at {playEndTime.ToShortTimeString()}, lasted {(playEndTime - playStartTime).TotalMinutes} minutes");
            else
            {
                if (!playStartTimeSet && !playEndTimeSet)
                    writer.WriteLine("Neither play start time nor end time set");
                else
                {
                    if (!playStartTimeSet)
                        writer.WriteLine($"Play ended at {playEndTime.ToShortTimeString()}, start time not set");
                    if (!playEndTimeSet)
                        writer.WriteLine($"Play started at {playStartTime.ToShortTimeString()}, end time not set");
                }
            }
            writer.WriteLine($"Found {infoNodesCollected} of {numInfoNodes} info nodes");
            writer.WriteLine();
            writer.WriteLine("Entries:");
            foreach (LogEntry entry in entries)
                writer.WriteLine(entry.GetLogLine());
        }
    }
}
