﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eSceneObjectType
    {
        STATIC_SCENE_OBJECT,
        ANIMATED_SCENE_OBJECT,
        ICON_SCENE_OBJECT,
        PLAYER_CHARACTER_SCENE_OBJECT
    }

    abstract class SceneObject
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public Point Position { get; private set; }
        public float Depth { get; private set; }
        public float Scale { get; private set; }
        public bool Visible { get; protected set; } = true;
        public bool Active { get; protected set; } = true;
        public Point Offset { get; private set; } = Point.Zero;
        public Point OffsetPosition => Position + Offset;

        /* -------- Private Fields -------- */
        private StageDirectionActivator stageDirectionActivator = new StageDirectionActivator();
        protected bool startedInvisible = false;
        protected bool startedInactive = false;

        /* -------- Constants -------- */
        private const float DEFAULT_MAX_OFFSET = 100f;

        /* -------- Constructors -------- */
        public SceneObject(string inID, Point inPosition, float inDepth, float inScale, List<StageDirection> objectStageDirections, GameManagerContainer gmc, ComponentContainer cc)
        {
            ID = inID;
            Position = inPosition;
            Depth = inDepth;
            Scale = inScale;

            if (objectStageDirections?.Count > 0)
                stageDirectionActivator.LoadStageDirections(objectStageDirections, this, null, gmc, cc);
        }

        /* -------- Public Methods -------- */
        public virtual void Update(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            UpdateOffset(gmc.Camera, cc.SettingsComponent);

            if (Active && gmc.SceneManager.CanInteractWithScene)
                stageDirectionActivator.Update(this, gtw, gmc, cc);
        }
        public virtual void DrawRenderTarget(ArtStyle artStyle, GraphicsComponent g) {}

        public void SetVisibility(bool visible)
        {
            Visible = visible;
        }
        public void SetActive(bool active)
        {
            Active = active;
        }
        public void SetToInitialVisibilityAndActivity()
        {
            if (startedInvisible)
                Visible = false;
            if (startedInactive)
                Active = false;
        }

        /* -------- Private Methods -------- */
        private void UpdateOffset(Camera c, SettingsComponent s)
        {
            Vector2 offset = c.Position * DEFAULT_MAX_OFFSET * (Depth - 1f);
            offset.X *= s.ParallaxXMultiplier;
            offset.Y *= s.ParallaxYMultiplier;
            Offset = new Point(Mathf.Round(offset.X), -Mathf.Round(offset.Y));
        }

        /* -------- Static Methods -------- */
        public static int Compare(SceneObject so1, SceneObject so2)
        {
            if (so1.Depth < so2.Depth)
                return -1;
            if (so1.Depth > so2.Depth)
                return 1;
            return 0;
        }
        public static SceneObject CreateFromData(SceneObjectData data, Dictionary<string, Animation> animationDict, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (data == null)
                return null;

            eSceneObjectType type = EnumUtil.Parse<eSceneObjectType>(data.Type);

            switch (type)
            {
                case eSceneObjectType.STATIC_SCENE_OBJECT:
                    return StaticSceneObject.CreateFromData(data, gmc, cc);
                case eSceneObjectType.ANIMATED_SCENE_OBJECT:
                    return AnimatedSceneObject.CreateFromData(data, animationDict, gmc, cc);
                case eSceneObjectType.ICON_SCENE_OBJECT:
                    return IconSceneObject.CreateFromData(data, gmc, cc);
                case eSceneObjectType.PLAYER_CHARACTER_SCENE_OBJECT:
                    return PlayerCharacterSceneObject.CreateFromData(data, gmc, cc);
                default:
                    return null;
            }
        }
        public static SceneObject CreateFromReference(SceneObjectReference reference, Dictionary<string, SceneObject> sceneObjectDict)
        {
            if (!sceneObjectDict.ContainsKey(reference.ID))
                return null;

            SceneObject so = sceneObjectDict[reference.ID].MemberwiseClone() as SceneObject;
            if (so == null)
                return null;

            // Set position
            so.Position = reference.Position;
            so.Depth = reference.Depth;
            so.Scale = reference.Scale;

            return so;
        }
    }
}
