﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace HistoricalAdventureGameEngine
{
    abstract class ActivatableUISceneObject : UISceneObject
    {
        /* -------- Private Fields -------- */
        private List<StageDirection> activationStageDirections;

        /* -------- Constants -------- */
        private const float DEFAULT_ACTIVATION_DISTANCE = 50;

        /* -------- Constructors -------- */
        public ActivatableUISceneObject(string inID, Point inPosition, float inDepth, float inScale, List<StageDirection> objectStageDirections, List<StageDirection> inActivationStageDirections, GameManagerContainer gmc, ComponentContainer cc)
            : base(inID, inPosition, inDepth, inScale, objectStageDirections, gmc, cc)
        {
            activationStageDirections = inActivationStageDirections;
        }

        /* -------- Public Methods -------- */
        public abstract void OnMouseOver(GameManagerContainer gmc);
        public abstract void OnMouseOut(GameManagerContainer gmc);
        public virtual void OnActivate(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            gmc.Player.LoadStageDirections(activationStageDirections, this, gtw, gmc, cc);
        }

        public virtual float GetMinDistanceForActivation() => DEFAULT_ACTIVATION_DISTANCE;
    }
}
