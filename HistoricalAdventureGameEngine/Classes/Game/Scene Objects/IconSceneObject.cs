﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eIconType
    {
        INFORMATION,
        MOVE_UP,
        MOVE_UP_RIGHT,
        MOVE_RIGHT,
        MOVE_DOWN_RIGHT,
        MOVE_DOWN,
        MOVE_DOWN_LEFT,
        MOVE_LEFT,
        MOVE_UP_LEFT,
        PICK_UP,
        TALK,
        CUNEIFORM
    }

    class IconSceneObject : ActivatableUISceneObject
    {
        /* -------- Properties -------- */
        public eIconType Type { get; private set; }

        /* -------- Private Fields -------- */
        private float size;
        private float alpha;
        private bool mouseOver;
        private bool activating;

        /* -------- Constants -------- */
        private const float DEFAULT_SIZE = 120f;
        private const float DEFAULT_ALPHA = 0.5f;
        private const float SIZE_MULTIPLIER_AT_MAX_DEPTH = 0.4f;
        private const float SIZE_MULTIPLIER_AT_LOW_PROXIMITY = 0.9f;
        private const float ALPHA_MULTIPLIER_AT_LOW_PROXIMITY = 0.8f;
        private const float SIZE_MULTIPLIER_AT_MOUSE_OVER = 1.4f;
        private const float SIZE_MULTIPLIER_ON_TAP = 1.2f;
        private const float ALPHA_AT_MOUSE_OVER = 1.0f;
        private const float ALPHA_ON_TAP = 1.0f;
        private const float MOUSE_DISTANCE_FOR_LOW_PROXIMITY = 100f;
        private const float DEPTH_DIFFERENCE_FOR_LOW_PROXIMITY = 0.2f;
        private const float SIZE_LERP_RATE = 10.0f;
        private const float ALPHA_LERP_RATE = 10.0f;

        /* -------- Constructors -------- */
        public IconSceneObject(string inID, Point inPosition, float inDepth, float inScale, eIconType inType, List<StageDirection> objectStageDirections, List<StageDirection> inActivationStageDirections, GameManagerContainer gmc, ComponentContainer cc)
            : base(inID, inPosition, inDepth, inScale, objectStageDirections, inActivationStageDirections, gmc, cc)
        {
            Type = inType;
        }

        /* -------- Public Methods -------- */
        public override void Update(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (Visible)
                UpdateSizeAndAlpha(gtw, gmc, cc);

            activating = false;

            base.Update(gtw, gmc, cc);
        }
        public override void SBDrawUI(float uiAlpha, SpriteBatch sb, GameManagerContainer gmc)
        {
            if (!Visible)
                return;

            Texture2D tex;
            switch (Type)
            {
                case eIconType.INFORMATION:
                    tex = gmc.UIContentManager.InformationIconTexture;
                    break;
                case eIconType.PICK_UP:
                    tex = gmc.UIContentManager.PickUpIconTexture;
                    break;
                case eIconType.TALK:
                    tex = gmc.UIContentManager.TalkIconTexture;
                    break;
                case eIconType.CUNEIFORM:
                    tex = gmc.UIContentManager.CuneiformIconTexture;
                    break;
                default:
                    tex = gmc.UIContentManager.MoveIconTexture;
                    break;
            }

            float rotation;
            switch (Type)
            {
                case eIconType.MOVE_RIGHT:
                    rotation = 0f;
                    break;
                case eIconType.MOVE_DOWN_RIGHT:
                    rotation = Mathf.PiByFour;
                    break;
                case eIconType.MOVE_DOWN:
                    rotation = Mathf.PiByTwo;
                    break;
                case eIconType.MOVE_DOWN_LEFT:
                    rotation = 3f * Mathf.PiByFour;
                    break;
                case eIconType.MOVE_LEFT:
                    rotation = Mathf.Pi;
                    break;
                case eIconType.MOVE_UP_LEFT:
                    rotation = 5f * Mathf.PiByFour;
                    break;
                case eIconType.MOVE_UP:
                    rotation = 3f * Mathf.PiByTwo;
                    break;
                case eIconType.MOVE_UP_RIGHT:
                    rotation = 7f * Mathf.PiByFour;
                    break;
                default:
                    rotation = 0f;
                    break;
            }

            Vector2 pos = new Vector2(OffsetPosition.X, OffsetPosition.Y);
            Vector2 origin = new Vector2(tex.Width / 2f, tex.Height / 2f);
            Color tint = Color.White * alpha * uiAlpha;
            float scale = size / tex.Width;
            sb.Draw(tex, pos, null, tint, rotation, origin, scale, SpriteEffects.None, 0f);
        }

        public override void OnMouseOver(GameManagerContainer gmc)
        {
            mouseOver = true;
        }
        public override void OnMouseOut(GameManagerContainer gmc)
        {
            mouseOver = false;
        }
        public override void OnActivate(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            activating = true;

            if (gmc.UIContentManager.IconActivateSoundEffect != null)
                cc.AudioComponent.PlaySoundEffect_FireAndForget(gmc.UIContentManager.IconActivateSoundEffect);

            Logging.LogIconActivated(this);

            base.OnActivate(gtw, gmc, cc);
        }

        /* -------- Private Methods -------- */
        private void UpdateSizeAndAlpha(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            float targetSize = DEFAULT_SIZE;
            targetSize *= Scale * Mathf.Lerp(1f, SIZE_MULTIPLIER_AT_MAX_DEPTH, Depth);
            float targetAlpha = DEFAULT_ALPHA;

            // If activating by tapping
            bool touchScreen = cc.SettingsComponent.UseTouchScreen;
            if (activating && touchScreen)
            {
                targetSize *= SIZE_MULTIPLIER_ON_TAP;
                targetAlpha = ALPHA_ON_TAP;
                size = targetSize;
                alpha = targetAlpha;
                return;
            }

            if (mouseOver && !touchScreen)
            {
                targetSize *= SIZE_MULTIPLIER_AT_MOUSE_OVER;
                targetAlpha = ALPHA_AT_MOUSE_OVER;
            }
            else
            {
                // Calculate proximity
                Point sceneMousePos = gmc.SceneManager.ActiveScene.ConvertScreenPosToScenePos(cc.InputComponent.MousePos);
                float distance = Mathf.PointToPointDistance(sceneMousePos, OffsetPosition);
                float distanceProximity = touchScreen ? 0f : 1f - Mathf.GetLerpFactor(0f, MOUSE_DISTANCE_FOR_LOW_PROXIMITY, distance, true);
                float depthDifference = Mathf.Abs(gmc.Camera.FocalDepth - Depth);
                float depthProximity = 1f - Mathf.GetLerpFactor(0f, DEPTH_DIFFERENCE_FOR_LOW_PROXIMITY, depthDifference, true);
                float proximity = Mathf.Max(distanceProximity, depthProximity);

                targetSize *= Mathf.Lerp(SIZE_MULTIPLIER_AT_LOW_PROXIMITY, 1f, proximity);
                targetAlpha *= Mathf.Lerp(ALPHA_MULTIPLIER_AT_LOW_PROXIMITY, 1f, proximity);
            }

            size = Mathf.Lerp(size, targetSize, gtw.ElapsedSeconds * SIZE_LERP_RATE, true);
            alpha = Mathf.Lerp(alpha, targetAlpha, gtw.ElapsedSeconds * ALPHA_LERP_RATE, true);
        }

        /* -------- Static Methods -------- */
        public static IconSceneObject CreateFromData(SceneObjectData data, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (data.IconSceneObjectData == null)
                return null;

            eIconType iconType = EnumUtil.Parse<eIconType>(data.IconSceneObjectData.IconType);
            List<StageDirection> objectStageDirections = StageDirection.CreateFromData(data.ObjectStageDirections, gmc);
            List<StageDirection> activationStageDirections = StageDirection.CreateFromData(data.IconSceneObjectData.ActivationStageDirections, gmc);
            IconSceneObject iconSceneObject = new IconSceneObject(data.ID, data.Position, data.Depth, data.Scale, iconType, objectStageDirections, activationStageDirections, gmc, cc);
            if (data.StartInvisible)
            {
                iconSceneObject.Visible = false;
                iconSceneObject.startedInvisible = true;
            }
            if (data.StartInactive)
            {
                iconSceneObject.Active = false;
                iconSceneObject.startedInactive = true;
            }

            return iconSceneObject;
        }
    }
}
