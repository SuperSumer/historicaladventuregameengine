﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class AnimatedSceneObject : GeometrySceneObject
    {
        /* -------- Private Fields -------- */
        private AnimationPlayState animationPlayState;
        private RenderTarget2D renderTarget;

        /* -------- Constructors -------- */
        public AnimatedSceneObject(string inID, Point inPosition, float inDepth, float inScale, Animation inAnimation, List<StageDirection> objectStageDirections, ComponentContainer cc, GameManagerContainer gmc)
            : base(inID, inPosition, inDepth, inScale, objectStageDirections, gmc, cc)
        {
            animationPlayState = new AnimationPlayState(inAnimation, cc.SettingsComponent);
            renderTarget = new RenderTarget2D(cc.GraphicsComponent.GraphicsDevice, inAnimation.Width, inAnimation.Height);
        }
        ~AnimatedSceneObject()
        {
            renderTarget.Dispose();
        }

        /* -------- Public Methods -------- */
        public override void Update(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (Visible && Active)
                animationPlayState.Update(gtw);

            base.Update(gtw, gmc, cc);
        }
        public override void DrawRenderTarget(ArtStyle artStyle, GraphicsComponent g)
        {
            base.DrawRenderTarget(artStyle, g);

            animationPlayState.Render(renderTarget, artStyle, g);
        }
        public override void SBDrawGeometry(SpriteBatch sb, Color lightColor)
        {
            if (!Visible)
                return;

            Rectangle rect = new Rectangle(OffsetPosition.X, OffsetPosition.Y, Mathf.Round(renderTarget.Width * Scale), Mathf.Round(renderTarget.Height * Scale));
            sb.Draw(renderTarget, rect, lightColor);
        }
        public override void SBDrawDepth(SpriteBatch sb)
        {
            if (!Visible)
                return;

            Rectangle rect = new Rectangle(OffsetPosition.X, OffsetPosition.Y, Mathf.Round(renderTarget.Width * Scale), Mathf.Round(renderTarget.Height * Scale));
            Color tint = new Color(Depth, Depth, Depth, 1f);
            sb.Draw(renderTarget, rect, tint);
        }

        /* -------- Static Methods -------- */
        public new static AnimatedSceneObject CreateFromData(SceneObjectData data, Dictionary<string, Animation> animationDict, GameManagerContainer gmc, ComponentContainer cc)
        {
            AnimatedSceneObjectData asid = data.AnimatedSceneObjectData;
            if (asid == null)
                return null;

            Animation animation = null;
            if (asid.Animation != null)
                animation = Animation.CreateFromData(asid.Animation, cc.SettingsComponent);
            if (animation == null && !string.IsNullOrEmpty(asid.AnimationID) && animationDict.ContainsKey(asid.AnimationID))
                animation = animationDict[asid.AnimationID];
            if (animation == null)
                return null;

            List<StageDirection> stageDirections = StageDirection.CreateFromData(data.ObjectStageDirections, gmc);

            AnimatedSceneObject animatedSceneObject = new AnimatedSceneObject(data.ID, data.Position, data.Depth, data.Scale, animation, stageDirections, cc, gmc);
            if (data.StartInvisible)
            {
                animatedSceneObject.Visible = false;
                animatedSceneObject.startedInvisible = true;
            }
            if (data.StartInactive)
            {
                animatedSceneObject.Active = false;
                animatedSceneObject.startedInactive = true;
            }

            return animatedSceneObject;
        }
    }
}
