﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    abstract class GeometrySceneObject : SceneObject
    {
        /* -------- Constructors -------- */
        public GeometrySceneObject(string inID, Point inPosition, float inDepth, float inScale, List<StageDirection> objectStageDirections, GameManagerContainer gmc, ComponentContainer cc)
            : base(inID, inPosition, inDepth, inScale, objectStageDirections, gmc, cc)
        {
        }

        /* -------- Public Methods -------- */
        public abstract void SBDrawGeometry(SpriteBatch sb, Color lightColor);
        public abstract void SBDrawDepth(SpriteBatch sb);
    }
}
