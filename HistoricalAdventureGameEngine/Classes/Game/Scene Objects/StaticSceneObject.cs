﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class StaticSceneObject : GeometrySceneObject
    {
        /* -------- Private Fields -------- */
        private Texture2D texture;

        /* -------- Constructors -------- */
        public StaticSceneObject(string inID, Point inPosition, float inDepth, float inScale, string textureIDOrPath, List<StageDirection> objectStageDirections, GameManagerContainer gmc, ComponentContainer cc)
            : base(inID, inPosition, inDepth, inScale, objectStageDirections, gmc, cc)
        {
            texture = ContentLoader.LoadTexture(textureIDOrPath);
        }

        /* -------- Public Methods -------- */
        public override void SBDrawGeometry(SpriteBatch sb, Color lightColor)
        {
            if (!Visible)
                return;

            Rectangle rect = new Rectangle(OffsetPosition.X, OffsetPosition.Y, Mathf.Round(texture.Width * Scale), Mathf.Round(texture.Height * Scale));
            sb.Draw(texture, rect, lightColor);
        }
        public override void SBDrawDepth(SpriteBatch sb)
        {
            if (!Visible)
                return;

            Rectangle rect = new Rectangle(OffsetPosition.X, OffsetPosition.Y, Mathf.Round(texture.Width * Scale), Mathf.Round(texture.Height * Scale));
            Color tint = new Color(Depth, Depth, Depth, 1f);
            sb.Draw(texture, rect, tint);
        }

        /* -------- Static Methods -------- */
        public static StaticSceneObject CreateFromData(SceneObjectData data, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (data.StaticSceneObjectData == null)
                return null;

            List<StageDirection> stageDirections = StageDirection.CreateFromData(data.ObjectStageDirections, gmc);
            StaticSceneObject staticSceneObject = new StaticSceneObject(data.ID, data.Position, data.Depth, data.Scale, data.StaticSceneObjectData.TextureIDOrPath, stageDirections, gmc, cc);
            if (data.StartInvisible)
            {
                staticSceneObject.Visible = false;
                staticSceneObject.startedInvisible = true;
            }
            if (data.StartInactive)
            {
                staticSceneObject.Active = false;
                staticSceneObject.startedInactive = true;
            }

            return staticSceneObject;
        }
    }
}
