﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class PlayerCharacterSceneObject : GeometrySceneObject
    {
        /* -------- Private Fields -------- */
        private Texture2D texture;
        private string lastSceneID;
        private bool showing;

        /* -------- Constructors -------- */
        public PlayerCharacterSceneObject(string inID, Point inPosition, float inDepth, float inScale, string textureIDOrPath, string inLastSceneID, List<StageDirection> objectStageDirections, GameManagerContainer gmc, ComponentContainer cc)
            : base(inID, inPosition, inDepth, inScale, objectStageDirections, gmc, cc)
        {
            texture = ContentLoader.LoadTexture(textureIDOrPath);
            lastSceneID = inLastSceneID;
        }

        /* -------- Public Methods -------- */
        public override void SBDrawGeometry(SpriteBatch sb, Color lightColor)
        {
            if (!Visible || !showing)
                return;

            Rectangle rect = new Rectangle(OffsetPosition.X, OffsetPosition.Y, Mathf.Round(texture.Width * Scale), Mathf.Round(texture.Height * Scale));
            sb.Draw(texture, rect, lightColor);
        }
        public override void SBDrawDepth(SpriteBatch sb)
        {
            if (!Visible || !showing)
                return;

            Rectangle rect = new Rectangle(OffsetPosition.X, OffsetPosition.Y, Mathf.Round(texture.Width * Scale), Mathf.Round(texture.Height * Scale));
            Color tint = new Color(Depth, Depth, Depth, 1f);
            sb.Draw(texture, rect, tint);
        }

        public void SetShowing(string playerLastSceneID)
        {
            if (string.IsNullOrEmpty(lastSceneID))
            {
                showing = true;
                return;
            }

            showing = (playerLastSceneID == lastSceneID);
        }

        /* -------- Static Methods -------- */
        public static PlayerCharacterSceneObject CreateFromData(SceneObjectData data, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (data.PlayerCharacterSceneObjectData == null)
                return null;

            List<StageDirection> stageDirections = StageDirection.CreateFromData(data.ObjectStageDirections, gmc);
            PlayerCharacterSceneObject staticSceneObject = new PlayerCharacterSceneObject(data.ID, data.Position, data.Depth, data.Scale, data.PlayerCharacterSceneObjectData.TextureIDOrPath, data.PlayerCharacterSceneObjectData.LastSceneID, stageDirections, gmc, cc);
            if (data.StartInvisible)
            {
                staticSceneObject.Visible = false;
                staticSceneObject.startedInvisible = true;
            }
            if (data.StartInactive)
            {
                staticSceneObject.Active = false;
                staticSceneObject.startedInactive = true;
            }

            return staticSceneObject;
        }
    }
}
