﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    abstract class UISceneObject : SceneObject
    {
        /* -------- Constructors -------- */
        public UISceneObject(string inID, Point inPosition, float inDepth, float inScale, List<StageDirection> objectStageDirections, GameManagerContainer gmc, ComponentContainer cc)
            : base(inID, inPosition, inDepth, inScale, objectStageDirections, gmc, cc)
        {
        }

        /* -------- Public Methods -------- */
        public abstract void SBDrawUI(float uiAlpha, SpriteBatch sb, GameManagerContainer gmc);
    }
}
