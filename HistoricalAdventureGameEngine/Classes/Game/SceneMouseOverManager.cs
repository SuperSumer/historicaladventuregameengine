﻿using Microsoft.Xna.Framework;

namespace HistoricalAdventureGameEngine
{
    class SceneMouseOverManager
    {
        /* -------- Properties -------- */
        public ActivatableUISceneObject MouseOverObject { get; private set; }
        public Item MouseOverItem { get; private set; }

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            ObjectPopulatedScene ops = gmc.SceneManager.ActiveScene as ObjectPopulatedScene;
            if (ops == null)
            {
                MouseOverObject = null;
                return;
            }

            // Get the new mouse over objects
            Item newMouseOverItem = CanInteractWithUI(gmc) ? SelectMouseOverItem(gmc, cc) : null;
            ActivatableUISceneObject newMouseOverObject = (CanInteractWithUI(gmc) && newMouseOverItem == null) ? SelectMouseOverObject(ops, cc) : null;

            // Convert mouse over items to activatable items
            ActivatableItem mouseOverActivatableItem = MouseOverItem as ActivatableItem;
            ActivatableItem newMouseOverActivatableItem = newMouseOverItem as ActivatableItem;

            // Fire relevant activations
            if (newMouseOverActivatableItem != mouseOverActivatableItem)
            {
                // A different mouse over item
                mouseOverActivatableItem?.OnMouseOut(gmc);
                newMouseOverActivatableItem?.OnMouseOver(gmc);
            }
            if (newMouseOverObject != MouseOverObject)
            {
                // A different mouse over object
                MouseOverObject?.OnMouseOut(gmc);
                newMouseOverObject?.OnMouseOver(gmc);
            }
            if (cc.InputComponent.MouseButtonIsPressing(eMouseButton.LEFT))
            {
                // Mouse click
                newMouseOverActivatableItem?.OnActivate(gtw, gmc, cc);
                newMouseOverObject?.OnActivate(gtw, gmc, cc);
            }

            MouseOverItem = newMouseOverItem;
            MouseOverObject = newMouseOverObject;
        }

        /* -------- Private Methods -------- */
        private Item SelectMouseOverItem(GameManagerContainer gmc, ComponentContainer cc)
        {
            Scene scene = gmc.SceneManager.ActiveScene;
            Point sceneMousePos = scene.ConvertScreenPosToScenePos(cc.InputComponent.MousePos);

            return scene.GetMouseOverItem(sceneMousePos, gmc);
        }
        private ActivatableUISceneObject SelectMouseOverObject(ObjectPopulatedScene ops, ComponentContainer cc)
        {
            Point sceneMousePos = ops.ConvertScreenPosToScenePos(cc.InputComponent.MousePos);

            ActivatableUISceneObject newMouseOverObject = null;
            float minDist = float.MaxValue;

            foreach (SceneObject so in ops.SceneObjects)
            {
                ActivatableUISceneObject auiso = so as ActivatableUISceneObject;
                if (auiso == null || !auiso.Visible || !auiso.Active)
                    continue;

                float dist = Mathf.PointToPointDistance(so.OffsetPosition, sceneMousePos);
                if (dist < auiso.GetMinDistanceForActivation() && dist < minDist)
                {
                    newMouseOverObject = auiso;
                    minDist = dist;
                }
            }

            return newMouseOverObject;
        }

        private bool CanInteractWithUI(GameManagerContainer gmc)
        {
            if (!gmc.SceneManager.CanInteractWithScene)
                return false;

            if (gmc.InfoMessageManager.InfoMessageIsActive || gmc.InfoNodeManager.InfoNodeIsActive)
                return false;

            return true;
        }
    }
}
