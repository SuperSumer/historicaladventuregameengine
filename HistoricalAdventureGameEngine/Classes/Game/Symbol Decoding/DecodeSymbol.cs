﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class DecodeSymbol
    {
        /* -------- Properties -------- */
        public string Name { get; private set; }
        public Texture2D SymbolTexture { get; private set; }
        public Texture2D DecodeTexture { get; private set; }

        /* -------- Constructors -------- */
        public DecodeSymbol(string inName, string symbolTextureIDOrPath, string decodeTextureIDOrPath)
        {
            Name = inName;
            SymbolTexture = ContentLoader.LoadTexture(symbolTextureIDOrPath);
            DecodeTexture = ContentLoader.LoadTexture(decodeTextureIDOrPath);
        }

        /* -------- Static Methods -------- */
        public static DecodeSymbol CreateFromData(DecodeSymbolData data)
        {
            if (data == null)
                return null;

            DecodeSymbol ds = new DecodeSymbol(data.Text, data.SymbolTextureIDOrPath, data.DecodeTextureIDOrPath);
            return ds;
        }
        public static List<DecodeSymbol> CreateFromData(DecodeSymbolData[] datas)
        {
            List<DecodeSymbol> decodes = new List<DecodeSymbol>();

            if (datas == null)
                return decodes;

            foreach (DecodeSymbolData data in datas)
            {
                DecodeSymbol decode = CreateFromData(data);
                if (decode != null)
                    decodes.Add(decode);
            }

            return decodes;
        }
    }
}
