﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eInfoMessageManagerMode
    {
        IDLE,
        SHOWING_INFO_MESSAGE_FIRST_FRAME,
        SHOWING_INFO_MESSAGE
    }

    class InfoMessageManager
    {
        /* -------- Properties -------- */
        public bool InfoMessageIsActive => mode == eInfoMessageManagerMode.SHOWING_INFO_MESSAGE_FIRST_FRAME || mode == eInfoMessageManagerMode.SHOWING_INFO_MESSAGE;

        /* -------- Private Fields -------- */
        private eInfoMessageManagerMode mode = eInfoMessageManagerMode.IDLE;
        private string activeInfoMessage;
        private string activeInfoMessageString;
        private Texture2D activeTexture;
        private bool lineSet;

        /* -------- Constants -------- */
        private const string CONTINUE_MESSAGE = "Click to Continue";
        private const string TOUCH_SCREEN_CONTINUE_MESSAGE = "Tap to Continue";

        /* -------- Public Methods -------- */
        public void Update(InputComponent i)
        {
            switch (mode)
            {
                case eInfoMessageManagerMode.IDLE:
                    break;
                case eInfoMessageManagerMode.SHOWING_INFO_MESSAGE_FIRST_FRAME:
                    mode = eInfoMessageManagerMode.SHOWING_INFO_MESSAGE;
                    break;
                case eInfoMessageManagerMode.SHOWING_INFO_MESSAGE:
                    if (i.MouseButtonIsPressing(eMouseButton.LEFT))
                    {
                        mode = eInfoMessageManagerMode.IDLE;
                        activeInfoMessage = null;
                    }
                    break;
                default:
                    break;
            }
        }
        public void SBDrawUI(GameManagerContainer gmc, ComponentContainer cc)
        {
            if (!InfoMessageIsActive)
                return;
            
            UIContentManager ui = gmc.UIContentManager;
            SpriteFont sf = ui.InfoMessageSpriteFont;
            Color col = ui.InfoMessageTextColor;
            Vector2 pos;
            int halfWidth = gmc.SceneManager.ActiveScene.Width / 2;
            int halfHeight = gmc.SceneManager.ActiveScene.Height / 2;

            // Draw box
            Texture2D tex = ui.InfoMessageTexture;
            Rectangle rect = new Rectangle(halfWidth - tex.Width / 2, halfHeight - tex.Height / 2, tex.Width, tex.Height);
            cc.GraphicsComponent.SpriteBatch.Draw(tex, rect, Color.White);

            if (activeTexture != null)
                SBDrawTexture(gmc, cc.GraphicsComponent);
            else
                SBDrawText(gmc, cc.GraphicsComponent);

            // Continue message
            Vector2 continueMessageSize = sf.MeasureString(GetContinueMessage(cc.SettingsComponent));
            pos = new Vector2(halfWidth - continueMessageSize.X / 2f, halfHeight + ui.InfoMessageBoxHeight / 2 - continueMessageSize.Y);
            cc.GraphicsComponent.SpriteBatch.DrawString(sf, GetContinueMessage(cc.SettingsComponent), pos, col);
        }

        public void ActivateInfoMessage(string infoMessage)
        {
            activeInfoMessage = infoMessage;
            activeTexture = null;
            mode = eInfoMessageManagerMode.SHOWING_INFO_MESSAGE_FIRST_FRAME;
            lineSet = false;
        }
        public void ActivateTextureInfoMessage(string textureIDOrPath)
        {
            activeInfoMessage = "";
            activeTexture = ContentLoader.LoadTexture(textureIDOrPath);
            mode = eInfoMessageManagerMode.SHOWING_INFO_MESSAGE_FIRST_FRAME;
        }

        /* -------- Private Methods -------- */
        private string GetContinueMessage(SettingsComponent s) => s.UseTouchScreen ? TOUCH_SCREEN_CONTINUE_MESSAGE : CONTINUE_MESSAGE;

        private void SetLine(GameManagerContainer gmc, GraphicsComponent g)
        {
            activeInfoMessageString = StringUtil.InsertLineBreaksForWidth(activeInfoMessage, gmc.UIContentManager.InfoMessageSpriteFont, gmc.UIContentManager.InfoMessageBoxWidth);
            lineSet = true;
        }

        private void SBDrawTexture(GameManagerContainer gmc, GraphicsComponent g)
        {
            UIContentManager ui = gmc.UIContentManager;
            int halfWidth = gmc.SceneManager.ActiveScene.Width / 2;
            int halfHeight = gmc.SceneManager.ActiveScene.Height / 2;

            Rectangle rect = new Rectangle(halfWidth - activeTexture.Width / 2, halfHeight - ui.InfoMessageBoxHeight / 2, activeTexture.Width, activeTexture.Height);
            g.SpriteBatch.Draw(activeTexture, rect, Color.White);
        }
        private void SBDrawText(GameManagerContainer gmc, GraphicsComponent g)
        {
            UIContentManager ui = gmc.UIContentManager;
            SpriteFont sf = ui.InfoMessageSpriteFont;
            Color col = ui.InfoMessageTextColor;
            Vector2 pos;
            int halfWidth = gmc.SceneManager.ActiveScene.Width / 2;
            int halfHeight = gmc.SceneManager.ActiveScene.Height / 2;

            if (!lineSet)
                SetLine(gmc, g);

            // Info message
            pos = new Vector2(halfWidth - ui.InfoMessageBoxWidth / 2, halfHeight - ui.InfoMessageBoxHeight / 2);
            g.SpriteBatch.DrawString(sf, activeInfoMessageString, pos, col);
        }
    }
}
