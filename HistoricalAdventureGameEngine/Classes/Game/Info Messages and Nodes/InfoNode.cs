﻿using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class InfoNode
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public string Title { get; private set; }
        public string Body { get; private set; }

        /* -------- Constructors -------- */
        public InfoNode(string inID, string inTitle, string inBody)
        {
            ID = inID;
            Title = inTitle;
            Body = inBody;
        }

        /* -------- Static Methods -------- */
        public static InfoNode CreateFromData(InfoNodeData data) => new InfoNode(data.ID, data.Title, data.Body);
    }
}
