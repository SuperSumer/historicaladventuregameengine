﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eInfoNodeManagerMode
    {
        IDLE,
        SHOWING_INFO_MESSAGE_FIRST_FRAME,
        SHOWING_INFO_MESSAGE
    }

    class InfoNodeManager
    {
        /* -------- Properties -------- */
        public bool InfoNodeIsActive => mode == eInfoNodeManagerMode.SHOWING_INFO_MESSAGE_FIRST_FRAME || mode == eInfoNodeManagerMode.SHOWING_INFO_MESSAGE;
        public int NumInfoNodes => infoNodes.Count;
        public int ScorePerInfoNode { get; set; }

        /* -------- Private Fields -------- */
        private eInfoNodeManagerMode mode;
        private InfoNode activeInfoNode;
        private string activeInfoNodeString;
        private bool lineSet;
        private List<InfoNode> infoNodesFound = new List<InfoNode>();
        private List<InfoNode> infoNodes = new List<InfoNode>();

        /* -------- Constants -------- */
        private const string CONTINUE_MESSAGE = "Click to Continue";
        private const string TOUCH_SCREEN_CONTINUE_MESSAGE = "Tap to Continue";
        private const int TITLES_GAP = 2;
        private const int BODY_GAP = 40;

        /* -------- Public Methods -------- */
        public void Update(InputComponent i)
        {
            switch (mode)
            {
                case eInfoNodeManagerMode.IDLE:
                    break;
                case eInfoNodeManagerMode.SHOWING_INFO_MESSAGE_FIRST_FRAME:
                    mode = eInfoNodeManagerMode.SHOWING_INFO_MESSAGE;
                    break;
                case eInfoNodeManagerMode.SHOWING_INFO_MESSAGE:
                    if (i.MouseButtonIsPressing(eMouseButton.LEFT))
                    {
                        mode = eInfoNodeManagerMode.IDLE;
                        activeInfoNode = null;
                    }
                    break;
                default:
                    break;
            }
        }
        public void SBDrawUI(GameManagerContainer gmc, ComponentContainer cc)
        {
            if (!InfoNodeIsActive)
                return;

            if (!lineSet)
                SetLine(gmc, cc.GraphicsComponent);

            UIContentManager ui = gmc.UIContentManager;
            SpriteFont sf = ui.InfoNodeSpriteFont;
            Color col = ui.InfoNodeTextColor;
            Vector2 pos;
            int halfWidth = gmc.SceneManager.ActiveScene.Width / 2;
            int halfHeight = gmc.SceneManager.ActiveScene.Height / 2;

            // Draw box
            Texture2D tex = ui.InfoNodeTexture;
            Rectangle rect = new Rectangle(halfWidth - tex.Width / 2, halfHeight - tex.Height / 2, tex.Width, tex.Height);
            cc.GraphicsComponent.SpriteBatch.Draw(tex, rect, Color.White);

            float y = halfHeight - ui.InfoNodeBoxHeight / 2f;

            // Number found
            string numFoundString = $"{infoNodesFound.Count} of {infoNodes.Count} Found";
            Vector2 numFoundSize = sf.MeasureString(numFoundString);
            pos = new Vector2(halfWidth - numFoundSize.X / 2f, y);
            cc.GraphicsComponent.SpriteBatch.DrawString(sf, numFoundString, pos, col);

            y += numFoundSize.Y + TITLES_GAP;

            // Title
            Vector2 titleSize = sf.MeasureString(activeInfoNode.Title);
            pos = new Vector2(halfWidth - titleSize.X / 2f, y);
            cc.GraphicsComponent.SpriteBatch.DrawString(sf, activeInfoNode.Title, pos, col);

            y += titleSize.Y + BODY_GAP;

            // Body
            pos = new Vector2(halfWidth - ui.InfoNodeBoxWidth / 2f, y);
            cc.GraphicsComponent.SpriteBatch.DrawString(sf, activeInfoNodeString, pos, col);

            // Continue message
            Vector2 continueMessageSize = sf.MeasureString(GetContinueMessage(cc.SettingsComponent));
            pos = new Vector2(halfWidth - continueMessageSize.X / 2f, halfHeight + ui.InfoNodeBoxHeight / 2f - continueMessageSize.Y);
            cc.GraphicsComponent.SpriteBatch.DrawString(sf, GetContinueMessage(cc.SettingsComponent), pos, col);
        }

        public void ActivateInfoNode(InfoNode infoNode, GameManagerContainer gmc)
        {
            gmc.ScoreManager.IncreaseScore(ScorePerInfoNode);

            activeInfoNode = infoNode;
            if (infoNodesFound.Contains(infoNode))
                Logging.LogInfoNodeRefound(infoNode);
            else
            {
                Logging.LogNewInfoNodeFound(infoNode);
                infoNodesFound.Add(infoNode);
            }
            mode = eInfoNodeManagerMode.SHOWING_INFO_MESSAGE_FIRST_FRAME;
            lineSet = false;
        }
        public void DeactivateInfoNode(InfoNode infoNode, GameManagerContainer gmc)
        {
            gmc.ScoreManager.IncreaseScore(-ScorePerInfoNode);
            infoNodesFound.Remove(infoNode);
        }

        public void LoadData(InfoNodeData[] datas)
        {
            if (datas == null)
                return;

            foreach (InfoNodeData ind in datas)
            {
                InfoNode infoNode = InfoNode.CreateFromData(ind);
                if (infoNode != null)
                    infoNodes.Add(infoNode);
            }
        }
        public InfoNode GetInfoNode(string id) => infoNodes.Find(x => x.ID == id);

        /* -------- Private Methods -------- */
        private void SetLine(GameManagerContainer gmc, GraphicsComponent g)
        {
            activeInfoNodeString = StringUtil.InsertLineBreaksForWidth(activeInfoNode.Body, gmc.UIContentManager.InfoNodeSpriteFont, gmc.UIContentManager.InfoNodeBoxWidth);
            lineSet = true;
        }

        private string GetContinueMessage(SettingsComponent s) => s.UseTouchScreen ? TOUCH_SCREEN_CONTINUE_MESSAGE : CONTINUE_MESSAGE;
    }
}
