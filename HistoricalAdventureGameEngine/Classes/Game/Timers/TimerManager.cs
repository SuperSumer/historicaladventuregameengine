﻿using System.Collections.Generic;

namespace HistoricalAdventureGameEngine
{
    class TimerManager
    {
        /* -------- Properties -------- */
        public List<Timer> Timers { get; private set; } = new List<Timer>();

        /* -------- Private Fields -------- */
        private List<Timer> removedTimers = new List<Timer>();

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            List<Timer> removalList = new List<Timer>();

            foreach(Timer t in Timers)
            {
                t.Update(gtw, gmc, cc);

                if (t.TimedOut)
                    removalList.Add(t);
            }

            foreach (Timer t in removalList)
                Timers.Remove(t);
        }

        public void AddTimer(Timer timer)
        {
            Timers.Add(timer);
        }
        public void RemoveTimer(string id)
        {
            List<Timer> timers = Timers.FindAll(x => x.ID == id);
            Timers.RemoveAll(x => x.ID == id);
            removedTimers.AddRange(timers);
        }

        public void ReinstateTimer(string id)
        {
            Timer timer = removedTimers.Find(x => x.ID == id);
            if (timer != null)
            {
                timer.Reset();
                AddTimer(timer);
            }
        }
    }
}
