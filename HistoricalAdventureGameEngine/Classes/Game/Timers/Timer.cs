﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class Timer
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public float Duration { get; private set; }
        public float ElapsedTime { get; private set; }
        public float RemainingTime => Duration - ElapsedTime;
        public float CompletionFraction => ElapsedTime / Duration;
        public float DepletionFraction => RemainingTime / Duration;
        public Texture2D Texture { get; private set; }
        public bool TimedOut { get; private set; } = false;

        /* -------- Private Fields -------- */
        private List<StageDirection> timeoutStageDirections = new List<StageDirection>();

        /* -------- Constants -------- */
        private const float DEPLETION_FRACTION_FOR_RED_TEXTURE = 0.2f;
        private readonly Color TEXTURE_RED_TINT = new Color(1f, 0.5f, 0.5f);

        /* -------- Constructors -------- */
        public Timer(string inID, float inDuration, string textureIDOrPath)
        {
            ID = inID;
            Duration = inDuration;
            Texture = ContentLoader.LoadTexture(textureIDOrPath);
        }

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (!gmc.SceneManager.CanInteractWithScene || TimedOut || gmc.InfoMessageManager.InfoMessageIsActive || gmc.InfoNodeManager.InfoNodeIsActive)
                return;

            ElapsedTime += gtw.ElapsedSeconds;
            if (ElapsedTime >= Duration)
            {
                ElapsedTime = Duration;
                TimedOut = true;
                if (timeoutStageDirections.Count > 0)
                    gmc.Player.LoadStageDirections(timeoutStageDirections, null, gtw, gmc, cc);
            }
        }

        public void SBDrawTexture(Rectangle renderRect, float uiAlpha, SpriteBatch sb)
        {
            Color tint = DepletionFraction < DEPLETION_FRACTION_FOR_RED_TEXTURE ? TEXTURE_RED_TINT * uiAlpha : Color.White * uiAlpha;
            sb.Draw(Texture, renderRect, tint);
        }

        public void Reset()
        {
            ElapsedTime = 0f;
            TimedOut = false;
        }

        /* -------- Static Methods -------- */
        public static Timer CreateFromData(TimerData data, GameManagerContainer gmc)
        {
            if (data == null)
                return null;

            Timer timer = new Timer(data.ID, data.Duration, data.TextureIDOrPath);

            timer.timeoutStageDirections.AddRange(StageDirection.CreateFromData(data.StageDirections, gmc));

            return timer;
        }
    }
}
