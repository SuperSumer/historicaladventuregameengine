﻿using System;
using System.Collections.Generic;

namespace HistoricalAdventureGameEngine
{
    class StageDirectionManager
    {
        /* -------- Private Fields -------- */
        private List<StageDirectionActivation> stageDirectionActivations = new List<StageDirectionActivation>();

        /* -------- Public Methods -------- */
        public void OnStageDirectionActivated(StageDirection sd, SceneObject so)
        {
            StageDirectionActivation sda = new StageDirectionActivation(sd, so);
            stageDirectionActivations.Add(sda);
        }
        public void RevertAndClearStageDirections(ComponentContainer cc, GameManagerContainer gmc)
        {
            for (int i = stageDirectionActivations.Count - 1; i >= 0; i--)
            {
                StageDirectionActivation sda = stageDirectionActivations[i];
                sda.StageDirection.Revert(sda.SceneObject, cc, gmc);
            }

            stageDirectionActivations.Clear();
        }
    }
}
