﻿using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eStageDirectionType
    {
        DUMMY_STAGE_DIRECTION,
        SHOW_INFO_NODE,
        SHOW_INFO_NODE_AND_WAIT,
        SHOW_INFO_MESSAGE,
        SHOW_INFO_MESSAGE_AND_WAIT,
        SET_GAME_FLAG,
        REQUEST_SCENE_CHANGE,
        INCREMENT_NUMERICAL_ITEM,
        DECREMENT_NUMERICAL_ITEM,
        LOG_PLAY_START_TIME,
        LOG_PLAY_END_TIME,
        ADD_TIMER,
        REMOVE_TIMER,
        WAIT_FOR_SCENE_INTERACTIVITY,
        WAIT_FOR_DURATION,
        WAIT_FOR_NEW_SCENE_ACTIVATED,
        PLAY_SOUND,
        PLAY_SOUND_AND_WAIT,
        SET_VISIBILITY,
        SET_ACTIVITY,
        ADD_ITEM,
        REMOVE_ITEM,
        SET_ITEM_STATE,
        SET_ITEM_ACTIVATABILITY,
        INCREASE_SCORE,
        REVERT_ALL_STAGE_DIRECTIONS,
        SET_PLAYER_CHARACTER_SHOWING
    }

    abstract class StageDirection
    {
        /* -------- Properties -------- */
        public int NumTimesActivated { get; private set; }
        public bool IsFinished { get; protected set; }

        /* -------- Private Fields -------- */
        private List<StageDirectionInhibitor> inhibitors = new List<StageDirectionInhibitor>();

        /* -------- Public Methods -------- */
        public void Activate(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            IsFinished = false;
            if (IsInhibited(gmc))
            {
                IsFinished = true;
                return;
            }

            NumTimesActivated++;
            OnActivation(so, cc, gmc);
        }
        public void Revert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            NumTimesActivated = 0;
            OnRevert(so, cc, gmc);
        }

        public virtual void Update(SceneObject so, GameTimeWrapper gtw, GameManagerContainer gmc)
        {
        }

        /* -------- Private Methods -------- */
        private bool IsInhibited(GameManagerContainer gmc) => inhibitors.Find(x => x.InhibitsStageDirection(this, gmc)) != null;

        protected abstract void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc);
        protected abstract void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc);

        /* -------- Static Methods -------- */
        public static StageDirection CreateFromData(StageDirectionData data, GameManagerContainer gmc)
        {
            if (data == null)
                return null;

            eStageDirectionType type = EnumUtil.Parse<eStageDirectionType>(data.Type);

            StageDirection sd;

            switch (type)
            {
                case eStageDirectionType.DUMMY_STAGE_DIRECTION:
                    sd = new DummyStageDirection();
                    break;
                case eStageDirectionType.SHOW_INFO_NODE:
                    sd = ShowInfoNode.CreateFromData(data, gmc.InfoNodeManager);
                    break;
                case eStageDirectionType.SHOW_INFO_NODE_AND_WAIT:
                    sd = ShowInfoNodeAndWait.CreateFromData(data, gmc.InfoNodeManager);
                    break;
                case eStageDirectionType.SHOW_INFO_MESSAGE:
                    sd = ShowInfoMessage.CreateFromData(data);
                    break;
                case eStageDirectionType.SHOW_INFO_MESSAGE_AND_WAIT:
                    sd = ShowInfoMessageAndWait.CreateFromData(data);
                    break;
                case eStageDirectionType.SET_GAME_FLAG:
                    sd = SetGameFlag.CreateFromData(data);
                    break;
                case eStageDirectionType.REQUEST_SCENE_CHANGE:
                    sd = RequestSceneChange.CreateFromData(data);
                    break;
                case eStageDirectionType.INCREMENT_NUMERICAL_ITEM:
                    sd = IncrementNumericalItem.CreateFromData(data);
                    break;
                case eStageDirectionType.DECREMENT_NUMERICAL_ITEM:
                    sd = DecrementNumericalItem.CreateFromData(data);
                    break;
                case eStageDirectionType.LOG_PLAY_START_TIME:
                    sd = new LogPlayStartTime();
                    break;
                case eStageDirectionType.LOG_PLAY_END_TIME:
                    sd = new LogPlayEndTime();
                    break;
                case eStageDirectionType.ADD_TIMER:
                    sd = AddTimer.CreateFromData(data, gmc);
                    break;
                case eStageDirectionType.REMOVE_TIMER:
                    sd = RemoveTimer.CreateFromData(data);
                    break;
                case eStageDirectionType.WAIT_FOR_SCENE_INTERACTIVITY:
                    sd = new WaitForSceneInteractivity();
                    break;
                case eStageDirectionType.WAIT_FOR_DURATION:
                    sd = WaitForDuration.CreateFromData(data);
                    break;
                case eStageDirectionType.WAIT_FOR_NEW_SCENE_ACTIVATED:
                    sd = new WaitForNewSceneActivated();
                    break;
                case eStageDirectionType.PLAY_SOUND:
                    sd = PlaySound.CreateFromData(data);
                    break;
                case eStageDirectionType.PLAY_SOUND_AND_WAIT:
                    sd = PlaySoundAndWait.CreateFromData(data);
                    break;
                case eStageDirectionType.SET_VISIBILITY:
                    sd = SetVisibility.CreateFromData(data);
                    break;
                case eStageDirectionType.SET_ACTIVITY:
                    sd = SetActivity.CreateFromData(data);
                    break;
                case eStageDirectionType.ADD_ITEM:
                    sd = AddItem.CreateFromData(data);
                    break;
                case eStageDirectionType.REMOVE_ITEM:
                    sd = RemoveItem.CreateFromData(data);
                    break;
                case eStageDirectionType.SET_ITEM_STATE:
                    sd = SetItemState.CreateFromData(data);
                    break;
                case eStageDirectionType.SET_ITEM_ACTIVATABILITY:
                    sd = SetItemActivatability.CreateFromData(data);
                    break;
                case eStageDirectionType.INCREASE_SCORE:
                    sd = IncreaseScore.CreateFromData(data);
                    break;
                case eStageDirectionType.REVERT_ALL_STAGE_DIRECTIONS:
                    sd = new RevertAllStageDirections();
                    break;
                case eStageDirectionType.SET_PLAYER_CHARACTER_SHOWING:
                    sd = SetPlayerCharacterShowing.CreateFromData(data);
                    break;
                default:
                    return null;
            }

            if (sd == null)
                return null;

            sd.inhibitors.AddRange(StageDirectionInhibitor.CreateFromData(data.Inhibitors));

            return sd;
        }
        public static List<StageDirection> CreateFromData(StageDirectionData[] datas, GameManagerContainer gmc)
        {
            List<StageDirection> stageDirections = new List<StageDirection>();

            if (datas == null)
                return stageDirections;
            
            foreach (StageDirectionData data in datas)
            {
                StageDirection sd = CreateFromData(data, gmc);
                if (sd != null)
                    stageDirections.Add(sd);
            }
            return stageDirections;
        }
    }
}
