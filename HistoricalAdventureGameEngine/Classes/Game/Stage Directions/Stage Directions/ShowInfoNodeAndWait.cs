﻿using System;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class ShowInfoNodeAndWait : StageDirection
    {
        /* -------- Private Fields -------- */
        private InfoNode infoNode;

        /* -------- Constructors -------- */
        public ShowInfoNodeAndWait(InfoNode inInfoNode)
        {
            infoNode = inInfoNode;
        }

        /* -------- Public Methods -------- */
        public override void Update(SceneObject so, GameTimeWrapper gtw, GameManagerContainer gmc)
        {
            if (!gmc.InfoNodeManager.InfoNodeIsActive)
                IsFinished = true;

            base.Update(so, gtw, gmc);
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.InfoNodeManager.ActivateInfoNode(infoNode, gmc);
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.InfoNodeManager.DeactivateInfoNode(infoNode, gmc);
        }

        /* -------- Static Methods -------- */
        public static ShowInfoNodeAndWait CreateFromData(StageDirectionData data, InfoNodeManager inm)
        {
            if (data.ShowInfoNodeData == null)
                return null;

            InfoNode infoNode = inm.GetInfoNode(data.ShowInfoNodeData.InfoNodeID);
            if (infoNode == null)
                return null;

            return new ShowInfoNodeAndWait(infoNode);
        }
    }
}
