﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class AddItem : StageDirection
    {
        /* -------- Private Fields -------- */
        private string itemID;

        /* -------- Constructors -------- */
        public AddItem(string inItemID)
        {
            itemID = inItemID;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            Item item = gmc.ItemManager.GetItem(itemID);
            if (item == null)
                return;

            gmc.Player.Inventory.Items.Add(item);

            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.Player.Inventory.Items.RemoveAll(x => x.ID == itemID);
        }

        /* -------- Static Methods -------- */
        public static AddItem CreateFromData(StageDirectionData data)
        {
            AlterItemData aid = data.AlterItemData;
            if (aid == null)
                return null;

            AddItem addItem = new AddItem(aid.ItemID);
            return addItem;
        }
    }
}
