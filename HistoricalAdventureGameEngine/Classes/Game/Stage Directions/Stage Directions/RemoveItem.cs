﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class RemoveItem : StageDirection
    {
        /* -------- Private Fields -------- */
        private string itemID;

        /* -------- Constructors -------- */
        public RemoveItem(string inItemID)
        {
            itemID = inItemID;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.Player.Inventory.Items.RemoveAll(x => x.ID == itemID);

            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            Item item = gmc.ItemManager.GetItem(itemID);
            if (item != null)
                gmc.Player.Inventory.Items.Add(item);
        }

        /* -------- Static Methods -------- */
        public static RemoveItem CreateFromData(StageDirectionData data)
        {
            AlterItemData aid = data.AlterItemData;
            if (aid == null)
                return null;

            RemoveItem removeItem = new RemoveItem(aid.ItemID);
            return removeItem;
        }
    }
}
