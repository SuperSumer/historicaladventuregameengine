﻿using System;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class SetGameFlag : StageDirection
    {
        /* -------- Private Fields -------- */
        private string flagID;
        private bool flagValue;

        /* -------- Constructors -------- */
        public SetGameFlag(string inFlagID, bool inFlagValue)
        {
            flagID = inFlagID;
            flagValue = inFlagValue;
        }
        
        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.GameVariableManager.SetFlag(flagID, flagValue);

            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.GameVariableManager.SetFlag(flagID, !flagValue);
        }

        /* -------- Static Methods -------- */
        public static SetGameFlag CreateFromData(StageDirectionData data)
        {
            if (data.SetGameFlagData == null)
                return null;

            SetGameFlag setGameFlag = new SetGameFlag(data.SetGameFlagData.FlagID, data.SetGameFlagData.FlagValue);
            return setGameFlag;
        }
    }
}
