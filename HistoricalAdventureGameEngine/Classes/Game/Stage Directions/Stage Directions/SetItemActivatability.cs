﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class SetItemActivatability : StageDirection
    {
        /* -------- Private Fields -------- */
        private string itemID;
        private bool activatable;

        /* -------- Constructors -------- */
        public SetItemActivatability(string inItemID, bool inActivatable)
        {
            itemID = inItemID;
            activatable = inActivatable;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            ActivatableItem item = gmc.Player.Inventory.Items.Find(x => x.ID == itemID) as ActivatableItem;
            if (item == null)
                return;

            item.SetActivatable(activatable);

            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            ActivatableItem item = gmc.Player.Inventory.Items.Find(x => x.ID == itemID) as ActivatableItem;
            if (item == null)
                return;

            item.SetActivatable(!activatable);
        }

        /* -------- Static Methods -------- */
        public static SetItemActivatability CreateFromData(StageDirectionData data)
        {
            SetItemFlagData sifd = data.SetItemFlagData;
            if (sifd == null)
                return null;

            SetItemActivatability sia = new SetItemActivatability(sifd.ItemID, sifd.Flag);
            return sia;
        }
    }
}
