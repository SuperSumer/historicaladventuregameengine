﻿using System;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class ShowInfoMessage : StageDirection
    {
        /* -------- Private Fields -------- */
        private string infoMessage;
        private string textureIDOrPath;

        /* -------- Constructors -------- */
        public ShowInfoMessage(string inInfoMessage, string inTextureIDOrPath)
        {
            infoMessage = inInfoMessage;
            textureIDOrPath = inTextureIDOrPath;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            if (!string.IsNullOrEmpty(textureIDOrPath))
                gmc.InfoMessageManager.ActivateTextureInfoMessage(textureIDOrPath);
            else
                gmc.InfoMessageManager.ActivateInfoMessage(infoMessage);
            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }

        /* -------- Static Methods -------- */
        public static ShowInfoMessage CreateFromData(StageDirectionData data)
        {
            if (data.ShowInfoMessageData == null)
                return null;

            return new ShowInfoMessage(data.ShowInfoMessageData.InfoMessage, data.ShowInfoMessageData.TextureIDOrPath);
        }
    }
}
