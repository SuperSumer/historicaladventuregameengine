﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class IncreaseScore : StageDirection
    {
        /* -------- Private Fields -------- */
        private int increase;

        /* -------- Constructors -------- */
        public IncreaseScore(int inIncrease)
        {
            increase = inIncrease;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.ScoreManager.IncreaseScore(increase);
            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.ScoreManager.IncreaseScore(-increase);
        }

        /* -------- Static Methods -------- */
        public static IncreaseScore CreateFromData(StageDirectionData data)
        {
            if (data.IncreaseScoreData == null)
                return null;

            IncreaseScore increaseScore = new IncreaseScore(data.IncreaseScoreData.ScoreIncrease);
            return increaseScore;
        }
    }
}
