﻿using System;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class ShowInfoMessageAndWait : StageDirection
    {
        /* -------- Private Fields -------- */
        private string infoMessage;
        private string textureIDOrPath;

        /* -------- Constructors -------- */
        public ShowInfoMessageAndWait(string inInfoMessage, string inTextureIDOrPath)
        {
            infoMessage = inInfoMessage;
            textureIDOrPath = inTextureIDOrPath;
        }

        /* -------- Public Methods -------- */
        public override void Update(SceneObject so, GameTimeWrapper gtw, GameManagerContainer gmc)
        {
            if (!gmc.InfoMessageManager.InfoMessageIsActive)
                IsFinished = true;

            base.Update(so, gtw, gmc);
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            if (!string.IsNullOrEmpty(textureIDOrPath))
                gmc.InfoMessageManager.ActivateTextureInfoMessage(textureIDOrPath);
            else
                gmc.InfoMessageManager.ActivateInfoMessage(infoMessage);
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }

        /* -------- Static Methods -------- */
        public static ShowInfoMessageAndWait CreateFromData(StageDirectionData data)
        {
            if (data.ShowInfoMessageData == null)
                return null;

            return new ShowInfoMessageAndWait(data.ShowInfoMessageData.InfoMessage, data.ShowInfoMessageData.TextureIDOrPath);
        }
    }
}
