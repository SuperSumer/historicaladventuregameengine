﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HistoricalAdventureGameEngine
{
    class RevertAllStageDirections : StageDirection
    {
        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.StageDirectionManager.RevertAndClearStageDirections(cc, gmc);
            foreach (Scene scene in gmc.SceneManager.Scenes)
            {
                ObjectPopulatedScene ops = scene as ObjectPopulatedScene;
                if (ops == null)
                    continue;

                ops.SetAllSceneObjectsToInitialVisibilityAndActivity();
            }

            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }
    }
}
