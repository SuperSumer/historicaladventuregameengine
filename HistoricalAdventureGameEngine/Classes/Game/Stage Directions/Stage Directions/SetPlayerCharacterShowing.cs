﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class SetPlayerCharacterShowing : StageDirection
    {
        /* -------- Private Fields -------- */
        private string forcedLastSceneID;

        /* -------- Constructors -------- */
        public SetPlayerCharacterShowing(string inForcedLastSceneID)
        {
            forcedLastSceneID = inForcedLastSceneID;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            IsFinished = true;

            ObjectPopulatedScene ops = gmc.SceneManager.ActiveScene as ObjectPopulatedScene;
            if (ops == null)
                return;

            string lastSceneID = string.IsNullOrEmpty(forcedLastSceneID) ? gmc.Player.LastSceneID : forcedLastSceneID;
            ops.SetPlayerCharacterShowing(lastSceneID);
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }

        /* -------- Static Methods -------- */
        public static SetPlayerCharacterShowing CreateFromData(StageDirectionData data)
        {
            if (data.SetPlayerCharacterShowingData == null)
                return null;

            SetPlayerCharacterShowing spcs = new SetPlayerCharacterShowing(data.SetPlayerCharacterShowingData.ForcedLastSceneID);
            return spcs;
        }
    }
}
