﻿using System;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class DecrementNumericalItem : StageDirection
    {
        /* -------- Private Fields -------- */
        private string itemID;

        /* -------- Constructors -------- */
        public DecrementNumericalItem(string inItemID)
        {
            itemID = inItemID;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            NumericalItem ni = gmc.Player.Inventory.Items.Find(x => x is NumericalItem && x.ID == itemID) as NumericalItem;
            if (ni == null)
            {
                IsFinished = true;
                return;
            }

            ni.Number--;

            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            NumericalItem ni = gmc.Player.Inventory.Items.Find(x => x is NumericalItem && x.ID == itemID) as NumericalItem;
            if (ni != null)
                ni.Number++;
        }

        /* -------- Static Methods -------- */
        public static DecrementNumericalItem CreateFromData(StageDirectionData data)
        {
            AlterItemData aid = data.AlterItemData;
            if (aid == null)
                return null;

            DecrementNumericalItem dni = new DecrementNumericalItem(aid.ItemID);
            return dni;
        }
    }
}
