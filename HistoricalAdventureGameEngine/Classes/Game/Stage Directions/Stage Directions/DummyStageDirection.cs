﻿using System;

namespace HistoricalAdventureGameEngine
{
    class DummyStageDirection : StageDirection
    {
        /* -------- Public Methods -------- */
        public override void Update(SceneObject so, GameTimeWrapper gtw, GameManagerContainer gmc)
        {
            IsFinished = true;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
        }
    }
}
