﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class SetItemState : StageDirection
    {
        /* -------- Private Fields -------- */
        private string itemID;
        private string stateID;
        private string stateIDOnChange;

        /* -------- Constructors -------- */
        public SetItemState(string inItemID, string inStateID)
        {
            itemID = inItemID;
            stateID = inStateID;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            IsFinished = true;

            MultiStateItem item = gmc.Player.Inventory.Items.Find(x => x.ID == itemID) as MultiStateItem;
            if (item == null)
                return;

            stateIDOnChange = item.GetCurrentStateID();
            item.SetState(stateID);
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            MultiStateItem item = gmc.Player.Inventory.Items.Find(x => x.ID == itemID) as MultiStateItem;
            if (item == null)
                return;
            
            item.SetState(stateIDOnChange);
        }

        /* -------- Static Methods -------- */
        public static SetItemState CreateFromData(StageDirectionData data)
        {
            SetItemStateData sisd = data.SetItemStateData;
            if (sisd == null)
                return null;

            SetItemState sis = new SetItemState(sisd.ItemID, sisd.StateID);
            return sis;
        }
    }
}
