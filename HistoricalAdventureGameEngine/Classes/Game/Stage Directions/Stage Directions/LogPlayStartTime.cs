﻿using System;

namespace HistoricalAdventureGameEngine
{
    class LogPlayStartTime : StageDirection
    {
        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            Logging.LogPlayStartTime();
            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }
    }
}
