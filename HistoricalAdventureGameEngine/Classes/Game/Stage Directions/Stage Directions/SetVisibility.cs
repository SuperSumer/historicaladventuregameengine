﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class SetVisibility : StageDirection
    {
        /* -------- Private Fields -------- */
        private string sceneID;
        private string sceneObjectID;
        private bool visible;

        /* -------- Constructors -------- */
        public SetVisibility(string inSceneID, string inSceneObjectID, bool inVisible)
        {
            sceneID = inSceneID;
            sceneObjectID = inSceneObjectID;
            visible = inVisible;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            IsFinished = true;

            if (string.IsNullOrEmpty(sceneObjectID))
            {
                so?.SetVisibility(visible);
                return;
            }

            Scene scene;
            if (string.IsNullOrEmpty(sceneID))
                scene = gmc.SceneManager.ActiveScene;
            else
                scene = gmc.SceneManager.Scenes.Find(x => x.ID == sceneID);

            if (scene == null)
                return;

            ObjectPopulatedScene ops = scene as ObjectPopulatedScene;
            if (ops == null)
                return;

            SceneObject sceneObject = ops.SceneObjects.Find(x => x.ID == sceneObjectID);
            if (sceneObject == null)
                return;

            sceneObject.SetVisibility(visible);
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            if (string.IsNullOrEmpty(sceneObjectID))
            {
                so?.SetVisibility(!visible);
                return;
            }

            Scene scene;
            if (string.IsNullOrEmpty(sceneID))
                scene = gmc.SceneManager.ActiveScene;
            else
                scene = gmc.SceneManager.Scenes.Find(x => x.ID == sceneID);

            if (scene == null)
                return;

            ObjectPopulatedScene ops = scene as ObjectPopulatedScene;
            if (ops == null)
                return;

            SceneObject sceneObject = ops.SceneObjects.Find(x => x.ID == sceneObjectID);
            if (sceneObject == null)
                return;

            sceneObject.SetVisibility(!visible);
        }

        /* -------- Static Methods -------- */
        public static SetVisibility CreateFromData(StageDirectionData data)
        {
            SetSceneObjectFlagData ssofd = data.SetSceneObjectFlagData;
            if (ssofd == null)
                return null;

            SetVisibility sv = new SetVisibility(ssofd.SceneID, ssofd.SceneObjectID, ssofd.Flag);
            return sv;
        }
    }
}
