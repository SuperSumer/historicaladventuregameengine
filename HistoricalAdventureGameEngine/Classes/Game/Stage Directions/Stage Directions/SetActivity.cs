﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class SetActivity : StageDirection
    {
        /* -------- Private Fields -------- */
        private string sceneID;
        private string sceneObjectID;
        private bool active;

        /* -------- Constructors -------- */
        public SetActivity(string inSceneID, string inSceneObjectID, bool inActive)
        {
            sceneID = inSceneID;
            sceneObjectID = inSceneObjectID;
            active = inActive;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            IsFinished = true;

            if (string.IsNullOrEmpty(sceneObjectID))
            {
                so?.SetActive(active);
                return;
            }

            Scene scene;
            if (string.IsNullOrEmpty(sceneID))
                scene = gmc.SceneManager.ActiveScene;
            else
                scene = gmc.SceneManager.Scenes.Find(x => x.ID == sceneID);

            if (scene == null)
                return;

            ObjectPopulatedScene ops = scene as ObjectPopulatedScene;
            if (ops == null)
                return;

            SceneObject sceneObject = ops.SceneObjects.Find(x => x.ID == sceneObjectID);
            if (sceneObject == null)
                return;

            sceneObject.SetActive(active);
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            if (string.IsNullOrEmpty(sceneObjectID))
            {
                so?.SetActive(!active);
                return;
            }

            Scene scene;
            if (string.IsNullOrEmpty(sceneID))
                scene = gmc.SceneManager.ActiveScene;
            else
                scene = gmc.SceneManager.Scenes.Find(x => x.ID == sceneID);

            if (scene == null)
                return;

            ObjectPopulatedScene ops = scene as ObjectPopulatedScene;
            if (ops == null)
                return;

            SceneObject sceneObject = ops.SceneObjects.Find(x => x.ID == sceneObjectID);
            if (sceneObject == null)
                return;

            sceneObject.SetActive(!active);
        }

        /* -------- Static Methods -------- */
        public static SetActivity CreateFromData(StageDirectionData data)
        {
            SetSceneObjectFlagData ssofd = data.SetSceneObjectFlagData;
            if (ssofd == null)
                return null;

            SetActivity sa = new SetActivity(ssofd.SceneID, ssofd.SceneObjectID, ssofd.Flag);
            return sa;
        }
    }
}
