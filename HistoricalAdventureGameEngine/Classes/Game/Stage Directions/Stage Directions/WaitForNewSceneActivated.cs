﻿using System;
using System.Collections.Generic;

namespace HistoricalAdventureGameEngine
{
    class WaitForNewSceneActivated : StageDirection
    {
        /* -------- Public Methods -------- */
        public override void Update(SceneObject so, GameTimeWrapper gtw, GameManagerContainer gmc)
        {
            if (gmc.SceneManager.Mode != eSceneMode.FADING_OUT)
                IsFinished = true;
            
            base.Update(so, gtw, gmc);
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }
    }
}
