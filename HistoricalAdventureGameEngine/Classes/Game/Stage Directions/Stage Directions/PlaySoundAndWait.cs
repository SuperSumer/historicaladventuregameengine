﻿using Microsoft.Xna.Framework.Audio;
using DataTypes;
using System;

namespace HistoricalAdventureGameEngine
{
    class PlaySoundAndWait : StageDirection
    {
        /* -------- Private Fields -------- */
        private SoundEffect sound;
        private float timer = 0f;
        private string sceneID;

        /* -------- Constructors -------- */
        public PlaySoundAndWait(SoundEffect inSound, string inSceneID)
        {
            sound = inSound;
            sceneID = inSceneID;
        }

        /* -------- Public Methods -------- */
        public override void Update(SceneObject so, GameTimeWrapper gtw, GameManagerContainer gmc)
        {
            base.Update(so, gtw, gmc);

            timer += gtw.ElapsedSeconds;
            float duration = (float)sound.Duration.TotalSeconds;
            if (timer >= duration)
                IsFinished = true;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            if (string.IsNullOrEmpty(sceneID) || gmc.SceneManager.ActiveScene.ID == sceneID)
                cc.AudioComponent.PlaySoundEffect_FireAndForget(sound);
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }

        /* -------- Static Methods -------- */
        public static PlaySoundAndWait CreateFromData(StageDirectionData data)
        {
            if (data.PlaySoundData == null)
                return null;

            SoundEffect se = ContentLoader.LoadSoundEffect(data.PlaySoundData.SoundEffectIDOrPath);
            PlaySoundAndWait playSound = new PlaySoundAndWait(se, data.PlaySoundData.SceneID);

            return playSound;
        }
    }
}
