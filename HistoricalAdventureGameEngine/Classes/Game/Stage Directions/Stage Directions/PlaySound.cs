﻿using Microsoft.Xna.Framework.Audio;
using DataTypes;
using System;

namespace HistoricalAdventureGameEngine
{
    class PlaySound : StageDirection
    {
        /* -------- Private Fields -------- */
        private SoundEffect sound;
        private string sceneID;

        /* -------- Constructors -------- */
        public PlaySound(SoundEffect inSound, string inSceneID)
        {
            sound = inSound;
            sceneID = inSceneID;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            if (string.IsNullOrEmpty(sceneID) || gmc.SceneManager.ActiveScene.ID == sceneID)
                cc.AudioComponent.PlaySoundEffect_FireAndForget(sound);

            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }

        /* -------- Static Methods -------- */
        public static PlaySound CreateFromData(StageDirectionData data)
        {
            if (data.PlaySoundData == null)
                return null;

            SoundEffect se = ContentLoader.LoadSoundEffect(data.PlaySoundData.SoundEffectIDOrPath);
            PlaySound playSound = new PlaySound(se, data.PlaySoundData.SceneID);

            return playSound;
        }
    }
}
