﻿using System;

namespace HistoricalAdventureGameEngine
{
    class WaitForSceneInteractivity : StageDirection
    {
        /* -------- Public Methods -------- */
        public override void Update(SceneObject so, GameTimeWrapper gtw, GameManagerContainer gmc)
        {
            if (gmc.SceneManager.CanInteractWithScene)
                IsFinished = true;

            base.Update(so, gtw, gmc);
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }
    }
}
