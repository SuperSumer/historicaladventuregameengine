﻿using System;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class AddTimer : StageDirection
    {
        /* -------- Private Fields -------- */
        private Timer timer;

        /* -------- Constructors -------- */
        public AddTimer(Timer inTimer)
        {
            timer = inTimer;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            timer.Reset();
            gmc.TimerManager.AddTimer(timer);
            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.TimerManager.RemoveTimer(timer.ID);
        }

        /* -------- Static Methods -------- */
        public new static AddTimer CreateFromData(StageDirectionData data, GameManagerContainer gmc)
        {
            AddTimerData atd = data.AddTimerData;
            if (atd == null)
                return null;

            Timer timer = Timer.CreateFromData(atd.Timer, gmc);
            if (timer == null)
                return null;

            return new AddTimer(timer);
        }
    }
}
