﻿using System;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class ShowInfoNode : StageDirection
    {
        /* -------- Private Fields -------- */
        private InfoNode infoNode;

        /* -------- Constructors -------- */
        public ShowInfoNode(InfoNode inInfoNode)
        {
            infoNode = inInfoNode;
        }
        
        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.InfoNodeManager.ActivateInfoNode(infoNode, gmc);
            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.InfoNodeManager.DeactivateInfoNode(infoNode, gmc);
        }

        /* -------- Static Methods -------- */
        public static ShowInfoNode CreateFromData(StageDirectionData data, InfoNodeManager inm)
        {
            if (data.ShowInfoNodeData == null)
                return null;

            InfoNode infoNode = inm.GetInfoNode(data.ShowInfoNodeData.InfoNodeID);
            if (infoNode == null)
                return null;

            return new ShowInfoNode(infoNode);
        }
    }
}
