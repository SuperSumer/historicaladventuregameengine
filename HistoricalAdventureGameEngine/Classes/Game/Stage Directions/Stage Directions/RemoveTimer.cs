﻿using System;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class RemoveTimer : StageDirection
    {
        /* -------- Private Fields -------- */
        private string id;

        /* -------- Constructors -------- */
        public RemoveTimer(string inID)
        {
            id = inID;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.TimerManager.RemoveTimer(id);
            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            gmc.TimerManager.ReinstateTimer(id);
        }

        /* -------- Static Methods -------- */
        public static RemoveTimer CreateFromData(StageDirectionData data)
        {
            RemoveTimerData rtd = data.RemoveTimerData;
            if (rtd == null)
                return null;

            return new RemoveTimer(rtd.ID);
        }
    }
}
