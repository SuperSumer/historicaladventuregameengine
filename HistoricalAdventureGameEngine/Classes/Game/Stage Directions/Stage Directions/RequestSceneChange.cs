﻿using System;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class RequestSceneChange : StageDirection
    {
        /* -------- Private Fields -------- */
        private string sceneID;
        private string sceneIDOnChange;

        /* -------- Constructors -------- */
        public RequestSceneChange(string inSceneID)
        {
            sceneID = inSceneID;
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            sceneIDOnChange = gmc.SceneManager.ActiveScene.ID;
            gmc.SceneManager.RequestSceneChange(sceneID);
            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            if (string.IsNullOrEmpty(sceneIDOnChange))
                return;
            gmc.SceneManager.RequestSceneChange(sceneIDOnChange);
        }

        /* -------- Static Methods -------- */
        public static RequestSceneChange CreateFromData(StageDirectionData data)
        {
            if (data.RequestSceneChangeData == null)
                return null;

            RequestSceneChange rsc = new RequestSceneChange(data.RequestSceneChangeData.SceneID);
            return rsc;
        }
    }
}
