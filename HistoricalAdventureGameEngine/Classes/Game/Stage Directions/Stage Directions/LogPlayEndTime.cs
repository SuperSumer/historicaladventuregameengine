﻿using System;

namespace HistoricalAdventureGameEngine
{
    class LogPlayEndTime : StageDirection
    {
        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc)
        {
            Logging.LogPlayEndTime();
            IsFinished = true;
        }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }
    }
}
