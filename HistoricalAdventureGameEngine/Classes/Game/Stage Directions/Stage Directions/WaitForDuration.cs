﻿using System;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class WaitForDuration : StageDirection
    {
        /* -------- Private Fields -------- */
        private float duration;
        private float timer = 0f;

        /* -------- Constructors -------- */
        public WaitForDuration(float inDuration)
        {
            duration = inDuration;
        }

        /* -------- Public Methods -------- */
        public override void Update(SceneObject so, GameTimeWrapper gtw, GameManagerContainer gmc)
        {
            timer += gtw.ElapsedSeconds;

            if (timer >= duration)
                IsFinished = true;

            base.Update(so, gtw, gmc);
        }

        /* -------- Private Methods -------- */
        protected override void OnActivation(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }
        protected override void OnRevert(SceneObject so, ComponentContainer cc, GameManagerContainer gmc) { }

        /* -------- Static Methods -------- */
        public static WaitForDuration CreateFromData(StageDirectionData data)
        {
            WaitForDurationData wfdd = data.WaitForDurationData;
            if (wfdd == null)
                return null;

            WaitForDuration wfd = new WaitForDuration(wfdd.Duration);

            return wfd;
        }
    }
}
