﻿using System;
using System.Collections.Generic;

namespace HistoricalAdventureGameEngine
{
    class StageDirectionActivation
    {
        /* -------- Properties -------- */
        public StageDirection StageDirection { get; private set; }
        public SceneObject SceneObject { get; private set; }

        /* -------- Constructors -------- */
        public StageDirectionActivation(StageDirection inStageDirection, SceneObject inSceneObject)
        {
            StageDirection = inStageDirection;
            SceneObject = inSceneObject;
        }
    }
}
