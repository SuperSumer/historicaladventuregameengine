﻿using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class LimitPerGameFlag : StageDirectionInhibitor
    {
        /* -------- Private Fields -------- */
        private string flagID;
        private bool requiredFlagValue;

        /* -------- Constructors -------- */
        public LimitPerGameFlag(string inFlagID, bool inRequiredValue)
        {
            flagID = inFlagID;
            requiredFlagValue = inRequiredValue;
        }

        /* -------- Public Methods -------- */
        public override bool InhibitsStageDirection(StageDirection sd, GameManagerContainer gmc) => !gmc.GameVariableManager.FlagValueIsEqual(flagID, requiredFlagValue);

        /* -------- Static Methods -------- */
        public new static LimitPerGameFlag CreateFromData(StageDirectionInhibitorData data)
        {
            if (data.LimitPerGameFlagData == null)
                return null;

            LimitPerGameFlag lpgf = new LimitPerGameFlag(data.LimitPerGameFlagData.FlagID, data.LimitPerGameFlagData.RequiredFlagValue);
            return lpgf;
        }
    }
}
