﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class LimitPerNumericalItem : StageDirectionInhibitor
    {
        /* -------- Private Fields -------- */
        private string itemID;
        private int minNumber;
        private int maxNumber;

        /* -------- Constructors -------- */
        public LimitPerNumericalItem(string inItemID, int inMinNumber, int inMaxNumber)
        {
            itemID = inItemID;
            minNumber = inMinNumber;
            maxNumber = inMaxNumber;
        }

        /* -------- Public Methods -------- */
        public override bool InhibitsStageDirection(StageDirection sd, GameManagerContainer gmc)
        {
            NumericalItem numericalItem = gmc.Player.Inventory.Items.Find(x => x.ID == itemID) as NumericalItem;
            if (numericalItem == null)
                return true;

            if (numericalItem.Number < minNumber || numericalItem.Number > maxNumber)
                return true;

            return false;
        }

        /* -------- Static Methods -------- */
        public new static LimitPerNumericalItem CreateFromData(StageDirectionInhibitorData data)
        {
            LimitPerNumericalItemData lpnid = data.LimitPerNumericalItemData;
            if (lpnid == null)
                return null;

            LimitPerNumericalItem lpni = new LimitPerNumericalItem(lpnid.ItemID, lpnid.MinNumber, lpnid.MaxNumber);
            return lpni;
        }
    }
}
