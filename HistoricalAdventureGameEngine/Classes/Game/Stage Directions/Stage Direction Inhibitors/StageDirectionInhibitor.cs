﻿using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eStageDirectionInhibitorType
    {
        LIMIT_STAGE_DIRECTION_ACTIVATIONS,
        LIMIT_PER_GAME_FLAG,
        LIMIT_PER_NUMERICAL_ITEM
    }

    abstract class StageDirectionInhibitor
    {
        /* -------- Public Methods -------- */
        public abstract bool InhibitsStageDirection(StageDirection sd, GameManagerContainer gmc);

        /* -------- Static Methods -------- */
        public static StageDirectionInhibitor CreateFromData(StageDirectionInhibitorData data)
        {
            if (data == null)
                return null;

            eStageDirectionInhibitorType type = EnumUtil.Parse<eStageDirectionInhibitorType>(data.Type);

            switch (type)
            {
                case eStageDirectionInhibitorType.LIMIT_STAGE_DIRECTION_ACTIVATIONS:
                    return LimitStageDirectionActivations.CreateFromData(data);
                case eStageDirectionInhibitorType.LIMIT_PER_GAME_FLAG:
                    return LimitPerGameFlag.CreateFromData(data);
                case eStageDirectionInhibitorType.LIMIT_PER_NUMERICAL_ITEM:
                    return LimitPerNumericalItem.CreateFromData(data);
                default:
                    return null;
            }
        }
        public static List<StageDirectionInhibitor> CreateFromData(StageDirectionInhibitorData[] datas)
        {
            List<StageDirectionInhibitor> inhibitors = new List<StageDirectionInhibitor>();

            if (datas == null)
                return inhibitors;

            foreach (StageDirectionInhibitorData data in datas)
            {
                StageDirectionInhibitor inhibitor = CreateFromData(data);
                if (inhibitor != null)
                    inhibitors.Add(inhibitor);
            }

            return inhibitors;
        }
    }
}
