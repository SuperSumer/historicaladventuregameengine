﻿using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class LimitStageDirectionActivations : StageDirectionInhibitor
    {
        /* -------- Private Fields -------- */
        private int maxActivations;

        /* -------- Constructors -------- */
        public LimitStageDirectionActivations(int inMaxActivations)
        {
            maxActivations = inMaxActivations;
        }

        /* -------- Public Methods -------- */
        public override bool InhibitsStageDirection(StageDirection sd, GameManagerContainer gmc) => sd.NumTimesActivated >= maxActivations;

        /* -------- Static Methods -------- */
        public new static LimitStageDirectionActivations CreateFromData(StageDirectionInhibitorData data)
        {
            if (data.LimitStageDirectionActivationsData == null)
                return null;

            LimitStageDirectionActivations lsda = new LimitStageDirectionActivations(data.LimitStageDirectionActivationsData.MaxActivations);
            return lsda;
        }
    }
}
