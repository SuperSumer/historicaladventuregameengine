﻿using System.Collections.Generic;

namespace HistoricalAdventureGameEngine
{
    class StageDirectionActivator
    {
        /* -------- Private Fields -------- */
        private StageDirection chamberedDirection;
        private Queue<StageDirection> loadedDirections = new Queue<StageDirection>();

        /* -------- Public Methods -------- */
        public void Update(SceneObject so, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            PerformStageDirectionIteration(so, gtw, cc, gmc);
        }
        
        public void LoadStageDirections(List<StageDirection> sdList, SceneObject so, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            foreach (StageDirection sd in sdList)
                loadedDirections.Enqueue(sd);

            PerformStageDirectionIteration(so, gtw, cc, gmc);
        }
        public bool Empty()
        {
            if (loadedDirections.Count == 0 && chamberedDirection == null)
                return false;

            loadedDirections.Clear();
            chamberedDirection = null;

            return true;
        }
        public bool IsEmpty() => chamberedDirection == null && loadedDirections.Count == 0;

        /* -------- Private Methods -------- */
        private void PerformStageDirectionIteration(SceneObject so, GameTimeWrapper gtw, ComponentContainer cc, GameManagerContainer gmc)
        {
            while (true)
            {
                // Abort if scene object is not active
                if (so != null && !so.Active)
                    return;

                // If there is no stage direction in the chamber
                if (chamberedDirection == null)
                {
                    if (loadedDirections.Count == 0)
                        return;

                    // Chamber the next stage direction and activate
                    chamberedDirection = loadedDirections.Dequeue();
                    gmc.StageDirectionManager.OnStageDirectionActivated(chamberedDirection, so);
                    chamberedDirection.Activate(so, cc, gmc);

                    // If already finished, eject it
                    if (chamberedDirection.IsFinished)
                        chamberedDirection = null;

                    continue;
                }

                // There is one in the chamber, update it
                chamberedDirection.Update(so, gtw, gmc);

                // If it's done, eject it
                if (chamberedDirection.IsFinished)
                    chamberedDirection = null;

                // Now need to wait for the next update loop
                break;
            }
        }
    }
}
