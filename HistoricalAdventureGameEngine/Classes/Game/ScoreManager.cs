﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class ScoreManager
    {
        /* -------- Properties -------- */
        public int Score { get; private set; }
        
        /* -------- Constants -------- */
        private const int SCORE_ICON_GAP = 20;
        private const int SCORE_TEXT_GAP = 10;
        private readonly Color TEXT_COLOR = Color.White;

        /* -------- Public Methods -------- */
        public void SBDrawScore(float uiAlpha, GameManagerContainer gmc, GraphicsComponent g)
        {
            Texture2D tex = gmc.UIContentManager.ScoreIconTexture;
            SpriteFont sf = gmc.UIContentManager.ScoreSpriteFont;

            Rectangle iconRect = new Rectangle(SCORE_ICON_GAP, SCORE_ICON_GAP, tex.Width, tex.Height);
            g.SpriteBatch.Draw(tex, iconRect, Color.White * uiAlpha);

            Vector2 scoreSize = sf.MeasureString("1234567890");
            Vector2 scorePos = new Vector2(SCORE_ICON_GAP + tex.Width + SCORE_TEXT_GAP, SCORE_ICON_GAP + tex.Height / 2 - scoreSize.Y / 2f);
            g.SpriteBatch.DrawString(sf, Score.ToString(), scorePos, TEXT_COLOR * uiAlpha);
        }

        public void IncreaseScore(int increase)
        {
            Score += increase;
        }
    }
}
