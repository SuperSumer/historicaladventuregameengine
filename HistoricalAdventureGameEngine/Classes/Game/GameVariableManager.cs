﻿using System.Collections.Generic;

namespace HistoricalAdventureGameEngine
{
    class GameVariableManager
    {
        /* -------- Private Fields -------- */
        private Dictionary<string, bool> flags = new Dictionary<string, bool>();

        /* -------- Public Methods -------- */
        public void SetFlag(string id, bool value)
        {
            if (flags.ContainsKey(id))
                flags[id] = value;
            else
                flags.Add(id, value);
        }
        public bool FlagValueIsEqual(string id, bool value)
        {
            if (!flags.ContainsKey(id))
                return !value;

            return flags[id] == value;
        }
    }
}
