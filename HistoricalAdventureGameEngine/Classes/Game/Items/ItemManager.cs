﻿using System.Collections.Generic;

namespace HistoricalAdventureGameEngine
{
    class ItemManager
    {
        /* -------- Private Fields -------- */
        private Dictionary<string, Item> itemDict = new Dictionary<string, Item>();

        /* -------- Public Methods -------- */
        public void AddItem(Item item)
        {
            if (item == null)
                return;

            itemDict.Add(item.ID, item);
        }
        public Item GetItem(string id)
        {
            if (!itemDict.ContainsKey(id))
                return null;
            return itemDict[id];
        }
    }
}
