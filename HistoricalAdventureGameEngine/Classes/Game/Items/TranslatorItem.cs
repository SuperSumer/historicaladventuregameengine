﻿using Microsoft.Xna.Framework.Graphics;
using DataTypes;
using Microsoft.Xna.Framework;

namespace HistoricalAdventureGameEngine
{
    class TranslatorItem : Item
    {
        /* -------- Properties -------- */
        public bool Active { get; set; }

        /* -------- Private Fields -------- */
        private Texture2D inactiveTexture;
        private Texture2D activeTexture;

        /* -------- Constructors -------- */
        public TranslatorItem(string inID, string inactiveTextureIDOrPath, string activeTextureIDOrPath)
            : base(inID)
        {
            inactiveTexture = ContentLoader.LoadTexture(inactiveTextureIDOrPath);
            activeTexture = ContentLoader.LoadTexture(activeTextureIDOrPath);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(Rectangle renderRect, float uiAlpha, SpriteBatch sb)
        {
            Texture2D tex = Active ? activeTexture : inactiveTexture;
            Color tint = Color.White * uiAlpha;
            sb.Draw(tex, renderRect, tint);
        }

        /* -------- Static Methods -------- */
        public static TranslatorItem CreateFromData(ItemData data)
        {
            if (data.TranslatorItemData == null)
                return null;

            return new TranslatorItem(data.ID, data.TranslatorItemData.InactiveTextureIDOrPath, data.TranslatorItemData.ActiveTextureIDOrPath);
        }
    }
}
