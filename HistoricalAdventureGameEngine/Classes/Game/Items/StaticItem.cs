﻿using Microsoft.Xna.Framework.Graphics;
using DataTypes;
using Microsoft.Xna.Framework;

namespace HistoricalAdventureGameEngine
{
    class StaticItem : Item
    {
        /* -------- Private Fields -------- */
        private Texture2D texture;

        /* -------- Constructors -------- */
        public StaticItem(string inID, string textureIDOrPath)
            : base(inID)
        {
            texture = ContentLoader.LoadTexture(textureIDOrPath);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(Rectangle renderRect, float uiAlpha, SpriteBatch sb)
        {
            Color tint = Color.White * uiAlpha;
            sb.Draw(texture, renderRect, tint);
        }

        /* -------- Static Methods -------- */
        public static StaticItem CreateFromData(ItemData data)
        {
            if (data.StaticItemData == null)
                return null;

            StaticItem si = new StaticItem(data.ID, data.StaticItemData.TextureIDOrPath);
            return si;
        }
    }
}
