﻿using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class ItemState
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public Texture2D Texture { get; private set; }

        /* -------- Constructors -------- */
        public ItemState(string inID, string textureIDOrPath)
        {
            ID = inID;
            Texture = ContentLoader.LoadTexture(textureIDOrPath);
        }

        /* -------- Static Methods -------- */
        public static ItemState CreateFromData(ItemStateData data)
        {
            if (data == null)
                return null;
            return new ItemState(data.ID, data.TextureIDOrPath);
        }
    }
}
