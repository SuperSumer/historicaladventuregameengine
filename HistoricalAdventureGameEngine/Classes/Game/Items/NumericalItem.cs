﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;
using Microsoft.Xna.Framework;

namespace HistoricalAdventureGameEngine
{
    class NumericalItem : Item
    {
        /* -------- Properties -------- */
        public int Number { get; set; } = 0;

        /* -------- Private Fields -------- */
        private Texture2D defaultTexture;
        private List<NumericalItemState> states = new List<NumericalItemState>();
        private SpriteFont spriteFont;

        /* -------- Constants -------- */
        private const int NUMBER_X_OFFSET = 52;
        private const int NUMBER_Y_OFFSET = 48;

        /* -------- Constructors -------- */
        public NumericalItem(string inID, string defaultTextureIDOrPath, List<NumericalItemState> inStates)
            : base(inID)
        {
            defaultTexture = ContentLoader.LoadTexture(defaultTextureIDOrPath);
            states = inStates;
            spriteFont = ContentLoader.LoadSpriteFont("Sprite Fonts/Arial24B");
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(Rectangle renderRect, float uiAlpha, SpriteBatch sb)
        {
            NumericalItemState state = states.Find(x => x.Number == Number);
            Texture2D tex = state == null ? defaultTexture : state.Texture;
            Color tint = Color.White * uiAlpha;
            sb.Draw(tex, renderRect, tint);
            Vector2 textPos = new Vector2(renderRect.Left + NUMBER_X_OFFSET, renderRect.Top + NUMBER_Y_OFFSET);
            sb.DrawString(spriteFont, Number.ToString(), textPos, tint);
        }

        /* -------- Static Methods -------- */
        public static NumericalItem CreateFromData(ItemData data)
        {
            if (data.NumericalItemData == null)
                return null;

            List<NumericalItemState> states = NumericalItemState.CreateFromData(data.NumericalItemData.States);
            if (states == null)
                return null;

            NumericalItem ni = new NumericalItem(data.ID, data.NumericalItemData.DefaultTextureIDOrPath, states);
            return ni;
        }
    }
}
