﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class NumericalItemState
    {
        /* -------- Properties -------- */
        public int Number { get; private set; }
        public Texture2D Texture { get; private set; }

        /* -------- Constructors -------- */
        public NumericalItemState(int inNumber, string textureIDOrPath)
        {
            Number = inNumber;
            Texture = ContentLoader.LoadTexture(textureIDOrPath);
        }

        /* -------- Static Methods -------- */
        public static NumericalItemState CreateFromData(NumericalItemStateData data)
        {
            if (data == null)
                return null;

            return new NumericalItemState(data.Number, data.TextureIDOrPath);
        }
        public static List<NumericalItemState> CreateFromData(NumericalItemStateData[] datas)
        {
            List<NumericalItemState> states = new List<NumericalItemState>();
            foreach (NumericalItemStateData nisd in datas)
            {
                NumericalItemState nis = CreateFromData(nisd);
                if (nis != null)
                    states.Add(nis);
            }

            return states;
        }
    }
}
