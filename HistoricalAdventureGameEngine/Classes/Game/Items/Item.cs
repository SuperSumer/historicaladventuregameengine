﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eItemType
    {
        STATIC_ITEM,
        MULTI_STATE_ITEM,
        TRANSLATOR_ITEM,
        NUMERICAL_ITEM,
        ACTIVATABLE_ITEM
    }

    abstract class Item
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }

        /* -------- Constructors -------- */
        public Item(string inID)
        {
            ID = inID;
        }

        /* -------- Public Methods -------- */
        public abstract void SBDraw(Rectangle renderRect, float uiAlpha, SpriteBatch sb);

        /* -------- Static Methods -------- */
        public static Item CreateFromData(ItemData data, GameManagerContainer gmc)
        {
            if (data == null)
                return null;

            eItemType type = EnumUtil.Parse<eItemType>(data.Type);

            switch (type)
            {
                case eItemType.STATIC_ITEM:
                    return StaticItem.CreateFromData(data);
                case eItemType.MULTI_STATE_ITEM:
                    return MultiStateItem.CreateFromData(data);
                case eItemType.TRANSLATOR_ITEM:
                    return TranslatorItem.CreateFromData(data);
                case eItemType.NUMERICAL_ITEM:
                    return NumericalItem.CreateFromData(data);
                case eItemType.ACTIVATABLE_ITEM:
                    return ActivatableItem.CreateFromData(data, gmc);
                default:
                    return null;
            }
        }
    }
}
