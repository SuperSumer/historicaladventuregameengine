﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class ActivatableItem : Item
    {
        /* -------- Properties -------- */
        public bool Activatable { get; private set; }

        /* -------- Private Fields -------- */
        private List<StageDirection> activationStageDirections = new List<StageDirection>();
        private bool mouseIsOver;
        private Texture2D activatableTexture;
        private Texture2D mouseOverTexture;
        private Texture2D nonActivatableTexture;

        /* -------- Constructors -------- */
        public ActivatableItem(string inID, bool inActivatable, string activatableTextureIDOrPath, string mouseOverTextureIDOrPath, string nonActivatableTextureIDOrPath)
            : base(inID)
        {
            Activatable = inActivatable;
            activatableTexture = ContentLoader.LoadTexture(activatableTextureIDOrPath);
            mouseOverTexture = ContentLoader.LoadTexture(mouseOverTextureIDOrPath);
            nonActivatableTexture = ContentLoader.LoadTexture(nonActivatableTextureIDOrPath);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(Rectangle renderRect, float uiAlpha, SpriteBatch sb)
        {
            Texture2D tex = Activatable ? (mouseIsOver ? mouseOverTexture : activatableTexture) : nonActivatableTexture;
            Color tint = Color.White * uiAlpha;
            sb.Draw(tex, renderRect, tint);
        }

        public virtual void OnMouseOver(GameManagerContainer gmc)
        {
            mouseIsOver = true;
        }
        public virtual void OnMouseOut(GameManagerContainer gmc)
        {
            mouseIsOver = false;
        }
        public virtual void OnActivate(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (!Activatable)
                return;

            if (activationStageDirections.Count > 0)
                gmc.Player.LoadStageDirections(activationStageDirections, null, gtw, gmc, cc);
        }

        public void SetActivatable(bool inActivatable)
        {
            Activatable = inActivatable;
        }

        /* -------- Static Methods -------- */
        public new static ActivatableItem CreateFromData(ItemData data, GameManagerContainer gmc)
        {
            ActivatableItemData aid = data.ActivatableItemData;
            if (aid == null)
                return null;

            ActivatableItem ai = new ActivatableItem(data.ID, aid.Activatable, aid.ActivatableTextureIDOrPath, aid.MouseOverTextureIDOrPath, aid.NonActivatableTextureIDOrPath);

            ai.activationStageDirections.AddRange(StageDirection.CreateFromData(aid.StageDirections, gmc));

            return ai;
        }
    }
}
