﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;
using Microsoft.Xna.Framework;

namespace HistoricalAdventureGameEngine
{
    class MultiStateItem : Item
    {
        /* -------- Private Fields -------- */
        private List<ItemState> states;
        private ItemState currentState;

        /* -------- Constructors -------- */
        public MultiStateItem(string inID, List<ItemState> inStates)
            : base(inID)
        {
            states = inStates;
            currentState = states[0];
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(Rectangle renderRect, float uiAlpha, SpriteBatch sb)
        {
            Color tint = Color.White * uiAlpha;
            sb.Draw(currentState.Texture, renderRect, tint);
        }
        
        public void SetState(string stateID)
        {
            ItemState newState = states.Find(x => x.ID == stateID);
            if (newState != null)
                currentState = newState;
        }
        public void SetState(int idx)
        {
            if (idx >= 0 && idx < states.Count)
                currentState = states[idx];
        }
        public string GetCurrentStateID() => currentState.ID;

        /* -------- Static Methods -------- */
        public static MultiStateItem CreateFromData(ItemData data)
        {
            if (data.MultiStateItemData == null)
                return null;

            List<ItemState> states = new List<ItemState>();
            foreach (ItemStateData isd in data.MultiStateItemData.States)
            {
                ItemState itemState = ItemState.CreateFromData(isd);
                if (itemState != null)
                    states.Add(itemState);
            }

            if (states.Count == 0)
                return null;

            return new MultiStateItem(data.ID, states);
        }
    }
}
