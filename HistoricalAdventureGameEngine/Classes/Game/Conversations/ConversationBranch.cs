﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;

namespace HistoricalAdventureGameEngine
{
    abstract class ConversationBranch
    {
        /* -------- Properties -------- */
        public List<ConversationBranchSpeech> Speeches { get; private set; } = new List<ConversationBranchSpeech>();
        public List<ConversationBranchCondition> Conditions { get; private set; } = new List<ConversationBranchCondition>();
        public List<StageDirection> ActivationStageDirections { get; private set; } = new List<StageDirection>();
        public bool IsGhostBranch => Speeches.Count == 0;

        /* -------- Private Fields -------- */
        private int speechIdx = 0;

        /* -------- Constants -------- */
        private const float MIN_DURATION = 2.0f;

        /* -------- Public Methods -------- */
        public bool ConditionsAreSatisfied(GameManagerContainer gmc, ComponentContainer cc)
        {
            foreach (ConversationBranchCondition cbc in Conditions)
                if (!cbc.IsSatisfied(gmc, cc))
                    return false;

            return true;
        }
        public float GetDuration()
        {
            SoundEffect se = GetSoundEffect();
            if (se != null)
                return Mathf.Max((float)se.Duration.TotalSeconds, MIN_DURATION);

            return MIN_DURATION;
        }

        public void OnActivate()
        {
            speechIdx++;
            if (speechIdx >= Speeches.Count)
                speechIdx = 0;
        }

        public string GetTitle() => Speeches[speechIdx].Title;
        public string GetText() => Speeches[speechIdx].Text;
        public SoundEffect GetSoundEffect() => Speeches[speechIdx].SoundEffect;
    }
}
