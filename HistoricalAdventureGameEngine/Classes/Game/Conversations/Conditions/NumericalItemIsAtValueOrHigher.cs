﻿using System;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class NumericalItemIsAtValueOrHigher : ConversationBranchCondition
    {
        /* -------- Private Fields -------- */
        private string itemID;
        private int value;

        /* -------- Constructors -------- */
        public NumericalItemIsAtValueOrHigher(bool inReversed, string inItemID, int inValue)
            : base(inReversed)
        {
            itemID = inItemID;
            value = inValue;
        }

        /* -------- Public Methods -------- */
        public override bool IsSatisfied(GameManagerContainer gmc, ComponentContainer cc)
        {
            bool isSatisfied;

            NumericalItem numericalItem = gmc.Player.Inventory.Items.Find(x => x is NumericalItem && x.ID == itemID) as NumericalItem;
            if (numericalItem == null)
                isSatisfied = false;
            else
            {
                isSatisfied = numericalItem.Number >= value;
            }

            return reversed ? !isSatisfied : isSatisfied;
        }

        /* -------- Static Methods -------- */
        public new static NumericalItemIsAtValueOrHigher CreateFromData(ConversationBranchConditionData data)
        {
            NumericalItemIsAtValueData niiavd = data.NumericalItemIsAtValueData;
            if (niiavd == null)
                return null;

            NumericalItemIsAtValueOrHigher niiav = new NumericalItemIsAtValueOrHigher(data.Reversed, niiavd.ItemID, niiavd.Value);
            return niiav;
        }
    }
}
