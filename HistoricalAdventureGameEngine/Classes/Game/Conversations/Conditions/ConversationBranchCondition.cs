﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eConversationBranchConditionType
    {
        NUMERICAL_ITEM_IS_AT_VALUE,
        NUMERICAL_ITEM_IS_AT_VALUE_OR_HIGHER,
        FLAG_IS_AT_VALUE
    }

    abstract class ConversationBranchCondition
    {
        /* -------- Private Fields -------- */
        protected bool reversed;

        /* -------- Constructors -------- */
        public ConversationBranchCondition(bool inReversed)
        {
            reversed = inReversed;
        }

        /* -------- Public Methods -------- */
        public abstract bool IsSatisfied(GameManagerContainer gmc, ComponentContainer cc);

        /* -------- Static Methods -------- */
        public static ConversationBranchCondition CreateFromData(ConversationBranchConditionData data)
        {
            if (data == null)
                return null;

            eConversationBranchConditionType type = EnumUtil.Parse<eConversationBranchConditionType>(data.Type);

            switch (type)
            {
                case eConversationBranchConditionType.NUMERICAL_ITEM_IS_AT_VALUE:
                    return NumericalItemIsAtValue.CreateFromData(data);
                case eConversationBranchConditionType.NUMERICAL_ITEM_IS_AT_VALUE_OR_HIGHER:
                    return NumericalItemIsAtValueOrHigher.CreateFromData(data);
                case eConversationBranchConditionType.FLAG_IS_AT_VALUE:
                    return FlagIsAtValue.CreateFromData(data);
                default:
                    return null;
            }
        }
        public static List<ConversationBranchCondition> CreateFromData(ConversationBranchConditionData[] datas)
        {
            List<ConversationBranchCondition> cbcs = new List<ConversationBranchCondition>();

            if (datas == null)
                return cbcs;

            foreach (ConversationBranchConditionData data in datas)
            {
                ConversationBranchCondition cbc = CreateFromData(data);
                if (cbc != null)
                    cbcs.Add(cbc);
            }

            return cbcs;
        }
    }
}
