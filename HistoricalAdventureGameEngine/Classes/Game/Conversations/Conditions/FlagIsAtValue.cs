﻿using System;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class FlagIsAtValue : ConversationBranchCondition
    {
        /* -------- Private Fields -------- */
        private string flagID;
        private bool value;

        /* -------- Constructors -------- */
        public FlagIsAtValue(string inFlagID, bool inValue, bool inReversed)
            : base(inReversed)
        {
            flagID = inFlagID;
            value = inValue;
        }

        /* -------- Public Methods -------- */
        public override bool IsSatisfied(GameManagerContainer gmc, ComponentContainer cc) => gmc.GameVariableManager.FlagValueIsEqual(flagID, value);

        /* -------- Static Methods -------- */
        public new static FlagIsAtValue CreateFromData(ConversationBranchConditionData data)
        {
            if (data.FlagIsAtValueData == null)
                return null;

            FlagIsAtValue fiav = new FlagIsAtValue(data.FlagIsAtValueData.FlagID, data.FlagIsAtValueData.Value, data.Reversed);
            return fiav;
        }
    }
}
