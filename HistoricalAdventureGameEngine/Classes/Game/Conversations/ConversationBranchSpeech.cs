﻿using System;
using Microsoft.Xna.Framework.Audio;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class ConversationBranchSpeech
    {
        /* -------- Properties -------- */
        public string Title { get; private set; }
        public string Text { get; private set; }
        public SoundEffect SoundEffect { get; private set; }

        /* -------- Constructors -------- */
        public ConversationBranchSpeech(string inTitle, string inText, string soundEffectIDOrPath)
        {
            Title = inTitle;
            Text = inText;
            if (!string.IsNullOrEmpty(soundEffectIDOrPath))
                SoundEffect = ContentLoader.LoadSoundEffect(soundEffectIDOrPath);
        }

        /* -------- Static Methods -------- */
        public static ConversationBranchSpeech CreateFromData(ConversationBranchSpeechData data)
        {
            ConversationBranchSpeech cbs = new ConversationBranchSpeech(data.Title, data.Text, data.SoundEffectIDOrPath);
            return cbs;
        }
    }
}
