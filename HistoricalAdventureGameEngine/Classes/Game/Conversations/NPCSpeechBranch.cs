﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class NPCSpeechBranch : ConversationBranch
    {
        /* -------- Properties -------- */
        public List<PlayerSpeechBranch> PlayerBranches { get; private set; } = new List<PlayerSpeechBranch>();

        /* -------- Static Methods -------- */
        public static NPCSpeechBranch CreateFromData(NPCSpeechBranchData data, GameManagerContainer gmc)
        {
            if (data == null)
                return null;

            NPCSpeechBranch branch = new NPCSpeechBranch();

            foreach (ConversationBranchSpeechData cbsd in data.Speeches)
            {
                ConversationBranchSpeech cbs = ConversationBranchSpeech.CreateFromData(cbsd);
                branch.Speeches.Add(cbs);
            }
            branch.Conditions.AddRange(ConversationBranchCondition.CreateFromData(data.Conditions));
            branch.ActivationStageDirections.AddRange(StageDirection.CreateFromData(data.ActivationStageDirections, gmc));
            branch.PlayerBranches.AddRange(PlayerSpeechBranch.CreateFromData(data.PlayerBranches, gmc));

            return branch;
        }
        public static List<NPCSpeechBranch> CreateFromData(NPCSpeechBranchData[] datas, GameManagerContainer gmc)
        {
            List<NPCSpeechBranch> branches = new List<NPCSpeechBranch>();

            if (datas == null)
                return branches;

            foreach (NPCSpeechBranchData data in datas)
            {
                NPCSpeechBranch branch = CreateFromData(data, gmc);
                if (branch != null)
                    branches.Add(branch);
            }

            return branches;
        }
    }
}
