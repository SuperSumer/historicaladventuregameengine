﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class ConversationTree
    {
        /* -------- Properties -------- */
        public string Name { get; private set; }
        public List<NPCSpeechBranch> InitialBranches { get; private set; } = new List<NPCSpeechBranch>();

        /* -------- Constructors -------- */
        public ConversationTree(string inName)
        {
            Name = inName;
        }

        /* -------- Static Methods -------- */
        public static ConversationTree CreateFromData(ConversationTreeData data, GameManagerContainer gmc)
        {
            if (data == null)
                return null;

            ConversationTree tree = new ConversationTree(data.Name);
            tree.InitialBranches = NPCSpeechBranch.CreateFromData(data.NPCBranches, gmc);

            return tree;
        }
    }
}
