﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class PlayerSpeechBranch : ConversationBranch
    {
        /* -------- Properties -------- */
        public List<NPCSpeechBranch> NPCBranches { get; private set; } = new List<NPCSpeechBranch>();
        
        /* -------- Static Methods -------- */
        public static PlayerSpeechBranch CreateFromData(PlayerSpeechBranchData data, GameManagerContainer gmc)
        {
            if (data == null)
                return null;

            PlayerSpeechBranch branch = new PlayerSpeechBranch();

            foreach (ConversationBranchSpeechData cbsd in data.Speeches)
            {
                ConversationBranchSpeech cbs = ConversationBranchSpeech.CreateFromData(cbsd);
                branch.Speeches.Add(cbs);
            }
            branch.Conditions.AddRange(ConversationBranchCondition.CreateFromData(data.Conditions));
            branch.ActivationStageDirections.AddRange(StageDirection.CreateFromData(data.ActivationStageDirections, gmc));
            branch.NPCBranches.AddRange(NPCSpeechBranch.CreateFromData(data.NPCBranches, gmc));

            return branch;
        }
        public static List<PlayerSpeechBranch> CreateFromData(PlayerSpeechBranchData[] datas, GameManagerContainer gmc)
        {
            List<PlayerSpeechBranch> branches = new List<PlayerSpeechBranch>();

            if (datas == null)
                return branches;

            foreach (PlayerSpeechBranchData data in datas)
            {
                PlayerSpeechBranch branch = CreateFromData(data, gmc);
                if (branch != null)
                    branches.Add(branch);
            }

            return branches;
        }
    }
}
