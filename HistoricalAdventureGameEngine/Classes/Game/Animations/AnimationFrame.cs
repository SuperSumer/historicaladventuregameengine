﻿using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class AnimationFrame
    {
        /* -------- Properties -------- */
        public Texture2D Texture { get; private set; }
        public float FrameTime { get; private set; }

        /* -------- Constructors -------- */
        public AnimationFrame(string textureIDOrPath, float inFrameTime)
        {
            Texture = ContentLoader.LoadTexture(textureIDOrPath);
            FrameTime = inFrameTime;
        }
    }
}
