﻿using System.Collections.Generic;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eAnimationPlayType
    {
        PLAY_ONCE,
        REPEAT
    }

    class Animation
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public eAnimationPlayType PlayType { get; private set; }
        public List<AnimationFrame> Frames { get; private set; } = new List<AnimationFrame>();
        public float TotalTime { get; private set; } = 0f;
        public int Width { get; private set; }
        public int Height { get; private set; }

        /* -------- Private Fields -------- */
        private float animationTime;

        /* -------- Constructors -------- */
        public Animation(string inID, eAnimationPlayType inPlayType, SettingsComponent s)
        {
            ID = inID;
            PlayType = inPlayType;
            animationTime = s.ObjectAnimationTime;
        }

        /* -------- Public Methods -------- */
        public void AddFrame(AnimationFrame frame)
        {
            Frames.Add(frame);
            TotalTime += frame.FrameTime;
            if (Frames.Count >= 2)
                TotalTime += animationTime;
        }

        /* -------- Private Methods -------- */
        private void CalculateDimensions()
        {
            foreach (AnimationFrame af in Frames)
            {
                Width = Mathf.Max(Width, af.Texture.Width);
                Height = Mathf.Max(Height, af.Texture.Height);
            }
        }

        /* -------- Static Methods -------- */
        public static Animation CreateFromData(AnimationData data, SettingsComponent s)
        {
            Animation animation = new Animation(data.ID, EnumUtil.Parse<eAnimationPlayType>(data.PlayType), s);
            foreach (AnimationFrameData afd in data.Frames)
                animation.AddFrame(new AnimationFrame(afd.TextureIDOrPath, afd.FrameTime));
            animation.CalculateDimensions();

            return animation;
        }
    }
}
