﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eAnimationPlayStateMode
    {
        SOLID,
        TRANSITIONING
    }

    class AnimationPlayState
    {
        /* -------- Private Fields -------- */
        private eAnimationPlayStateMode mode = eAnimationPlayStateMode.SOLID;
        private Animation animation;
        private int frameIdx;
        private float timer = 0f;
        private float animationTime;

        /* -------- Constructors -------- */
        public AnimationPlayState(Animation inAnimation, SettingsComponent s)
        {
            animation = inAnimation;
            animationTime = s.ObjectAnimationTime;
        }

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw)
        {
            switch (mode)
            {
                case eAnimationPlayStateMode.SOLID:
                    if (frameIdx < animation.Frames.Count - 1 || animation.PlayType == eAnimationPlayType.REPEAT)
                    {
                        // Has more frames to animate
                        timer += gtw.ElapsedSeconds;

                        AnimationFrame frame = animation.Frames[frameIdx];
                        if (timer > frame.FrameTime)
                        {
                            //Now transitioning
                            mode = eAnimationPlayStateMode.TRANSITIONING;
                            timer = 0f;
                            return;
                        }
                    }
                    break;
                case eAnimationPlayStateMode.TRANSITIONING:
                    timer += gtw.ElapsedSeconds;

                    if (timer > animationTime)
                    {
                        // Now solid
                        mode = eAnimationPlayStateMode.SOLID;
                        timer = 0f;
                        frameIdx = GetNextFrameIdx();
                    }
                    break;
                default:
                    break;
            }
        }

        public void Render(RenderTarget2D rt, ArtStyle artStyle, GraphicsComponent g)
        {
            AnimationFrame frame = animation.Frames[frameIdx];

            switch (mode)
            {
                case eAnimationPlayStateMode.SOLID:
                    g.GraphicsDevice.SetRenderTarget(rt);
                    g.GraphicsDevice.Clear(Color.Transparent);

                    g.SpriteBatch.Begin();
                    Rectangle rect = new Rectangle(0, 0, rt.Width, rt.Height);
                    g.SpriteBatch.Draw(frame.Texture, rect, Color.White);
                    g.SpriteBatch.End();
                    break;
                case eAnimationPlayStateMode.TRANSITIONING:
                    float animationVal = Mathf.Clamp01(timer / animationTime);
                    int nextFrameIdx = GetNextFrameIdx();
                    AnimationFrame nextFrame = animation.Frames[nextFrameIdx];

                    artStyle.RenderAnimation(frame.Texture, nextFrame.Texture, rt, animationVal, g);
                    break;
                default:
                    break;
            }
        }

        /* -------- Private Methods -------- */
        private int GetNextFrameIdx()
        {
            if (frameIdx < animation.Frames.Count - 1)
                return frameIdx + 1;

            if (animation.PlayType == eAnimationPlayType.REPEAT)
                return 0;
            else
                return frameIdx;
        }
    }
}
