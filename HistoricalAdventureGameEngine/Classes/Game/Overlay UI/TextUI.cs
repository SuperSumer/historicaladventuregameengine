﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class TextUI : OverlayUI
    {
        /* -------- Private Fields -------- */
        private SpriteFont spriteFont;
        private string text;
        private Color color;

        /* -------- Constructors -------- */
        public TextUI(Point inPosition, string inText, Color inColor, SpriteFont inSpriteFont)
            : base(inPosition, Mathf.Round(inSpriteFont.MeasureString(inText)))
        {
            spriteFont = inSpriteFont;
            text = inText;
            color = inColor;
        }

        /* -------- Public Methods -------- */
        public override void SBDrawUI(float uiAlpha, GameManagerContainer gmc, ComponentContainer cc)
        {
            Color tint = color * uiAlpha;
            cc.GraphicsComponent.SpriteBatch.DrawString(spriteFont, text, Mathf.CastVector(Position), tint);

            base.SBDrawUI(uiAlpha, gmc, cc);
        }
    }
}
