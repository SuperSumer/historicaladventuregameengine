﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class SpeechOptionUI : OverlayUI
    {
        /* -------- Private Fields -------- */
        private PlayerSpeechBranch branch;
        private Action<PlayerSpeechBranch> activationMethod;

        /* -------- Constants -------- */
        private const int GAP_SIZE = 75;
        private const int ICON_SIZE = 100;

        /* -------- Constructors -------- */
        public SpeechOptionUI(Point inPosition, int width, PlayerSpeechBranch inBranch, Action<PlayerSpeechBranch> inActivationMethod, Color color, SpriteFont spriteFont)
            : base(inPosition, new Point(width, 0))
        {
            branch = inBranch;
            activationMethod = inActivationMethod;

            // Child text
            int textWidth = width - GAP_SIZE;
            WrappedTextUI text = new WrappedTextUI(new Point(Position.X + GAP_SIZE, Position.Y), branch.GetTitle(), textWidth, color, spriteFont);
            SetChild(text);

            // Child icon
            Point iconPos = new Point(Position.X, Position.Y + text.Size.Y / 2);
            IconUI icon = new IconUI(iconPos, eIconType.TALK, ICON_SIZE, Activate);
            SetChild(icon);

            Size = new Point(GAP_SIZE + text.Size.X, text.Size.Y);
        }
        
        /* -------- Private Methods -------- */
        private void Activate(GameManagerContainer gmc, ComponentContainer cc)
        {
            activationMethod(branch);
        }
    }
}
