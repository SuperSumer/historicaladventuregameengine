﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class DecodeOptionUI : OverlayUI
    {
        /* -------- Private Fields -------- */
        private DecodeSymbol symbol;
        private Action<DecodeSymbol, GameManagerContainer, ComponentContainer> activationMethod;

        /* -------- Constants -------- */
        private const int GAP_SIZE = 75;
        private const int ICON_SIZE = 60;
        private const int ICON_Y_OFFSET = 15;

        /* -------- Constructors -------- */
        public DecodeOptionUI(Point inPosition, DecodeSymbol inSymbol, int width, Action<DecodeSymbol, GameManagerContainer, ComponentContainer> inActivationMethod, Color color, SpriteFont spriteFont)
            : base(inPosition, new Point(0, 0))
        {
            symbol = inSymbol;
            activationMethod = inActivationMethod;

            // Child text
            WrappedTextUI text = new WrappedTextUI(new Point(Position.X + GAP_SIZE, Position.Y), symbol.Name, width - GAP_SIZE, color, spriteFont);
            SetChild(text);

            // Child icon
            Point iconPos = new Point(Position.X, Position.Y + ICON_Y_OFFSET);
            IconUI icon = new IconUI(iconPos, eIconType.TALK, ICON_SIZE, Activate);
            SetChild(icon);

            Size = new Point(GAP_SIZE + text.Size.X, text.Size.Y);
        }

        /* -------- Private Methods -------- */
        private void Activate(GameManagerContainer gmc, ComponentContainer cc)
        {
            activationMethod(symbol, gmc, cc);
        }
    }
}
