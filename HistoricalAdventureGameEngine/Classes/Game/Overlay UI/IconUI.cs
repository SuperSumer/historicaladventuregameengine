﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class IconUI : ActivatableOverlayUI
    {
        /* -------- Private Fields -------- */
        private eIconType type;
        private Action<GameManagerContainer, ComponentContainer> activationMethod;
        private float size;
        private int defaultSize;
        private bool activating;

        /* -------- Constants -------- */
        private const float DISTANCE_FOR_MOUSE_OVER = 25f;
        private const float SIZE_MULTIPLIER_ON_MOUSE_OVER = 1.3f;
        private const float SIZE_MULTIPLIER_ON_TAP = 1.4f;
        private const float SIZE_MULTIPLIER_AT_START = 0.8f;
        private const float SIZE_LERP_RATE = 12.0f;

        /* -------- Constructors -------- */
        public IconUI(Point inPosition, eIconType inType, int inDefaultSize, Action<GameManagerContainer, ComponentContainer> inActivationMethod)
            : base(inPosition, new Point(inDefaultSize, inDefaultSize))
        {
            type = inType;
            activationMethod = inActivationMethod;
            defaultSize = inDefaultSize;
            size = defaultSize * SIZE_MULTIPLIER_AT_START;
        }

        /* -------- Public Methods -------- */
        public override void Update(Point sceneMousePos, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            bool mouseIsOver = MouseIsOver(sceneMousePos, gmc);
            UpdateSize(gtw, mouseIsOver, cc.SettingsComponent);

            activating = false;

            base.Update(sceneMousePos, gtw, gmc, cc);
        }

        public override void SBDrawUI(float uiAlpha, GameManagerContainer gmc, ComponentContainer cc)
        {
            Texture2D tex;
            switch (type)
            {
                case eIconType.INFORMATION:
                    tex = gmc.UIContentManager.InformationIconTexture;
                    break;
                case eIconType.PICK_UP:
                    tex = gmc.UIContentManager.PickUpIconTexture;
                    break;
                case eIconType.TALK:
                    tex = gmc.UIContentManager.TalkIconTexture;
                    break;
                case eIconType.CUNEIFORM:
                    tex = gmc.UIContentManager.CuneiformIconTexture;
                    break;
                default:
                    tex = gmc.UIContentManager.MoveIconTexture;
                    break;
            }

            float rotation;
            switch (type)
            {
                case eIconType.MOVE_RIGHT:
                    rotation = 0f;
                    break;
                case eIconType.MOVE_DOWN_RIGHT:
                    rotation = Mathf.PiByFour;
                    break;
                case eIconType.MOVE_DOWN:
                    rotation = Mathf.PiByTwo;
                    break;
                case eIconType.MOVE_DOWN_LEFT:
                    rotation = 3f * Mathf.PiByFour;
                    break;
                case eIconType.MOVE_LEFT:
                    rotation = Mathf.Pi;
                    break;
                case eIconType.MOVE_UP_LEFT:
                    rotation = 5f * Mathf.PiByFour;
                    break;
                case eIconType.MOVE_UP:
                    rotation = 3f * Mathf.PiByTwo;
                    break;
                case eIconType.MOVE_UP_RIGHT:
                    rotation = 7f * Mathf.PiByFour;
                    break;
                default:
                    rotation = 0f;
                    break;
            }

            Vector2 pos = new Vector2(Position.X, Position.Y);
            Vector2 origin = new Vector2(tex.Width / 2f, tex.Height / 2f);
            Color tint = Color.White * uiAlpha;
            float scale = size / tex.Width;
            cc.GraphicsComponent.SpriteBatch.Draw(tex, pos, null, tint, rotation, origin, scale, SpriteEffects.None, 0f);

            base.SBDrawUI(uiAlpha, gmc, cc);
        }

        public override Rectangle GetRectangle() => new Rectangle(Position.X - Size.X / 2, Position.Y - Size.Y / 2, Size.X, Size.Y);

        /* -------- Private Methods -------- */
        protected override bool MouseIsOver(Point sceneMousePos, GameManagerContainer gmc)
        {
            if (!gmc.SceneManager.CanInteractWithScene)
                return false;

            float dist = Mathf.PointToPointDistance(sceneMousePos, Position);
            return (dist <= DISTANCE_FOR_MOUSE_OVER);
        }

        protected override void Activate(GameManagerContainer gmc, ComponentContainer cc)
        {
            activationMethod(gmc, cc);
            activating = true;
        }

        private void UpdateSize(GameTimeWrapper gtw, bool mouseIsOver, SettingsComponent s)
        {
            float targetSize = defaultSize;

            // Activating from a tap
            if (s.UseTouchScreen && activating)
            {
                targetSize *= SIZE_MULTIPLIER_ON_TAP;
                size = targetSize;
                Size = new Point(Mathf.Round(size), Mathf.Round(size));
                return;
            }

            if (mouseIsOver && !s.UseTouchScreen)
                targetSize *= SIZE_MULTIPLIER_ON_MOUSE_OVER;

            size = Mathf.Lerp(size, targetSize, gtw.ElapsedSeconds * SIZE_LERP_RATE, true);
            Size = new Point(Mathf.Round(size), Mathf.Round(size));
        }
    }
}
