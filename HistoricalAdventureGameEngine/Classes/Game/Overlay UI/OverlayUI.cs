﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace HistoricalAdventureGameEngine
{
    abstract class OverlayUI
    {
        /* -------- Properties -------- */
        public Point Position { get; protected set; }
        public Point Size { get; protected set; }

        /* -------- Private Fields -------- */
        private OverlayUI parent;
        private List<OverlayUI> children = new List<OverlayUI>();

        /* -------- Constructors -------- */
        public OverlayUI(Point inPosition, Point inSize)
        {
            Position = inPosition;
            Size = inSize;
        }

        /* -------- Public Methods -------- */
        public virtual void Update(Point sceneMousePos, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            foreach (OverlayUI child in children)
                child.Update(sceneMousePos, gtw, gmc, cc);
        }
        public virtual void SBDrawUI(float uiAlpha, GameManagerContainer gmc, ComponentContainer cc)
        {
            foreach (OverlayUI child in children)
                child.SBDrawUI(uiAlpha, gmc, cc);
        }

        public virtual void SetPosition(Point newPosition)
        {
            Point diff = newPosition - Position;
            Position = newPosition;

            foreach (OverlayUI child in children)
                child.SetPosition(child.Position + diff);
        }
        public virtual Rectangle GetRectangle() => new Rectangle(Position, Size);

        /* -------- Private Methods -------- */
        protected void SetChild(OverlayUI ui)
        {
            children.Add(ui);
            ui.parent = this;
        }
    }
}
