﻿using Microsoft.Xna.Framework;

namespace HistoricalAdventureGameEngine
{
    abstract class ActivatableOverlayUI : OverlayUI
    {
        /* -------- Constructors -------- */
        public ActivatableOverlayUI(Point inPosition, Point inSize)
            : base(inPosition, inSize)
        {
        }

        /* -------- Public Methods -------- */
        public override void Update(Point sceneMousePos, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (MouseIsOver(sceneMousePos, gmc) && cc.InputComponent.MouseButtonIsPressing(eMouseButton.LEFT))
                Activate(gmc, cc);

            base.Update(sceneMousePos, gtw, gmc, cc);
        }

        /* -------- Private Methods -------- */
        protected abstract bool MouseIsOver(Point sceneMousePos, GameManagerContainer gmc);
        protected abstract void Activate(GameManagerContainer gmc, ComponentContainer cc);
    }
}
