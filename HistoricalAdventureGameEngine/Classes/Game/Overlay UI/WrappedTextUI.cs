﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class WrappedTextUI : TextUI
    {
        /* -------- Constructors -------- */
        public WrappedTextUI(Point inPosition, string inText, int maxWidth, Color inColor, SpriteFont inSpriteFont)
            : base(inPosition, StringUtil.InsertLineBreaksForWidth(inText, inSpriteFont, maxWidth), inColor, inSpriteFont)
        {
        }
    }
}
