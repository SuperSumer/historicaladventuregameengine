﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class GameManagerContainer
    {
        /* -------- Properties -------- */
        public SceneManager SceneManager { get; private set; } = new SceneManager();
        public ArtStyleManager ArtStyleManager { get; private set; } = new ArtStyleManager();
        public RenderingPipelineManager RenderingPipelineManager { get; private set; } = new RenderingPipelineManager();
        public InfoNodeManager InfoNodeManager { get; private set; } = new InfoNodeManager();
        public InfoMessageManager InfoMessageManager { get; private set; } = new InfoMessageManager();
        public ItemManager ItemManager { get; private set; } = new ItemManager();
        public GameVariableManager GameVariableManager { get; private set; } = new GameVariableManager();
        public SceneMouseOverManager SceneMouseOverManager { get; private set; } = new SceneMouseOverManager();
        public UIContentManager UIContentManager { get; private set; } = new UIContentManager();
        public TimerManager TimerManager { get; private set; } = new TimerManager();
        public ScoreManager ScoreManager { get; private set; } = new ScoreManager();
        public StageDirectionManager StageDirectionManager { get; private set; } = new StageDirectionManager();
        public Camera Camera { get; private set; } = new Camera();
        public Player Player { get; private set; } = new Player();

        /* -------- Constructors -------- */
        public GameManagerContainer(ComponentContainer cc)
        {
            LoadData(cc);
        }

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw, ComponentContainer cc)
        {
            UpdateCamera(gtw, cc);

            SceneMouseOverManager.Update(gtw, this, cc);

            SceneManager.Update(gtw, this, cc);
            Player.Update(gtw, this, cc);

            InfoNodeManager.Update(cc.InputComponent);
            InfoMessageManager.Update(cc.InputComponent);

            TimerManager.Update(gtw, this, cc);
        }
        public void Draw(ComponentContainer cc)
        {
            Scene scene = SceneManager.ActiveScene;
            if (scene == null)
                return;

            if (scene.UsesRenderingPipeline())
            {
                RenderingPipeline rp = RenderingPipelineManager.GetRenderingPipeline(SceneManager.ActiveScene);
                rp.Render(scene, ArtStyleManager.ArtStyle, this, cc);
            }
            else
                scene.Draw(cc);
        }

        /* -------- Private Methods -------- */
        private void LoadData(ComponentContainer cc)
        {
            // Load game data
            string gamePath = XmlUtil.GetXmlFilePath(cc.SettingsComponent.GameFilename);
            GameData gameData = XmlUtil.DeserializeFromXml<GameData>(gamePath);
            if (gameData == null)
            {
                FatalErrorHandler.ActivateFatalError("Game could not be loaded");
                return;
            }
            
            // Load texture lists
            foreach (string textureListFilename in gameData.TextureListFilenames)
            {
                string xmlPath = XmlUtil.GetXmlFilePath(textureListFilename);
                AssetData[] textureList = XmlUtil.DeserializeFromXml<AssetData[]>(xmlPath);
                if (textureList == null)
                    continue;

                foreach (AssetData ali in textureList)
                    ContentLoader.AddTextureListItem(ali);
            }

            // Load sound effect lists
            foreach (string soundEffectListFilename in gameData.SoundEffectListFilenames)
            {
                string xmlPath = XmlUtil.GetXmlFilePath(soundEffectListFilename);
                AssetData[] soundEffectListList = XmlUtil.DeserializeFromXml<AssetData[]>(xmlPath);
                if (soundEffectListList == null)
                    continue;

                foreach (AssetData ali in soundEffectListList)
                    ContentLoader.AddSoundEffectListItem(ali);
            }

            // Load art style
            ArtStyleManager.SetArtStyle(gameData.ArtStyle, cc);

            // Load animation lists
            Dictionary<string, Animation> animationDict = new Dictionary<string, Animation>();
            foreach (string animationListFilename in gameData.AnimationListFilenames)
            {
                string xmlPath = XmlUtil.GetXmlFilePath(animationListFilename);
                AnimationData[] animationList = XmlUtil.DeserializeFromXml<AnimationData[]>(xmlPath);
                if (animationList == null)
                    continue;

                foreach (AnimationData ad in animationList)
                    animationDict.Add(ad.ID, Animation.CreateFromData(ad, cc.SettingsComponent));
            }

            // Load info nodes
            InfoNodeManager.ScorePerInfoNode = gameData.ScorePerInfoNode;
            InfoNodeManager.LoadData(gameData.InfoNodes);

            // Initialize logging
            Logging.Initialize(this, cc);

            // Load items
            foreach (ItemData id in gameData.Items)
            {
                Item item = Item.CreateFromData(id, this);
                if (item == null)
                    continue;

                ItemManager.AddItem(item);
            }
            foreach (string itemID in gameData.InitialInventoryItemIDs)
            {
                Item item = ItemManager.GetItem(itemID);
                if (item != null)
                    Player.Inventory.Items.Add(item);
            }

            // Load scene object lists
            Dictionary<string, SceneObject> sceneObjectDict = new Dictionary<string, SceneObject>();
            foreach (string sceneObjectListFilename in gameData.SceneObjectListFilenames)
            {
                string xmlPath = XmlUtil.GetXmlFilePath(sceneObjectListFilename);
                SceneObjectData[] sceneObjectList = XmlUtil.DeserializeFromXml<SceneObjectData[]>(xmlPath);
                if (sceneObjectList == null)
                    continue;

                foreach (SceneObjectData sod in sceneObjectList)
                    if (!sceneObjectDict.ContainsKey(sod.ID))
                        sceneObjectDict.Add(sod.ID, SceneObject.CreateFromData(sod, animationDict, this, cc));
            }

            // Load scene lists
            foreach (string sceneListFilename in gameData.SceneListFilenames)
            {
                string xmlPath = XmlUtil.GetXmlFilePath(sceneListFilename);
                SceneData[] sceneList = XmlUtil.DeserializeFromXml<SceneData[]>(xmlPath);
                if (sceneList == null)
                    continue;

                foreach (SceneData sd in sceneList)
                {
                    Scene scene = Scene.CreateFromData(sd,sceneObjectDict, animationDict, this, cc);
                    if (scene != null)
                        SceneManager.AddScene(scene);
                }
            }
            if (!SceneManager.SetActiveScene(gameData.InitialSceneID, null, this, cc))
                FatalErrorHandler.ActivateFatalError($"Could not set initial scene {gameData.InitialSceneID}");

            // Create rendering pipelines
            foreach (Scene scene in SceneManager.Scenes)
                RenderingPipelineManager.CreatePipeline(scene.Width, scene.Height, cc);
            
            // Load UI Content
            UIContentManager.LoadData(gameData.UIContent);
        }

        private void UpdateCamera(GameTimeWrapper gtw, ComponentContainer cc)
        {
            Scene scene = SceneManager.ActiveScene;
            if (scene == null)
                return;

            if (!scene.UsesRenderingPipeline())
                return;
            
            RenderingPipeline rp = RenderingPipelineManager.GetRenderingPipeline(SceneManager.ActiveScene);
            Point sceneMousePos = scene.ConvertScreenPosToScenePos(cc.InputComponent.MousePos);
            Camera.Update(gtw, rp.DepthBuffer, sceneMousePos, scene, SceneMouseOverManager, cc);
        }
    }
}
