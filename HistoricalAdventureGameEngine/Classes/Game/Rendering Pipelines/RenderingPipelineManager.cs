﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace HistoricalAdventureGameEngine
{
    class RenderingPipelineManager
    {
        /* -------- Private Fields -------- */
        private Dictionary<Point, RenderingPipeline> pipelineDict = new Dictionary<Point, RenderingPipeline>();

        /* -------- Public Methods -------- */
        public void CreatePipeline(int width, int height, ComponentContainer cc)
        {
            Point res = new Point(width, height);
            if (!pipelineDict.ContainsKey(res))
                pipelineDict.Add(res, new RenderingPipeline(width, height, cc));
        }

        public RenderingPipeline GetRenderingPipeline(Scene scene)
        {
            Point res = new Point(scene.Width, scene.Height);
            return pipelineDict[res];
        }
    }
}
