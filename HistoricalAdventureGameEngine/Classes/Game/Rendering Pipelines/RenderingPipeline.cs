﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class RenderingPipeline
    {
        /* -------- Properties -------- */
        public int Width { get; private set; }
        public int Height { get; private set; }
        public DepthMap DepthBuffer => depthBuffer;

        /* -------- Private Fields -------- */
        private RenderTarget2D geometryBuffer;
        private GaussianBlurRenderer blurredGeometryBuffer;
        private RenderTarget2D focusedGeometryBuffer;
        private DepthMap depthBuffer;
        private RenderTarget2D focusBuffer;
        private RenderTarget2D postProcessedBuffer;
        // Effects
        private Effect spriteColorEffect;
        private Effect focusEffect;
        private Effect focusSelectionEffect;

        /* -------- Constructors -------- */
        public RenderingPipeline(int inWidth, int inHeight, ComponentContainer cc)
        {
            Width = inWidth;
            Height = inHeight;

            GraphicsDevice device = cc.GraphicsComponent.GraphicsDevice;

            geometryBuffer = new RenderTarget2D(device, Width, Height);
            GaussianBlurSettings gaussSettings = new GaussianBlurSettings(cc.SettingsComponent.GaussianDownsamples, cc.SettingsComponent.GaussianKernelSize);
            blurredGeometryBuffer = new GaussianBlurRenderer(Width, Height, device, gaussSettings);
            focusedGeometryBuffer = new RenderTarget2D(device, Width, Height);
            depthBuffer = new DepthMap(Width, Height, device);
            focusBuffer = new RenderTarget2D(device, Width, Height);
            postProcessedBuffer = new RenderTarget2D(device, Width, Height);

            spriteColorEffect = ContentLoader.LoadEffect("Effects/SpriteColor");
            focusEffect = ContentLoader.LoadEffect("Effects/Focus");
            focusSelectionEffect = ContentLoader.LoadEffect("Effects/FocusSelection");
        }
        ~RenderingPipeline()
        {
            geometryBuffer.Dispose();
            focusedGeometryBuffer.Dispose();
            focusBuffer.Dispose();
            postProcessedBuffer.Dispose();
        }

        /* -------- Public Methods -------- */
        public void Render(Scene scene, ArtStyle artStyle, GameManagerContainer gmc, ComponentContainer cc)
        {
            scene.DrawIndividualRenderTargets(artStyle, cc.GraphicsComponent);
            RenderGeometryBuffer(scene, artStyle, cc.GraphicsComponent);
            blurredGeometryBuffer.Render(geometryBuffer, cc.GraphicsComponent.GraphicsDevice, cc.GraphicsComponent.SpriteBatch);
            RenderDepthMap(scene, cc.GraphicsComponent);
            RenderFocusBuffer(scene, gmc, cc);
            RenderFocusedGeometryBuffer(scene, cc);
            artStyle.PerformPostProcessing(focusedGeometryBuffer, postProcessedBuffer, scene.AnimationValue, cc.GraphicsComponent);
            RenderUI(scene, artStyle, gmc, cc);
            RenderToScreen(scene, cc.GraphicsComponent);
        }

        /* -------- Private Methods -------- */
        private void RenderGeometryBuffer(Scene scene, ArtStyle artStyle, GraphicsComponent g)
        {
            g.GraphicsDevice.SetRenderTarget(geometryBuffer);
            g.GraphicsDevice.Clear(Color.Black);
            g.SpriteBatch.Begin();
            scene.SBDrawGeometry(g);
            g.SpriteBatch.End();
        }
        private void RenderDepthMap(Scene scene, GraphicsComponent g)
        {
            g.GraphicsDevice.SetRenderTarget(depthBuffer.RenderTarget);
            g.GraphicsDevice.Clear(Color.Black);

            g.SpriteBatch.Begin();
            scene.SBDrawBGGeometryDepth(g);
            g.SpriteBatch.End();

            g.SpriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, spriteColorEffect);
            scene.SBDrawGeometryDepth(g);
            g.SpriteBatch.End();
        }
        private void RenderFocusBuffer(Scene scene, GameManagerContainer gmc, ComponentContainer cc)
        {
            GraphicsDevice device = cc.GraphicsComponent.GraphicsDevice;

            device.SetRenderTarget(focusBuffer);
            device.Clear(Color.Transparent);
            device.SetVertexBuffer(GraphicsUtil.FullTargetVertexBuffer);
            device.Indices = GraphicsUtil.FullTargetIndexBuffer;

            focusEffect.CurrentTechnique = focusEffect.Techniques["LinearFocus"];
            focusEffect.Parameters["xDepthMap"].SetValue(depthBuffer.RenderTarget);
            focusEffect.Parameters["xFocalDepth"].SetValue(gmc.Camera.FocalDepth);

            foreach (EffectPass pass in focusEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 2);
            }
        }
        private void RenderFocusedGeometryBuffer(Scene scene, ComponentContainer cc)
        {
            GraphicsDevice device = cc.GraphicsComponent.GraphicsDevice;

            device.SetRenderTarget(focusedGeometryBuffer);
            device.Clear(Color.Transparent);
            device.SetVertexBuffer(GraphicsUtil.FullTargetVertexBuffer);
            device.Indices = GraphicsUtil.FullTargetIndexBuffer;

            focusSelectionEffect.CurrentTechnique = focusSelectionEffect.Techniques["FocusBlur"];
            focusSelectionEffect.Parameters["xFrame"].SetValue(geometryBuffer);
            focusSelectionEffect.Parameters["xBlurFrame"].SetValue(blurredGeometryBuffer.Output);
            focusSelectionEffect.Parameters["xFocusMap"].SetValue(focusBuffer);
            focusSelectionEffect.Parameters["xBlurMultiplier"].SetValue(cc.SettingsComponent.BlurMultiplier);

            foreach (EffectPass pass in focusSelectionEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 2);
            }
        }
        private void RenderUI(Scene scene, ArtStyle artStyle, GameManagerContainer gmc, ComponentContainer cc)
        {
            float uiAlpha = artStyle.GetUIAlpha(scene.AnimationValue);

            cc.GraphicsComponent.GraphicsDevice.SetRenderTarget(postProcessedBuffer);
            cc.GraphicsComponent.SpriteBatch.Begin();
            scene.SBDrawUI(uiAlpha, gmc, cc);
            if (!gmc.InfoMessageManager.InfoMessageIsActive)
                gmc.InfoNodeManager.SBDrawUI(gmc, cc);
            gmc.InfoMessageManager.SBDrawUI(gmc, cc);
            cc.GraphicsComponent.SpriteBatch.End();
        }
        private void RenderToScreen(Scene scene, GraphicsComponent g)
        {
            g.GraphicsDevice.SetRenderTarget(null);
            g.GraphicsDevice.Clear(Color.Black);

            g.SpriteBatch.Begin();
            g.SpriteBatch.Draw(postProcessedBuffer, scene.ScreenRenderRectangle, Color.White);
            g.SpriteBatch.End();
        }
    }
}
