﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class PlainArtStyle : ArtStyle
    {
        /* -------- Private Fields -------- */
        private Texture2D pixTex;

        /* -------- Constructors -------- */
        public PlainArtStyle()
        {
            pixTex = ContentLoader.LoadTexture("Textures/WhitePixel");
        }

        /* -------- Public Methods -------- */
        public override void PerformPostProcessing(Texture2D focusedGeometryBuffer, RenderTarget2D rt, float animation, GraphicsComponent g)
        {
            g.GraphicsDevice.SetRenderTarget(rt);
            g.GraphicsDevice.Clear(Color.Transparent);

            g.SpriteBatch.Begin();
            Rectangle rect = new Rectangle(0, 0, rt.Width, rt.Height);
            g.SpriteBatch.Draw(focusedGeometryBuffer, rect, Color.White);
            Color fadeTint = Color.Black * (1f - animation);
            g.SpriteBatch.Draw(pixTex, rect, fadeTint);
            g.SpriteBatch.End();
        }
        public override void RenderAnimation(Texture2D texture1, Texture2D texture2, RenderTarget2D rt, float animation, GraphicsComponent g)
        {
            g.GraphicsDevice.SetRenderTarget(rt);
            g.GraphicsDevice.Clear(Color.Transparent);

            g.SpriteBatch.Begin();
            Rectangle rect = new Rectangle(0, 0, rt.Width, rt.Height);
            Texture2D tex = animation < 0.5f ? texture1 : texture2;
            g.SpriteBatch.Draw(tex, rect, Color.White);
            g.SpriteBatch.End();
        }
        public override float GetUIAlpha(float animation) => animation;
    }
}
