﻿using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class ArtStyleManager
    {
        /* -------- Properties -------- */
        public ArtStyle ArtStyle { get; private set; }

        /* -------- Public Methods -------- */
        public void SetArtStyle(ArtStyleData data, ComponentContainer cc)
        {
            ArtStyle newStyle = ArtStyle.CreateFromData(data, cc);
            if (newStyle == null)
                ArtStyle = new PlainArtStyle();
            else
                ArtStyle = newStyle;
        }
    }
}
