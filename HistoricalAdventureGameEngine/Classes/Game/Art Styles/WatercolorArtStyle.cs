﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using DataTypes;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    class WatercolorArtStyle : ArtStyle
    {
        /* -------- Private Fields -------- */
        private Dictionary<Point, WatercolorRenderer> rendererDict = new Dictionary<Point, WatercolorRenderer>();
        private WatercolorSettings settings;
        // Animation graphics objects
        private VertexPositionTwoTexture[] vertices;
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        private Effect maskEffect;
        private Texture2D perlinBlurTex;
        private Texture vignetteTex;

        /* -------- Constants -------- */
        private const float ANIMATION_FOR_ZERO_UI_ALPHA = 0.7f;
        private readonly float[] MODIFIED_ANIMATION_VALUES = { 0f, 0.4f, 0.6f, 0.8f, 1f };

        /* -------- Constructors -------- */
        public WatercolorArtStyle(WatercolorSettings inSettings, GraphicsComponent g)
        {
            settings = inSettings;

            CreateBuffers(g);
            maskEffect = ContentLoader.LoadEffect("Effects/Watercolor/MaskTwo");
            perlinBlurTex = ContentLoader.LoadTexture("Textures/Watercolor/PerlinBlurred");
            vignetteTex = ContentLoader.LoadTexture("Textures/Watercolor/Vignette");
        }

        /* -------- Public Methods -------- */
        public override void PerformPostProcessing(Texture2D focusedGeometryBuffer, RenderTarget2D rt, float animation, GraphicsComponent g)
        {
            WatercolorRenderer renderer = GetRenderer(new Point(rt.Width, rt.Height), g.GraphicsDevice);
            renderer.Render(focusedGeometryBuffer, ModifyAnimation(animation), g.GraphicsDevice, g.SpriteBatch);

            g.GraphicsDevice.SetRenderTarget(rt);
            g.SpriteBatch.Begin();
            Rectangle rect = new Rectangle(0, 0, rt.Width, rt.Height);
            g.SpriteBatch.Draw(renderer.Output, rect, Color.White);
            g.SpriteBatch.End();
        }
        public override void RenderAnimation(Texture2D texture1, Texture2D texture2, RenderTarget2D rt, float animation, GraphicsComponent g)
        {
            float modifiedAnimation = ModifyAnimation(animation);
            UpdateBuffers(rt.Width, rt.Height);

            g.GraphicsDevice.SetRenderTarget(rt);
            g.GraphicsDevice.Clear(Color.Transparent);

            g.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            g.GraphicsDevice.Indices = indexBuffer;

            maskEffect.CurrentTechnique = maskEffect.Techniques["MaskTwo"];
            maskEffect.Parameters["xTexture1"].SetValue(texture1);
            maskEffect.Parameters["xTexture2"].SetValue(texture2);
            maskEffect.Parameters["xMask"].SetValue(perlinBlurTex);
            maskEffect.Parameters["xVignette"].SetValue(vignetteTex);
            maskEffect.Parameters["xThreshold"].SetValue(1f - modifiedAnimation);

            foreach (EffectPass pass in maskEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                g.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, indexBuffer.IndexCount / 3);
            }
        }
        public override float GetUIAlpha(float animation) => Mathf.GetLerpFactor(ANIMATION_FOR_ZERO_UI_ALPHA, 1f, animation, true);

        /* -------- Private Methods -------- */
        private WatercolorRenderer GetRenderer(Point res, GraphicsDevice device)
        {
            if (rendererDict.ContainsKey(res))
                return rendererDict[res];

            WatercolorRenderer renderer = new WatercolorRenderer(res.X, res.Y, settings, device);
            rendererDict.Add(res, renderer);
            return renderer;
        }
        private float ModifyAnimation(float animation)
        {
            int arrayLowerIdx = Mathf.ClampUpper(Mathf.Floor(animation * (MODIFIED_ANIMATION_VALUES.Length - 1)), MODIFIED_ANIMATION_VALUES.Length - 2);
            int arrayUpperIdx = arrayLowerIdx + 1;

            float animationLower = arrayLowerIdx / (float)(MODIFIED_ANIMATION_VALUES.Length - 1);
            float animationUpper = arrayUpperIdx / (float)(MODIFIED_ANIMATION_VALUES.Length - 1);

            float modifiedLower = MODIFIED_ANIMATION_VALUES[arrayLowerIdx];
            float modifiedUpper = MODIFIED_ANIMATION_VALUES[arrayUpperIdx];

            float lerpFactor = Mathf.GetLerpFactor(animationLower, animationUpper, animation);
            float modifiedAnimation = Mathf.Lerp(modifiedLower, modifiedUpper, lerpFactor);

            return modifiedAnimation;
        }

        private void CreateBuffers(GraphicsComponent g)
        {
            vertices = new VertexPositionTwoTexture[4];
            vertices[0] = new VertexPositionTwoTexture(new Vector3(-1f, 1f, 0f), new Vector2(0f, 0f), Vector2.Zero);
            vertices[1] = new VertexPositionTwoTexture(new Vector3(1f, 1f, 0f), new Vector2(1f, 0f), Vector2.Zero);
            vertices[2] = new VertexPositionTwoTexture(new Vector3(-1f, -1f, 0f), new Vector2(0f, 1f), Vector2.Zero);
            vertices[3] = new VertexPositionTwoTexture(new Vector3(1f, -1f, 0f), new Vector2(1f, 1f), Vector2.Zero);

            vertexBuffer = new VertexBuffer(g.GraphicsDevice, VertexPositionTwoTexture.VertexDeclaration, 4, BufferUsage.WriteOnly);

            int[] indices = new int[]
            {
                0, 1, 2,
                2, 1, 3
            };
            indexBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, 6, BufferUsage.WriteOnly);
            indexBuffer.SetData(indices);
        }
        private void UpdateBuffers(int width, int height)
        {
            vertices[0].TextureCoordinate2 = GraphicsUtil.GetFittedTopLeftTextureCoord(width, height);
            vertices[1].TextureCoordinate2 = GraphicsUtil.GetFittedTopRightTextureCoord(width, height);
            vertices[2].TextureCoordinate2 = GraphicsUtil.GetFittedBottomLeftTextureCoord(width, height);
            vertices[3].TextureCoordinate2 = GraphicsUtil.GetFittedBottomRightTextureCoord(width, height);

            vertexBuffer.SetData(vertices);
        }

        /* -------- Static Methods -------- */
        public new static WatercolorArtStyle CreateFromData(ArtStyleData data, ComponentContainer cc)
        {
            if (data.WatercolorArtStyleData == null)
                return null;

            WatercolorSettings settings = WatercolorSettings.CreateFromData(data.WatercolorArtStyleData);
            if (settings == null)
                return null;

            WatercolorArtStyle was = new WatercolorArtStyle(settings, cc.GraphicsComponent);
            return was;
        }
    }
}
