﻿using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eArtStyleType
    {
        PLAIN_ART_STYLE,
        WATERCOLOR_ART_STYLE
    }

    abstract class ArtStyle
    {
        /* -------- Public Methods -------- */
        public abstract void PerformPostProcessing(Texture2D focusedGeometryBuffer, RenderTarget2D rt, float animation, GraphicsComponent g);
        public abstract void RenderAnimation(Texture2D texture1, Texture2D texture2, RenderTarget2D rt, float animation, GraphicsComponent g);
        public abstract float GetUIAlpha(float animation);

        /* -------- Static Methods -------- */
        public static ArtStyle CreateFromData(ArtStyleData data, ComponentContainer cc)
        {
            if (data == null)
                return null;

            eArtStyleType type = EnumUtil.Parse<eArtStyleType>(data.Type);

            switch (type)
            {
                case eArtStyleType.PLAIN_ART_STYLE:
                    return new PlainArtStyle();
                case eArtStyleType.WATERCOLOR_ART_STYLE:
                    return WatercolorArtStyle.CreateFromData(data, cc);
                default:
                    return null;
            }
        }
    }
}
