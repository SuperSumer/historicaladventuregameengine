﻿using System.Collections.Generic;

namespace HistoricalAdventureGameEngine
{
    class Player
    {
        /* -------- Properties -------- */
        public string LastSceneID { get; set; }
        public Inventory Inventory { get; private set; } = new Inventory();

        /* -------- Private Fields -------- */
        private StageDirectionActivator stageDirectionActivator = new StageDirectionActivator();

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            stageDirectionActivator.Update(null, gtw, gmc, cc);
        }

        public void LoadStageDirections(List<StageDirection> stageDirections, SceneObject so, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            // Empty to prevent stuck stage directions from jamming new ones
            if (stageDirectionActivator.Empty())
                Debug.LogWarning("Some new player stage directions were loaded by there were still some stage directions left in the activator");

            stageDirectionActivator.LoadStageDirections(stageDirections, so, gtw, gmc, cc);
        }
        public bool IsRunningStageDirections() => !stageDirectionActivator.IsEmpty();
        public void EmptyStageDirections()
        {
            stageDirectionActivator.Empty();
        }
    }
}
