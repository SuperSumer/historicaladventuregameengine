﻿using System.Collections.Generic;

namespace HistoricalAdventureGameEngine
{
    class Inventory
    {
        /* -------- Properties -------- */
        public List<Item> Items { get; private set; } = new List<Item>();

        /* -------- Public Methods -------- */
        public void ActivateTranslatorItems()
        {
            foreach (Item i in Items)
            {
                TranslatorItem ti = i as TranslatorItem;
                if (ti == null)
                    continue;

                ti.Active = true;
            }
        }
        public void DeactivateTranslatorItems()
        {
            foreach (Item i in Items)
            {
                TranslatorItem ti = i as TranslatorItem;
                if (ti == null)
                    continue;

                ti.Active = false;
            }
        }
    }
}
