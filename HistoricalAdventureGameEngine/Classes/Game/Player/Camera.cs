﻿using Microsoft.Xna.Framework;

namespace HistoricalAdventureGameEngine
{
    class Camera
    {
        /* -------- Properties -------- */
        public Vector2 Position { get; private set; } = Vector2.Zero;
        public float FocalDepth { get; private set; } = 0f;

        /* -------- Private Fields -------- */
        private Point targetMousePos = Point.Zero;
        private bool firstFrame = true;

        /* -------- Constants -------- */
        private const float POSITION_LERP_RATE = 2.0f;
        private const float FOCAL_DEPTH_LERP_RATE = 2.0f;

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw, DepthMap depthMap, Point sceneMousePos, Scene scene, SceneMouseOverManager smom, ComponentContainer cc)
        {
            // Position
            if (firstFrame)
            {
                targetMousePos = new Point(scene.Width / 2, scene.Height / 2);
                firstFrame = false;
            }
            if (cc.SettingsComponent.UseTouchScreen)
            {
                if (cc.InputComponent.MouseButtonIsPressingOrPressed(eMouseButton.LEFT))
                    targetMousePos = sceneMousePos;
            }
            else
                targetMousePos = sceneMousePos;
            Vector2 targetPosition = new Vector2(2f * targetMousePos.X / scene.Width - 1f, 1f - 2f * targetMousePos.Y / scene.Height);
            Position = Vector2.Lerp(Position, targetPosition, gtw.ElapsedSeconds * POSITION_LERP_RATE);

            // Focal depth
            float targetFocalDepth = 0f;
            if (!scene.ForcesFocalDepth(ref targetFocalDepth))
            {
                if (smom.MouseOverItem != null)
                    targetFocalDepth = 0f;
                else if (smom.MouseOverObject != null)
                    targetFocalDepth = smom.MouseOverObject.Depth;
                else
                {
                    depthMap.CopyPixelData();
                    targetFocalDepth = depthMap.GetDepth(targetMousePos);
                }
            }
            FocalDepth = Mathf.Lerp(FocalDepth, targetFocalDepth, gtw.ElapsedSeconds * FOCAL_DEPTH_LERP_RATE, true);
        }
    }
}
