﻿using System.Collections.Generic;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eSceneMode
    {
        STATIC,
        FADING_OUT,
        FADING_IN
    }

    class SceneManager
    {
        /* -------- Properties -------- */
        public Scene ActiveScene { get; private set; }
        public List<Scene> Scenes { get; private set; } = new List<Scene>();
        public eSceneMode Mode { get; private set; } = eSceneMode.FADING_IN;
        public bool CanInteractWithScene => Mode == eSceneMode.STATIC;

        /* -------- Private Fields -------- */
        private float timer = 0f;
        private Scene requestedScene;
        private bool fadeOutStarting = false;

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (ActiveScene == null)
                return;

            if (fadeOutStarting)
            {
                ActiveScene.OnStartFadingOut(gmc, cc);
                fadeOutStarting = false;
            }

            // Mode state machine
            switch (Mode)
            {
                case eSceneMode.STATIC:
                    break;
                case eSceneMode.FADING_IN:
                    timer += gtw.ElapsedSeconds;
                    if (timer >= cc.SettingsComponent.SceneAnimationTime)
                    {
                        ActiveScene.OnFinishFadingIn(gmc, cc);
                        Mode = eSceneMode.STATIC;
                        timer = 0f;
                    }
                    break;
                case eSceneMode.FADING_OUT:
                    timer += gtw.ElapsedSeconds;
                    if (timer >= cc.SettingsComponent.SceneAnimationTime)
                    {
                        ChangeScene(requestedScene, gtw, gmc, cc);
                        requestedScene = null;
                        Mode = eSceneMode.FADING_IN;
                        timer = 0f;
                    }
                    break;
                default:
                    break;
            }

            // Calculate animation
            float animation;
            switch (Mode)
            {
                case eSceneMode.STATIC:
                    animation = 1f;
                    break;
                case eSceneMode.FADING_IN:
                    animation = Mathf.Clamp01(timer / cc.SettingsComponent.SceneAnimationTime);
                    break;
                case eSceneMode.FADING_OUT:
                    animation = 1f - Mathf.Clamp01(timer / cc.SettingsComponent.SceneAnimationTime);
                    break;
                default:
                    animation = 1f;
                    break;
            }
            
            foreach (Scene scene in Scenes)
            {
                bool isActiveScene = scene == ActiveScene;
                scene.Update(animation, isActiveScene, gtw, gmc, cc);
            }
        }

        public void AddScene(Scene scene)
        {
            Scenes.Add(scene);
        }
        public bool SetActiveScene(string id, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            Scene newScene = Scenes.Find(x => x.ID == id);
            
            if (newScene != null)
            {
                if (ActiveScene != newScene)
                    ChangeScene(newScene, gtw, gmc, cc);

                return true;
            }

            return false;
        }

        public bool RequestSceneChange(string id)
        {
            requestedScene = Scenes.Find(x => x.ID == id);
            if (requestedScene == null)
                return false;

            fadeOutStarting = true;
            Mode = eSceneMode.FADING_OUT;
            return true;
        }

        public bool MouseIsDownOnBackground(SceneMouseOverManager smom, bool onlyPressing, InputComponent i)
        {
            if (!CanInteractWithScene)
                return false;
            
            if (onlyPressing)
                if (!i.MouseButtonIsPressing(eMouseButton.LEFT))
                    return false;
            else
                if (!i.MouseButtonIsPressingOrPressed(eMouseButton.LEFT))
                    return false;

            if (smom.MouseOverItem != null)
                return false;
            if (smom.MouseOverObject != null)
                return false;

            return true;
        }

        /* -------- Private Methods -------- */
        private void ChangeScene(Scene newScene, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            Logging.LogSceneChange(ActiveScene, newScene);

            gmc.Player.LastSceneID = ActiveScene?.ID;

            ActiveScene?.OnBecomeNonActive(gtw, gmc, cc);
            ActiveScene = newScene;
            ActiveScene.OnBecomeActive(gtw, gmc, cc);
        }
    }
}
