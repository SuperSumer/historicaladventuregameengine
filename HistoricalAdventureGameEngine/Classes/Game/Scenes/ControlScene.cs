﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using DataTypes;
using System;

namespace HistoricalAdventureGameEngine
{
    class ControlScene : Scene
    {
        /* -------- Private Fields -------- */
        private string controlMessage;
        private string nextSceneID;
        private Color backgroundColor;
        private Color textColor;
        private SpriteFont spriteFont;
        private bool showContinueMessage = false;

        /* -------- Constants -------- */
        private const string CONTINUE_MESSAGE = "Click to Continue";
        private const string TOUCH_SCREEN_CONTINUE_MESSAGE = "Tap to Continue";
        private const float CONTINUE_MESSAGE_BOTTOM_GAP = 30f;

        /* -------- Constructors -------- */
        public ControlScene(string inID, int inWidth, int inHeight, string inControlMessage, string inNextSceneID, Color inBackgroundColor, Color inTextColor, GraphicsComponent g)
            : base(inID, inWidth, inHeight, g)
        {
            controlMessage = inControlMessage;
            nextSceneID = inNextSceneID;
            backgroundColor = inBackgroundColor;
            textColor = inTextColor;

            spriteFont = ContentLoader.LoadSpriteFont("Sprite Fonts/Arial48B");
        }

        /* -------- Public Methods -------- */
        public override void Update(float animation, bool isActiveScene, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (isActiveScene)
            {
                if (gmc.SceneManager.CanInteractWithScene && cc.InputComponent.MouseButtonIsPressing(eMouseButton.LEFT))
                {
                    if (string.IsNullOrEmpty(nextSceneID) || !gmc.SceneManager.RequestSceneChange(nextSceneID))
                        cc.ProgramFlowComponent.RequestGameExit();
                }

                showContinueMessage = gmc.SceneManager.CanInteractWithScene;
            }

            base.Update(animation, isActiveScene, gtw, gmc, cc);
        }
        public override void Draw(ComponentContainer cc)
        {
            GraphicsComponent g = cc.GraphicsComponent;
            g.GraphicsDevice.SetRenderTarget(null);
            g.GraphicsDevice.Clear(backgroundColor);

            g.SpriteBatch.Begin();

            if (!string.IsNullOrEmpty(controlMessage))
            {
                Vector2 textSize = spriteFont.MeasureString(controlMessage);
                Vector2 textPosition = new Vector2(g.HalfScreenWidth, g.HalfScreenHeight) - textSize * 0.5f;
                Color textTint = textColor * AnimationValue;
                g.SpriteBatch.DrawString(spriteFont, controlMessage, textPosition, textTint);
            }

            if (showContinueMessage)
            {
                Vector2 continueMsgSize = spriteFont.MeasureString(GetContinueMessage(cc.SettingsComponent));
                Vector2 pos = new Vector2(g.HalfScreenWidth - continueMsgSize.X / 2f, g.ScreenHeight - continueMsgSize.Y - CONTINUE_MESSAGE_BOTTOM_GAP);
                g.SpriteBatch.DrawString(spriteFont, GetContinueMessage(cc.SettingsComponent), pos, textColor);
            }

            g.SpriteBatch.End();
        }
        public override void SBDrawGeometry(GraphicsComponent g) { /* Method not used */ }
        public override void SBDrawBGGeometryDepth(GraphicsComponent g) { /* Method not used */ }
        public override void SBDrawGeometryDepth(GraphicsComponent g) { /* Method not used */ }
        public override void DrawIndividualRenderTargets(ArtStyle artStyle, GraphicsComponent g) { /* Method not used */ }

        public override bool UsesRenderingPipeline() => false;
        public override bool ForcesFocalDepth(ref float focalDepth) => false;

        /* -------- Private Methods -------- */
        protected override bool RendersCursor() => false;
        protected override bool RendersInventory() => false;
        protected override bool RendersTimers() => false;
        protected override bool RendersScore() => false;

        private string GetContinueMessage(SettingsComponent s) => s.UseTouchScreen ? TOUCH_SCREEN_CONTINUE_MESSAGE : CONTINUE_MESSAGE;

        /* -------- Static Methods -------- */
        public static ControlScene CreateFromData(SceneData data, GraphicsComponent g)
        {
            ControlSceneData csd = data.ControlSceneData;
            if (csd == null)
                return null;

            ControlScene cs = new ControlScene(data.ID, data.Width, data.Height, csd.ControlMessage, csd.NextSceneID, csd.BackgroundColor, csd.TextColor, g);
            return cs;
        }
    }
}
