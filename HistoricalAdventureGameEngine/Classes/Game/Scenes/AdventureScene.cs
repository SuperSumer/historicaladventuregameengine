﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using DataTypes;
using System;

namespace HistoricalAdventureGameEngine
{
    class AdventureScene : ObjectPopulatedScene
    {
        /* -------- Constructors -------- */
        public AdventureScene(string inID, int inWidth, int inHeight, Color inLightColor, Color inSkyColor, Color inGroundColor, int inHorizonY, string cloudTextureIDOrPath, int inCloudMovementSpeed, string horizonTextureIDOrPath, GraphicsComponent g)
            : base(inID, inWidth, inHeight, inLightColor, inSkyColor, inGroundColor, inHorizonY, cloudTextureIDOrPath, inCloudMovementSpeed, horizonTextureIDOrPath, g)
        {
        }

        /* -------- Public Methods -------- */
        public override bool ForcesFocalDepth(ref float focalDepth) => false;

        /* -------- Private Methods -------- */
        protected override bool RendersInventory() => true;
        protected override bool RendersTimers() => true;
        protected override bool RendersScore() => true;

        /* -------- Static Methods -------- */
        public new static AdventureScene CreateFromData(SceneData data, Dictionary<string, SceneObject> sceneObjectDict, Dictionary<string, Animation> animationDict, GameManagerContainer gmc, ComponentContainer cc)
        {
            ObjectPopulatedSceneData opsd = data.ObjectPopulatedSceneData;
            AdventureSceneData asd = data.AdventureSceneData;

            if (opsd == null || asd == null)
                return null;

            AdventureScene scene = new AdventureScene(data.ID, data.Width, data.Height, opsd.LightColor, opsd.SkyColor, opsd.GroundColor, opsd.HorizonY, opsd.CloudTextureIDOrPath, opsd.CloudMovementSpeed, opsd.HorizonTextureIDOrPath, cc.GraphicsComponent);

            // Add scene objects
            if (opsd.SceneObjectReferences != null)
            {
                foreach (SceneObjectReference sor in opsd.SceneObjectReferences)
                {
                    SceneObject so = SceneObject.CreateFromReference(sor, sceneObjectDict);
                    if (so != null)
                        scene.AddSceneObject(so);
                }
            }

            // Create scene objects
            if (opsd.SceneObjects != null)
            {
                foreach (SceneObjectData sod in opsd.SceneObjects)
                {
                    SceneObject so = SceneObject.CreateFromData(sod, animationDict, gmc, cc);
                    if (so != null)
                        scene.AddSceneObject(so);
                }
            }

            return scene;
        }
    }
}
