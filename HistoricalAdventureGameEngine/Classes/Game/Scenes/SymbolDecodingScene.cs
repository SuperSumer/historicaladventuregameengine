﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eSymbolDecodingSceneMode
    {
        WAITING_FOR_INTERACTABILITY,
        CHOOSING_DECODE_OPTION,
        FADING_IN_DECODE,
        WAITING_FOR_EXIT,
        EXITING
    }

    class SymbolDecodingScene : Scene
    {
        /* -------- Private Fields -------- */
        private eSymbolDecodingSceneMode mode = eSymbolDecodingSceneMode.WAITING_FOR_INTERACTABILITY;
        private string returnSceneID;
        private Texture2D backgroundTex;
        private Texture2D pixTex;
        private Texture2D decodeTex;
        private Point decodePosition;
        private List<DecodeSymbol> decodeSymbols = new List<DecodeSymbol>();
        private List<DecodeSymbol> decodeOptions = new List<DecodeSymbol>();
        private List<DecodeSymbol> decodedSymbols = new List<DecodeSymbol>();
        private DecodeSymbol currentDecodeSymbol;
        private SpriteFont spriteFont;
        private Color textColor = Color.White;
        private float timer = 0f;
        private bool createOptionUI = false;
        private int scorePerDecode;

        /* -------- Constants -------- */
        private const float FADE_IN_TIME = 1.0f;
        private const string SYMBOL_TO_DECODE_TEXT = "Symbol to Decode";
        private readonly Point SYMBOL_TO_DECODE_TEXT_TOP_MIDDLE_POSITION = new Point(200, 70);
        private readonly Point SYMBOL_TOP_MIDDLE_POSITION = new Point(200, 120);
        private readonly Point FIRST_DECODE_OPTION_POSITION = new Point(50, 290);
        private const int DECODE_OPTION_MAX_WIDTH = 250;
        private const int DECODE_OPTION_Y_OFFSET = 50;
        private const int BOTTOM_GAP = 100;
        private const string CHOOSE_OPTION_MESSAGE = "Choose the English Word to Apply to the Symbol.";
        private const string CONTINUE_MESSAGE = "Click to Continue";
        private const string TOUCH_SCREEN_CONTINUE_MESSAGE = "Tap to Continue";

        /* -------- Constructors -------- */
        public SymbolDecodingScene(string inID, int inWidth, int inHeight, string inReturnSceneID, string backgroundTexIdOrPath, string decodeTexIDOrPath, Point inDecodePosition, GraphicsComponent g)
            : base(inID, inWidth, inHeight, g)
        {
            returnSceneID = inReturnSceneID;
            backgroundTex = ContentLoader.LoadTexture(backgroundTexIdOrPath);
            decodeTex = ContentLoader.LoadTexture(decodeTexIDOrPath);
            decodePosition = inDecodePosition;

            pixTex = ContentLoader.LoadTexture("Textures/WhitePixel");
            spriteFont = ContentLoader.LoadSpriteFont("Sprite Fonts/Arial20");
        }

        /* -------- Public Methods -------- */
        public override void Update(float animation, bool isActiveScene, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (isActiveScene)
            {
                if (createOptionUI)
                    CreateOptionUI();

                switch (mode)
                {
                    case eSymbolDecodingSceneMode.WAITING_FOR_INTERACTABILITY:
                        if (gmc.SceneManager.CanInteractWithScene)
                            GoToChoosingDecodeOption();
                        break;
                    case eSymbolDecodingSceneMode.CHOOSING_DECODE_OPTION:
                        break;
                    case eSymbolDecodingSceneMode.FADING_IN_DECODE:
                        timer += gtw.ElapsedSeconds;
                        if (timer >= FADE_IN_TIME)
                        {
                            timer = 0f;
                            decodedSymbols.Add(currentDecodeSymbol);
                            if (decodedSymbols.Count == decodeSymbols.Count)
                                GoToWaitingForExit();
                            else
                                GoToChoosingDecodeOption();
                        }
                        break;
                    case eSymbolDecodingSceneMode.WAITING_FOR_EXIT:
                        if (cc.InputComponent.MouseButtonIsPressing(eMouseButton.LEFT))
                            GoToExiting(gmc);
                        break;
                    case eSymbolDecodingSceneMode.EXITING:
                        break;
                }
            }

            base.Update(animation, isActiveScene, gtw, gmc, cc);
        }

        public override void Draw(ComponentContainer cc) { /* Method not used */ }
        public override void DrawIndividualRenderTargets(ArtStyle artStyle, GraphicsComponent g) { /* Method not used */ }
        public override void SBDrawGeometry(GraphicsComponent g)
        {
            Rectangle rect = new Rectangle(0, 0, Width, Height);
            g.SpriteBatch.Draw(backgroundTex, rect, Color.White);
        }
        public override void SBDrawBGGeometryDepth(GraphicsComponent g)
        {
            Rectangle rect = new Rectangle(0, 0, Width, Height);
            g.SpriteBatch.Draw(pixTex, rect, Color.Black);
        }
        public override void SBDrawGeometryDepth(GraphicsComponent g) { /* Method not used */ }
        public override void SBDrawUI(float uiAlpha, GameManagerContainer gmc, ComponentContainer cc)
        {
            // Decode texture
            Color tint = Color.White * uiAlpha;
            Rectangle decodeTexRect = new Rectangle(decodePosition, new Point(decodeTex.Width, decodeTex.Height));
            cc.GraphicsComponent.SpriteBatch.Draw(decodeTex, decodeTexRect, tint);

            // Decoded symbols
            foreach (DecodeSymbol ds in decodedSymbols)
            {
                if (ds == currentDecodeSymbol)
                    continue;

                Rectangle dsRect = new Rectangle(decodePosition, new Point(ds.DecodeTexture.Width, ds.DecodeTexture.Height));
                cc.GraphicsComponent.SpriteBatch.Draw(ds.DecodeTexture, dsRect, tint);
            }

            // Current decode symbol
            if (currentDecodeSymbol != null)
            {
                // Decode
                Rectangle dsRect = new Rectangle(decodePosition, new Point(currentDecodeSymbol.DecodeTexture.Width, currentDecodeSymbol.DecodeTexture.Height));
                float dsAlpha;
                switch (mode)
                {
                    case eSymbolDecodingSceneMode.FADING_IN_DECODE:
                        dsAlpha = Mathf.Clamp01(timer / FADE_IN_TIME) * uiAlpha;
                        break;
                    case eSymbolDecodingSceneMode.CHOOSING_DECODE_OPTION:
                        dsAlpha = 0f;
                        break;
                    case eSymbolDecodingSceneMode.WAITING_FOR_EXIT:
                        dsAlpha = uiAlpha;
                        break;
                    case eSymbolDecodingSceneMode.EXITING:
                        dsAlpha = uiAlpha;
                        break;
                    default:
                        dsAlpha = 0f;
                        break;
                }
                Color dsTint = Color.White * dsAlpha;
                cc.GraphicsComponent.SpriteBatch.Draw(currentDecodeSymbol.DecodeTexture, dsRect, dsTint);

                if (mode == eSymbolDecodingSceneMode.CHOOSING_DECODE_OPTION || mode == eSymbolDecodingSceneMode.FADING_IN_DECODE)
                {
                    // Symbol text
                    Vector2 symbolTextSize = spriteFont.MeasureString(SYMBOL_TO_DECODE_TEXT);
                    Vector2 symbolTextPos = new Vector2(SYMBOL_TO_DECODE_TEXT_TOP_MIDDLE_POSITION.X - symbolTextSize.X / 2f, SYMBOL_TO_DECODE_TEXT_TOP_MIDDLE_POSITION.Y);
                    Color symbolTextTint = textColor * uiAlpha;
                    cc.GraphicsComponent.SpriteBatch.DrawString(spriteFont, SYMBOL_TO_DECODE_TEXT, symbolTextPos, symbolTextTint);

                    // Symbol
                    Rectangle symbolRect = new Rectangle(SYMBOL_TOP_MIDDLE_POSITION.X - currentDecodeSymbol.SymbolTexture.Width / 2, SYMBOL_TOP_MIDDLE_POSITION.Y, currentDecodeSymbol.SymbolTexture.Width, currentDecodeSymbol.SymbolTexture.Height);
                    cc.GraphicsComponent.SpriteBatch.Draw(currentDecodeSymbol.SymbolTexture, symbolRect, tint);
                }
            }

            // Message text
            if (mode == eSymbolDecodingSceneMode.CHOOSING_DECODE_OPTION || mode == eSymbolDecodingSceneMode.WAITING_FOR_EXIT)
            {
                string message = mode == eSymbolDecodingSceneMode.CHOOSING_DECODE_OPTION ? CHOOSE_OPTION_MESSAGE : GetContinueMessage(cc.SettingsComponent);
                Vector2 messageSize = spriteFont.MeasureString(message);
                Vector2 messagePos = new Vector2(Width / 2f - messageSize.X / 2f, Height - BOTTOM_GAP - messageSize.Y);
                Color messageTint = textColor * uiAlpha;
                cc.GraphicsComponent.SpriteBatch.DrawString(spriteFont, message, messagePos, messageTint);
            }

            base.SBDrawUI(uiAlpha, gmc, cc);
        }

        public override bool UsesRenderingPipeline() => true;
        public override bool ForcesFocalDepth(ref float focalDepth)
        {
            focalDepth = 0f;
            return true;
        }

        public override void OnBecomeActive(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            decodedSymbols.Clear();
            mode = eSymbolDecodingSceneMode.WAITING_FOR_INTERACTABILITY;
            ui.Clear();

            base.OnBecomeActive(gtw, gmc, cc);
        }

        /* -------- Private Methods -------- */
        protected override bool RendersCursor() => true;
        protected override bool RendersInventory() => false;
        protected override bool RendersTimers() => false;
        protected override bool RendersScore() => true;

        private void GoToChoosingDecodeOption()
        {
            currentDecodeSymbol = GetNextSymbolToDecode();
            if (currentDecodeSymbol == null)
            {
                GoToWaitingForExit();
                return;
            }

            mode = eSymbolDecodingSceneMode.CHOOSING_DECODE_OPTION;
            createOptionUI = true;
        }
        private void GoToFadingInDecode()
        {
            mode = eSymbolDecodingSceneMode.FADING_IN_DECODE;
        }
        private void GoToWaitingForExit()
        {
            mode = eSymbolDecodingSceneMode.WAITING_FOR_EXIT;
            ui.Clear();
        }
        private void GoToExiting(GameManagerContainer gmc)
        {
            mode = eSymbolDecodingSceneMode.EXITING;
            if (!gmc.SceneManager.RequestSceneChange(returnSceneID))
                Debug.LogWarning("Symbol decoding return scene could not be successfully requested!");
        }

        private void CreateOptionUI()
        {
            ui.Clear();

            Point pos = FIRST_DECODE_OPTION_POSITION;

            foreach (DecodeSymbol ds in decodeOptions)
            {
                if (decodedSymbols.Contains(ds))
                    continue;

                DecodeOptionUI doui = new DecodeOptionUI(pos, ds, DECODE_OPTION_MAX_WIDTH, ChooseDecodeOption, textColor, spriteFont);
                ui.Add(doui);
                pos.Y += DECODE_OPTION_Y_OFFSET;
            }

            createOptionUI = false;
        }

        private void ChooseDecodeOption(DecodeSymbol ds, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (mode != eSymbolDecodingSceneMode.CHOOSING_DECODE_OPTION)
                return;

            if (ds == currentDecodeSymbol)
            {
                // Right choice
                if (gmc.UIContentManager.RightChoiceSoundEffect != null)
                    cc.AudioComponent.PlaySoundEffect_FireAndForget(gmc.UIContentManager.RightChoiceSoundEffect);
                gmc.ScoreManager.IncreaseScore(scorePerDecode);

                GoToFadingInDecode();
            }
            else
            {
                // Wrong choice
                if (gmc.UIContentManager.WrongChoiceSoundEffect != null)
                    cc.AudioComponent.PlaySoundEffect_FireAndForget(gmc.UIContentManager.WrongChoiceSoundEffect);
            }
        }

        private DecodeSymbol GetNextSymbolToDecode()
        {
            List<DecodeSymbol> possibleSymbols = decodeSymbols.FindAll(x => !decodedSymbols.Contains(x));
            if (possibleSymbols.Count == 0)
                return null;

            int idx = 0; // Choose first symbol, so go through them in order
            return possibleSymbols[idx];
        }

        private string GetContinueMessage(SettingsComponent s) => s.UseTouchScreen ? TOUCH_SCREEN_CONTINUE_MESSAGE : CONTINUE_MESSAGE;

        /* -------- Static Methods -------- */
        public static SymbolDecodingScene CreateFromData(SceneData data, GameManagerContainer gmc, ComponentContainer cc)
        {
            SymbolDecodingSceneData sdsd = data.SymbolDecodingSceneData;

            if (sdsd == null)
                return null;

            SymbolDecodingScene sds = new SymbolDecodingScene(data.ID, data.Width, data.Height, sdsd.ReturnSceneID, sdsd.BackgroundTextureIDOrPath, sdsd.DecodeTextureIDOrPath, sdsd.DecodePosition, cc.GraphicsComponent);

            sds.decodeSymbols.AddRange(DecodeSymbol.CreateFromData(sdsd.DecodeSymbols));
            sds.decodeOptions.AddRange(sds.decodeSymbols);
            RandomUtil.RandomizeOrder(sds.decodeOptions);
            sds.scorePerDecode = sdsd.ScorePerDecode;

            return sds;
        }
    }
}
