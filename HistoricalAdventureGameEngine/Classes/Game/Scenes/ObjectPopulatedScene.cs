﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HistoricalAdventureGameEngine
{
    abstract class ObjectPopulatedScene : Scene
    {
        /* -------- Properties -------- */
        public List<SceneObject> SceneObjects { get; private set; } = new List<SceneObject>();
        public Color LightColor { get; private set; }
        public Color SkyColor { get; private set; }
        public Color GroundColor { get; private set; }
        public int HorizonY { get; private set; }
        public Texture2D CloudTexture { get; private set; }
        public int CloudMovementSpeed { get; private set; }
        public Texture2D HorizonTexture { get; private set; }

        /* -------- Private Fields -------- */
        private Texture2D pixTex;
        private Texture2D groundDepthTex;
        private int cloudXOffset;

        /* -------- Constructors -------- */
        public ObjectPopulatedScene(string inID, int inWidth, int inHeight, Color inLightColor, Color inSkyColor, Color inGroundColor, int inHorizonY, string cloudTextureIDOrPath, int inCloudMovementSpeed, string horizonTextureIDOrPath, GraphicsComponent g)
            : base(inID, inWidth, inHeight, g)
        {
            LightColor = inLightColor;
            SkyColor = inSkyColor;
            GroundColor = inGroundColor;
            HorizonY = inHorizonY;
            if (!string.IsNullOrEmpty(cloudTextureIDOrPath))
                CloudTexture = ContentLoader.LoadTexture(cloudTextureIDOrPath);
            CloudMovementSpeed = inCloudMovementSpeed;
            if (!string.IsNullOrEmpty(horizonTextureIDOrPath))
                HorizonTexture = ContentLoader.LoadTexture(horizonTextureIDOrPath);

            pixTex = ContentLoader.LoadTexture("Textures/WhitePixel");
            groundDepthTex = ContentLoader.LoadTexture("Textures/DepthGradient");
        }

        /* -------- Public Methods -------- */
        public override void Update(float animation, bool isActiveScene, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            foreach (SceneObject so in SceneObjects)
                so.Update(gtw, gmc, cc);

            if (isActiveScene)
            {
                if (CloudTexture != null)
                {
                    cloudXOffset += CloudMovementSpeed;
                    if (cloudXOffset >= CloudTexture.Width)
                        cloudXOffset -= CloudTexture.Width;
                }
            }

            base.Update(animation, isActiveScene, gtw, gmc, cc);
        }

        public override void Draw(ComponentContainer cc) { /* Method not used */ }
        public override void SBDrawGeometry(GraphicsComponent g)
        {
            // Draw sky
            Rectangle skyRect = new Rectangle(0, 0, Width, HorizonY);
            g.SpriteBatch.Draw(pixTex, skyRect, SkyColor);

            // Draw clouds
            if (CloudTexture != null)
            {
                int cloudY = (CloudTexture.Height > HorizonY) ? HorizonY - CloudTexture.Height : 0;
                int cloudX;

                // Draw to the right of the offset
                cloudX = cloudXOffset;
                while (cloudX < Width)
                {
                    Rectangle cloudRect = new Rectangle(cloudX, cloudY, CloudTexture.Width, CloudTexture.Height);
                    g.SpriteBatch.Draw(CloudTexture, cloudRect, LightColor);
                    cloudX += CloudTexture.Width;
                }

                // Draw to the left of the offset
                cloudX = cloudXOffset - CloudTexture.Width;
                while (cloudX >= -CloudTexture.Width)
                {
                    Rectangle cloudRect = new Rectangle(cloudX, cloudY, CloudTexture.Width, CloudTexture.Height);
                    g.SpriteBatch.Draw(CloudTexture, cloudRect, LightColor);
                    cloudX -= CloudTexture.Width;
                }
            }

            // Draw horizon texture
            if (HorizonTexture != null)
            {
                int textureHeight = Mathf.Round(HorizonTexture.Height * ((float)Width / HorizonTexture.Width));
                Rectangle horizonTexRect = new Rectangle(0, HorizonY - textureHeight, Width, textureHeight);
                g.SpriteBatch.Draw(HorizonTexture, horizonTexRect, LightColor);
            }
            
            // Draw ground
            Rectangle groundRect = new Rectangle(0, HorizonY, Width, Height - HorizonY);
            g.SpriteBatch.Draw(pixTex, groundRect, GraphicsUtil.ElementWiseMultiplyColors(GroundColor, LightColor));

            // Draw objects
            foreach (SceneObject so in SceneObjects)
            {
                GeometrySceneObject gso = so as GeometrySceneObject;
                if (gso == null)
                    continue;

                gso.SBDrawGeometry(g.SpriteBatch, LightColor);
            }
        }
        public override void SBDrawBGGeometryDepth(GraphicsComponent g)
        {
            Rectangle skyRect = new Rectangle(0, 0, Width, HorizonY);
            g.SpriteBatch.Draw(pixTex, skyRect, Color.White);

            Rectangle groundRect = new Rectangle(0, HorizonY, Width, Height - HorizonY);
            g.SpriteBatch.Draw(groundDepthTex, groundRect, Color.White);
        }
        public override void SBDrawGeometryDepth(GraphicsComponent g)
        {
            foreach (SceneObject so in SceneObjects)
            {
                GeometrySceneObject gso = so as GeometrySceneObject;
                if (gso == null)
                    continue;

                gso.SBDrawDepth(g.SpriteBatch);
            }
        }
        public override void SBDrawUI(float uiAlpha, GameManagerContainer gmc, ComponentContainer cc)
        {
            foreach (SceneObject so in SceneObjects)
            {
                UISceneObject uiso = so as UISceneObject;
                if (uiso == null)
                    continue;

                uiso.SBDrawUI(uiAlpha, cc.GraphicsComponent.SpriteBatch, gmc);
            }

            base.SBDrawUI(uiAlpha, gmc, cc);
        }
        public override void DrawIndividualRenderTargets(ArtStyle artStyle, GraphicsComponent g)
        {
            foreach (SceneObject so in SceneObjects)
                so.DrawRenderTarget(artStyle, g);
        }

        public override bool UsesRenderingPipeline() => true;

        public override void OnBecomeActive(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            SetPlayerCharacterShowing(gmc.Player.LastSceneID);

            base.OnBecomeActive(gtw, gmc, cc);
        }

        public void AddSceneObject(SceneObject so)
        {
            SceneObjects.Add(so);
            SceneObjects.Sort(SceneObject.Compare);
            SceneObjects.Reverse();
        }

        public void SetAllSceneObjectsToInitialVisibilityAndActivity()
        {
            foreach (SceneObject so in SceneObjects)
                so.SetToInitialVisibilityAndActivity();
        }
        public void SetPlayerCharacterShowing(string lastSceneID)
        {
            foreach (SceneObject so in SceneObjects)
            {
                PlayerCharacterSceneObject pcso = so as PlayerCharacterSceneObject;
                if (pcso != null)
                    pcso.SetShowing(lastSceneID);
            }
        }

        /* -------- Private Methods -------- */
        protected override bool RendersCursor() => true;
    }
}
