﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;
using System;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eConversationSceneMode
    {
        WAITING_FOR_INTERACTABILITY,
        NPC_SPEAKING,
        PLAYER_CHOOSING_SPEECH,
        PLAYER_SPEAKING,
        WAITING_FOR_EXIT,
        EXITING
    }

    class ConversationScene : ObjectPopulatedScene
    {
        /* -------- Private Fields -------- */
        private string returnSceneID;
        private ConversationTree conversationTree;
        private float forcedFocalDepth;
        private eConversationSceneMode mode = eConversationSceneMode.WAITING_FOR_INTERACTABILITY;
        private float timer = 0f;
        private NPCSpeechBranch npcBranch;
        private PlayerSpeechBranch playerBranch;
        private List<PlayerSpeechBranch> choosablePlayerBranches;
        private SpriteFont spriteFont;
        private Color npcTextColor = Color.Yellow;
        private Color playerTextColor = Color.White;
        // Flags
        private bool clearAllUI = false;
        private bool createNPCSpeechUI = false;
        private bool createPlayerSpeechChoiceUI = false;
        private bool createPlayerSpeechUI = false;
        private bool createExitUI = false;
        private bool playerBranchChosen = false;

        /* -------- Constants -------- */
        private const float SPEECH_FRAME_WIDTH_FRACTION = 0.9f;
        private const float SPEECH_CHOICES_WIDTH_FRACTION = 0.6f;
        private const int BOTTOM_GAP = 100;
        private const int PLAYER_SPEECH_OPTION_GAP = 75;
        private const int NAME_GAP_X = 30;
        private const int NAME_GAP_Y = 80;
        private const int EXIT_ICON_SIZE = 100;

        /* -------- Constructors -------- */
        public ConversationScene(string inID, int inWidth, int inHeight, Color inLightColor, Color inSkyColor, Color inGroundColor, int inHorizonY, string cloudTextureIDOrPath, int inCloudMovementSpeed, string horizonTextureIDOrPath, string inReturnSceneID, float inForcedFocalDepth, GameManagerContainer gmc, GraphicsComponent g)
            : base(inID, inWidth, inHeight, inLightColor, inSkyColor, inGroundColor, inHorizonY, cloudTextureIDOrPath, inCloudMovementSpeed, horizonTextureIDOrPath, g)
        {
            returnSceneID = inReturnSceneID;
            forcedFocalDepth = inForcedFocalDepth;

            spriteFont = ContentLoader.LoadSpriteFont("Sprite Fonts/Arial20");
        }

        /* -------- Public Methods -------- */
        public override void Update(float animation, bool isActiveScene, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (isActiveScene)
            {
                if (clearAllUI)
                {
                    ui.Clear();
                    clearAllUI = false;
                }
                if (createNPCSpeechUI)
                    CreateNPCSpeechUI();
                if (createPlayerSpeechChoiceUI)
                    CreatePlayerSpeechChoiceUI();
                if (createPlayerSpeechUI)
                    CreatePlayerSpeechUI();
                if (createExitUI)
                    CreateExitUI();

                switch (mode)
                {
                    case eConversationSceneMode.WAITING_FOR_INTERACTABILITY:
                        if (gmc.SceneManager.CanInteractWithScene)
                        {
                            // Finished fading in
                            if (conversationTree == null)
                            {
                                GoToWaitingForExit();
                                break;
                            }

                            npcBranch = ChooseNPCBranch(conversationTree.InitialBranches, gmc, cc);
                            if (npcBranch == null)
                            {
                                GoToWaitingForExit();
                                break;
                            }

                            GoToNPCSpeech(gtw, gmc, cc);
                        }
                        break;
                    case eConversationSceneMode.NPC_SPEAKING:
                        timer += gtw.ElapsedSeconds;
                        if (timer > npcBranch.GetDuration() || npcBranch.IsGhostBranch)
                            GoToPlayerChoosingSpeech(gmc, cc);
                        break;
                    case eConversationSceneMode.PLAYER_CHOOSING_SPEECH:
                        if (playerBranchChosen)
                        {
                            GoToPlayerSpeech(gtw, gmc, cc);
                            playerBranchChosen = false;
                        }
                        break;
                    case eConversationSceneMode.PLAYER_SPEAKING:
                        timer += gtw.ElapsedSeconds;
                        if (timer > playerBranch.GetDuration() || playerBranch.IsGhostBranch)
                        {
                            // Try to choose NPC speech
                            npcBranch = ChooseNPCBranch(playerBranch.NPCBranches, gmc, cc);
                            if (npcBranch == null)
                            {
                                GoToWaitingForExit();
                                break;
                            }

                            GoToNPCSpeech(gtw, gmc, cc);
                        }
                        break;
                    case eConversationSceneMode.WAITING_FOR_EXIT:
                        break;
                    case eConversationSceneMode.EXITING:
                        break;
                    default:
                        break;
                }
            }
            
            base.Update(animation, isActiveScene, gtw, gmc, cc);
        }

        public override void SBDrawUI(float uiAlpha, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (!string.IsNullOrEmpty(conversationTree.Name))
            {
                Vector2 namePos = new Vector2(NAME_GAP_X, NAME_GAP_Y);
                Color tint = npcTextColor * uiAlpha;
                cc.GraphicsComponent.SpriteBatch.DrawString(spriteFont, conversationTree.Name, namePos, tint);
            }

            base.SBDrawUI(uiAlpha, gmc, cc);
        }

        public override bool ForcesFocalDepth(ref float focalDepth)
        {
            focalDepth = forcedFocalDepth;
            return true;
        }

        public override void OnBecomeActive(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            mode = eConversationSceneMode.WAITING_FOR_INTERACTABILITY;
            ui.Clear();

            base.OnBecomeActive(gtw, gmc, cc);
        }
        public override void OnStartFadingOut(GameManagerContainer gmc, ComponentContainer cc)
        {
            gmc.Player.Inventory.DeactivateTranslatorItems();

            base.OnStartFadingOut(gmc, cc);
        }
        public override void OnFinishFadingIn(GameManagerContainer gmc, ComponentContainer cc)
        {
            gmc.Player.Inventory.ActivateTranslatorItems();

            base.OnFinishFadingIn(gmc, cc);
        }

        /* -------- Private Methods -------- */
        protected override bool RendersInventory() => true;
        protected override bool RendersTimers() => true;
        protected override bool RendersScore() => true;

        private NPCSpeechBranch ChooseNPCBranch(List<NPCSpeechBranch> branches, GameManagerContainer gmc, ComponentContainer cc)
        {
            foreach (NPCSpeechBranch branch in branches)
                if (branch.ConditionsAreSatisfied(gmc, cc))
                    return branch;

            return null;
        }
        private PlayerSpeechBranch ChoosePlayerBranch(List<PlayerSpeechBranch> branches, GameManagerContainer gmc, ComponentContainer cc)
        {
            foreach (PlayerSpeechBranch branch in branches)
                if (branch.ConditionsAreSatisfied(gmc, cc))
                    return branch;

            return null;
        }

        private void GoToNPCSpeech(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            mode = eConversationSceneMode.NPC_SPEAKING;
            timer = 0f;
            playerBranch = null;
            
            if (npcBranch.GetSoundEffect() != null)
                cc.AudioComponent.PlaySoundEffect_FireAndForget(npcBranch.GetSoundEffect());
            
            if (npcBranch.ActivationStageDirections.Count > 0)
                gmc.Player.LoadStageDirections(npcBranch.ActivationStageDirections, null, gtw, gmc, cc);

            npcBranch.OnActivate();

            createNPCSpeechUI = true;
        }
        private void GoToPlayerChoosingSpeech(GameManagerContainer gmc, ComponentContainer cc)
        {
            choosablePlayerBranches = npcBranch.PlayerBranches.FindAll(x => x.ConditionsAreSatisfied(gmc, cc));
            if (choosablePlayerBranches.Count == 0)
            {
                GoToWaitingForExit();
                return;
            }

            mode = eConversationSceneMode.PLAYER_CHOOSING_SPEECH;
            
            createPlayerSpeechChoiceUI = true;
        }
        private void GoToPlayerSpeech(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            mode = eConversationSceneMode.PLAYER_SPEAKING;
            timer = 0f;
            npcBranch = null;
            choosablePlayerBranches = null;

            if (playerBranch.GetSoundEffect() != null)
                cc.AudioComponent.PlaySoundEffect_FireAndForget(playerBranch.GetSoundEffect());

            if (playerBranch.ActivationStageDirections.Count > 0)
                gmc.Player.LoadStageDirections(playerBranch.ActivationStageDirections, null, gtw, gmc, cc);

            playerBranch.OnActivate();

            createPlayerSpeechUI = true;
        }
        private void GoToWaitingForExit()
        {
            mode = eConversationSceneMode.WAITING_FOR_EXIT;

            createExitUI = true;
        }
        private void GoToExiting(GameManagerContainer gmc, ComponentContainer cc)
        {
            mode = eConversationSceneMode.EXITING;
            if (!gmc.SceneManager.RequestSceneChange(returnSceneID))
                Debug.LogWarning("Conversation return scene could not be successfully requested!");

            clearAllUI = true;
        }

        private void CreateNPCSpeechUI()
        {
            ui.Clear();

            if (!string.IsNullOrEmpty(npcBranch.GetText()))
            {
                int maxWidth = Mathf.Ceiling(SPEECH_FRAME_WIDTH_FRACTION * Width);
                WrappedTextUI text = new WrappedTextUI(Point.Zero, npcBranch.GetText(), maxWidth, npcTextColor, spriteFont);
                text.SetPosition(new Point((Width / 2) - (text.Size.X / 2), Height - BOTTOM_GAP - text.Size.Y));
                ui.Add(text);
            }

            createNPCSpeechUI = false;
        }
        private void CreatePlayerSpeechChoiceUI()
        {
            ui.Clear();

            int x = Mathf.Round(Width * (1f - SPEECH_CHOICES_WIDTH_FRACTION) / 2f);
            int y = Height - PLAYER_SPEECH_OPTION_GAP;
            int width = Mathf.Round(Width * SPEECH_CHOICES_WIDTH_FRACTION);
            for (int i = choosablePlayerBranches.Count - 1; i >= 0; i--)
            {
                PlayerSpeechBranch choosableBranch = choosablePlayerBranches[i];
                SpeechOptionUI option = new SpeechOptionUI(new Point(x, y), width, choosableBranch, ChoosePlayerSpeechBranch, playerTextColor, spriteFont);
                ui.Add(option);

                y -= PLAYER_SPEECH_OPTION_GAP;
            }
            
            if (!string.IsNullOrEmpty(npcBranch.GetText()))
            {
                width = Mathf.Ceiling(SPEECH_FRAME_WIDTH_FRACTION * Width);
                WrappedTextUI text = new WrappedTextUI(Point.Zero, npcBranch.GetText(), width, npcTextColor, spriteFont);
                x = (Width / 2) - (text.Size.X / 2);
                text.SetPosition(new Point(x, y - text.Size.Y));
                ui.Add(text);
            }

            createPlayerSpeechChoiceUI = false;
        }
        private void CreatePlayerSpeechUI()
        {
            ui.Clear();

            if (!string.IsNullOrEmpty(playerBranch.GetText()))
            {
                int maxWidth = Mathf.Ceiling(SPEECH_FRAME_WIDTH_FRACTION * Width);
                WrappedTextUI text = new WrappedTextUI(Point.Zero, playerBranch.GetText(), maxWidth, playerTextColor, spriteFont);
                text.SetPosition(new Point((Width / 2) - (text.Size.X / 2), Height - BOTTOM_GAP - text.Size.Y));
                ui.Add(text);
            }

            createPlayerSpeechUI = false;
        }
        private void CreateExitUI()
        {
            ui.Clear();

            IconUI icon = new IconUI(new Point(Width / 2, Height - BOTTOM_GAP), eIconType.MOVE_DOWN, EXIT_ICON_SIZE, GoToExiting);
            ui.Add(icon);

            createExitUI = false;
        }

        private void ChoosePlayerSpeechBranch(PlayerSpeechBranch branch)
        {
            playerBranch = branch;
            playerBranchChosen = true;
        }
        
        /* -------- Static Methods -------- */
        public new static ConversationScene CreateFromData(SceneData data, Dictionary<string, SceneObject> sceneObjectDict, Dictionary<string, Animation> animationDict, GameManagerContainer gmc, ComponentContainer cc)
        {
            ObjectPopulatedSceneData opsd = data.ObjectPopulatedSceneData;
            ConversationSceneData csd = data.ConversationSceneData;

            if (opsd == null || csd == null)
                return null;

            ConversationScene scene = new ConversationScene(data.ID, data.Width, data.Height, opsd.LightColor, opsd.SkyColor, opsd.GroundColor, opsd.HorizonY, opsd.CloudTextureIDOrPath, opsd.CloudMovementSpeed, opsd.HorizonTextureIDOrPath, csd.ReturnSceneID, csd.ForcedFocalDepth, gmc, cc.GraphicsComponent);

            scene.conversationTree = ConversationTree.CreateFromData(csd.ConversationTree, gmc);

            // Add scene objects
            if (opsd.SceneObjectReferences != null)
            {
                foreach (SceneObjectReference sor in opsd.SceneObjectReferences)
                {
                    SceneObject so = SceneObject.CreateFromReference(sor, sceneObjectDict);
                    if (so != null)
                        scene.AddSceneObject(so);
                }
            }

            // Create scene objects
            if (opsd.SceneObjects != null)
            {
                foreach (SceneObjectData sod in opsd.SceneObjects)
                {
                    SceneObject so = SceneObject.CreateFromData(sod, animationDict, gmc, cc);
                    if (so != null)
                        scene.AddSceneObject(so);
                }
            }

            return scene;
        }
    }
}
