﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    /* -------- Enumerations -------- */
    enum eSceneType
    {
        CONTROL_SCENE,
        ADVENTURE_SCENE,
        CONVERSATION_SCENE,
        CUTSCENE,
        SYMBOL_DECODING_SCENE
    }
    
    abstract class Scene
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public float AspectRatio => Width / (float)Height;
        public float AnimationValue { get; private set; }
        public Rectangle ScreenRenderRectangle { get; private set; }

        /* -------- Private Fields -------- */
        protected List<OverlayUI> ui = new List<OverlayUI>();
        private List<StageDirection> activationStageDirections = new List<StageDirection>();
        private List<StageDirection> deactivationStageDirections = new List<StageDirection>();
        private SoundEffect ambientSoundEffect;
        private Texture2D pixTex;
        private float timeSinceBackgroundTap = float.MaxValue;

        /* -------- Constants -------- */
        private const float CURSOR_FADE_TIME = 1.5f;
        private const int INVENTORY_ITEM_SIZE = 100;
        private const int INVENTORY_GAP = 20;
        private const int TIMER_SIZE = 100;
        private const int TIMER_GAP = 20;
        private const int TIMER_BAR_WIDTH = 100;
        private const int TIMER_BAR_HEIGHT = 20;
        private readonly Color TIMER_BAR_COLOR = Color.DarkSlateGray;
        private readonly Color TIMER_BAR_FILL_COLOR = Color.Maroon;

        /* -------- Constructors -------- */
        public Scene(string inID, int inWidth, int inHeight, GraphicsComponent g)
        {
            ID = inID;
            Width = inWidth;
            Height = inHeight;

            AnimationValue = 0f;
            ScreenRenderRectangle = CreateScreenRenderRectangle(g);

            pixTex = ContentLoader.LoadTexture("Textures/WhitePixel");
        }

        /* -------- Public Methods -------- */
        public virtual void Update(float animation, bool isActiveScene, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (isActiveScene)
            {
                AnimationValue = animation;

                Point sceneMousePos = ConvertScreenPosToScenePos(cc.InputComponent.MousePos);
                foreach (OverlayUI oui in ui)
                    oui.Update(sceneMousePos, gtw, gmc, cc);

                if (ambientSoundEffect != null)
                    cc.AudioComponent.SetSoundEffectVolume(ambientSoundEffect, animation);

                if (gmc.SceneManager.MouseIsDownOnBackground(gmc.SceneMouseOverManager, true, cc.InputComponent))
                    timeSinceBackgroundTap = 0f;
            }

            timeSinceBackgroundTap += gtw.ElapsedSeconds;
        }
        public abstract void Draw(ComponentContainer cc);
        public abstract void SBDrawGeometry(GraphicsComponent g);
        public abstract void SBDrawBGGeometryDepth(GraphicsComponent g);
        public abstract void SBDrawGeometryDepth(GraphicsComponent g);
        public virtual void SBDrawUI(float uiAlpha, GameManagerContainer gmc, ComponentContainer cc)
        {
            foreach (OverlayUI oui in ui)
                oui.SBDrawUI(uiAlpha, gmc, cc);

            DrawInventory(uiAlpha, gmc, cc);
            DrawTimers(uiAlpha, gmc, cc);
            if (RendersScore())
                gmc.ScoreManager.SBDrawScore(uiAlpha, gmc, cc.GraphicsComponent);
            DrawCursor(uiAlpha, gmc, cc);
        }
        public abstract void DrawIndividualRenderTargets(ArtStyle artStyle, GraphicsComponent g);

        public abstract bool UsesRenderingPipeline();
        public abstract bool ForcesFocalDepth(ref float focalDepth);

        public Point ConvertScreenPosToScenePos(Point screenPos)
        {
            Point renderRectPos = screenPos - ScreenRenderRectangle.Location;
            float xF = (float)renderRectPos.X / ScreenRenderRectangle.Width;
            float yF = (float)renderRectPos.Y / ScreenRenderRectangle.Height;

            int sceneX = Mathf.Clamp(Mathf.Round(xF * Width), 0, Width - 1);
            int sceneY = Mathf.Clamp(Mathf.Round(yF * Height), 0, Height - 1);
            Point scenePos = new Point(sceneX, sceneY);

            return scenePos;
        }

        public virtual void OnBecomeActive(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (activationStageDirections.Count > 0)
                gmc.Player.LoadStageDirections(activationStageDirections, null, gtw, gmc, cc);

            if (ambientSoundEffect != null)
                cc.AudioComponent.PlaySoundEffect_Tracked(ambientSoundEffect, true);
        }
        public virtual void OnBecomeNonActive(GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (deactivationStageDirections.Count > 0)
                gmc.Player.LoadStageDirections(deactivationStageDirections, null, gtw, gmc, cc);

            if (ambientSoundEffect != null)
                cc.AudioComponent.StopSoundEffect(ambientSoundEffect);
        }
        public virtual void OnFinishFadingIn(GameManagerContainer gmc, ComponentContainer cc)
        {
        }
        public virtual void OnStartFadingOut(GameManagerContainer gmc, ComponentContainer cc)
        {
        }

        public Item GetMouseOverItem(Point sceneMousePos, GameManagerContainer gmc)
        {
            if (!RendersInventory())
                return null;

            int x = Width - INVENTORY_ITEM_SIZE - INVENTORY_GAP;
            int y = Height - INVENTORY_ITEM_SIZE - INVENTORY_GAP;

            foreach (Item item in gmc.Player.Inventory.Items)
            {
                Rectangle rect = new Rectangle(x, y, INVENTORY_ITEM_SIZE, INVENTORY_ITEM_SIZE);
                if (rect.Contains(sceneMousePos) && item is ActivatableItem)
                    return item;

                y -= INVENTORY_ITEM_SIZE + INVENTORY_GAP;
            }

            return null;
        }

        /* -------- Private Methods -------- */
        private Rectangle CreateScreenRenderRectangle(GraphicsComponent g)
        {
            if (AspectRatio == g.ScreenAspectRatio)
            {
                // No blinders necessary
                return new Rectangle(0, 0, g.ScreenWidth, g.ScreenHeight);
            }

            if (AspectRatio > g.ScreenAspectRatio)
            {
                // Blinders at top and bottom
                int insetFrameHeight = Mathf.Round(Height * (g.ScreenWidth / (float)Width));
                int blinderSize = (g.ScreenHeight - insetFrameHeight) / 2;
                return new Rectangle(0, blinderSize, g.ScreenWidth, insetFrameHeight);
            }
            else
            {
                // Blinders at the sides
                int insetFrameWidth = Mathf.Round(Width * (g.ScreenHeight / (float)Height));
                int blinderSize = (g.ScreenWidth - insetFrameWidth) / 2;
                return new Rectangle(blinderSize, 0, insetFrameWidth, g.ScreenHeight);
            }
        }

        protected abstract bool RendersCursor();
        protected abstract bool RendersInventory();
        protected abstract bool RendersTimers();
        protected abstract bool RendersScore();

        private void DrawCursor(float uiAlpha, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (!cc.SettingsComponent.ShowCursor)
                return;

            Texture2D cursorTex = gmc.UIContentManager.CursorTexture;
            if (RendersCursor() && cursorTex != null)
            {
                float touchScreenAlpha;
                if (cc.SettingsComponent.UseTouchScreen)
                    touchScreenAlpha = (this is ObjectPopulatedScene) ? 1f - Mathf.Clamp01(timeSinceBackgroundTap / CURSOR_FADE_TIME) : 0f;
                else
                    touchScreenAlpha = 1f;

                Point sceneMousePos = ConvertScreenPosToScenePos(cc.InputComponent.MousePos);
                Rectangle rect = new Rectangle(sceneMousePos.X - cursorTex.Width / 2, sceneMousePos.Y - cursorTex.Height / 2, cursorTex.Width, cursorTex.Height);
                Color tint = Color.White * uiAlpha * touchScreenAlpha;
                cc.GraphicsComponent.SpriteBatch.Draw(cursorTex, rect, tint);
            }
        }
        private void DrawInventory(float uiAlpha, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (!RendersInventory())
                return;

            int x = Width - INVENTORY_ITEM_SIZE - INVENTORY_GAP;
            int y = Height - INVENTORY_ITEM_SIZE - INVENTORY_GAP;

            foreach (Item item in gmc.Player.Inventory.Items)
            {
                Rectangle rect = new Rectangle(x, y, INVENTORY_ITEM_SIZE, INVENTORY_ITEM_SIZE);
                item.SBDraw(rect, uiAlpha, cc.GraphicsComponent.SpriteBatch);

                y -= INVENTORY_ITEM_SIZE + INVENTORY_GAP;
            }
        }
        private void DrawTimers(float uiAlpha, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (!RendersTimers())
                return;

            int x = TIMER_GAP;
            int y = Height - TIMER_GAP - TIMER_SIZE;

            foreach (Timer timer in gmc.TimerManager.Timers)
            {
                Rectangle rect = new Rectangle(x, y, INVENTORY_ITEM_SIZE, INVENTORY_ITEM_SIZE);
                timer.SBDrawTexture(rect, uiAlpha, cc.GraphicsComponent.SpriteBatch);

                int barX = x + TIMER_SIZE + TIMER_GAP;
                int barY = y + TIMER_SIZE / 2 - TIMER_BAR_HEIGHT / 2;

                Rectangle barRect = new Rectangle(barX, barY, TIMER_BAR_WIDTH, TIMER_BAR_HEIGHT);
                cc.GraphicsComponent.SpriteBatch.Draw(pixTex, barRect, TIMER_BAR_COLOR * uiAlpha);
                Rectangle barFillRect = new Rectangle(barRect.X, barRect.Y, Mathf.Round(barRect.Width * timer.DepletionFraction), barRect.Height);
                cc.GraphicsComponent.SpriteBatch.Draw(pixTex, barFillRect, TIMER_BAR_FILL_COLOR * uiAlpha);
            }
        }

        /* -------- Static Methods -------- */
        public static Scene CreateFromData(SceneData data, Dictionary<string, SceneObject> sceneObjectDict, Dictionary<string, Animation> animationDict, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (data == null)
                return null;

            eSceneType type = EnumUtil.Parse<eSceneType>(data.Type);

            Scene scene;
            switch (type)
            {
                case eSceneType.ADVENTURE_SCENE:
                    scene = AdventureScene.CreateFromData(data, sceneObjectDict, animationDict, gmc, cc);
                    break;
                case eSceneType.CONTROL_SCENE:
                    scene = ControlScene.CreateFromData(data, cc.GraphicsComponent);
                    break;
                case eSceneType.CONVERSATION_SCENE:
                    scene = ConversationScene.CreateFromData(data, sceneObjectDict, animationDict, gmc, cc);
                    break;
                case eSceneType.CUTSCENE:
                    scene = Cutscene.CreateFromData(data, sceneObjectDict, animationDict, gmc, cc);
                    break;
                case eSceneType.SYMBOL_DECODING_SCENE:
                    scene = SymbolDecodingScene.CreateFromData(data, gmc, cc);
                    break;
                default:
                    return null;
            }
            if (scene == null)
                return null;

            if (!string.IsNullOrEmpty(data.AmbientSoundEffectIDOrPath))
                scene.ambientSoundEffect = ContentLoader.LoadSoundEffect(data.AmbientSoundEffectIDOrPath);
            scene.activationStageDirections.AddRange(StageDirection.CreateFromData(data.ActivationStageDirections, gmc));
            scene.deactivationStageDirections.AddRange(StageDirection.CreateFromData(data.DeactivationStageDirections, gmc));

            return scene;
        }
    }
}
