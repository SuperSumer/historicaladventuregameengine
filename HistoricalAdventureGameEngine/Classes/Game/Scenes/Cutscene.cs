﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class Cutscene : ObjectPopulatedScene
    {
        /* -------- Private Fields -------- */
        private string nextSceneID;

        /* -------- Constructors -------- */
        public Cutscene(string inID, int inWidth, int inHeight, string inNextSceneID, Color inLightColor, Color inSkyColor, Color inGroundColor, int inHorizonY, string cloudTextureIDOrPath, int inCloudMovementSpeed, string horizonTextureIDOrPath, GraphicsComponent g)
            : base(inID, inWidth, inHeight, inLightColor, inSkyColor, inGroundColor, inHorizonY, cloudTextureIDOrPath, inCloudMovementSpeed, horizonTextureIDOrPath, g)
        {
            nextSceneID = inNextSceneID;
        }

        /* -------- Public Methods -------- */
        public override void Update(float animation, bool isActiveScene, GameTimeWrapper gtw, GameManagerContainer gmc, ComponentContainer cc)
        {
            if (isActiveScene)
            {
                // Skip cutscene if click the mouse or if stage directions finished
                if (cc.InputComponent.MouseButtonIsPressing(eMouseButton.LEFT) || !gmc.Player.IsRunningStageDirections())
                {
                    // Clear stage directions, stop sounds, and request the next scene
                    gmc.Player.EmptyStageDirections();
                    cc.AudioComponent.StopAllSoundEffects();
                    gmc.SceneManager.RequestSceneChange(nextSceneID);
                }
            }

            base.Update(animation, isActiveScene, gtw, gmc, cc);
        }

        public override bool ForcesFocalDepth(ref float focalDepth) => false;
        
        /* -------- Private Methods -------- */
        protected override bool RendersInventory() => false;
        protected override bool RendersTimers() => false;
        protected override bool RendersScore() => false;

        /* -------- Static Methods -------- */
        public new static Cutscene CreateFromData(SceneData data, Dictionary<string, SceneObject> sceneObjectDict, Dictionary<string, Animation> animationDict, GameManagerContainer gmc, ComponentContainer cc)
        {
            ObjectPopulatedSceneData opsd = data.ObjectPopulatedSceneData;
            CutsceneData cd = data.CutsceneData;
            if (opsd == null || cd == null)
                return null;

            Cutscene cutscene = new Cutscene(data.ID, data.Width, data.Height, cd.NextSceneID, opsd.LightColor, opsd.SkyColor, opsd.GroundColor, opsd.HorizonY, opsd.CloudTextureIDOrPath, opsd.CloudMovementSpeed, opsd.HorizonTextureIDOrPath, cc.GraphicsComponent);

            // Add scene objects
            if (opsd.SceneObjectReferences != null)
            {
                foreach (SceneObjectReference sor in opsd.SceneObjectReferences)
                {
                    SceneObject so = SceneObject.CreateFromReference(sor, sceneObjectDict);
                    if (so != null)
                        cutscene.AddSceneObject(so);
                }
            }

            // Create scene objects
            if (opsd.SceneObjects != null)
            {
                foreach (SceneObjectData sod in opsd.SceneObjects)
                {
                    SceneObject so = SceneObject.CreateFromData(sod, animationDict, gmc, cc);
                    if (so != null)
                        cutscene.AddSceneObject(so);
                }
            }

            return cutscene;
        }
    }
}
