﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace HistoricalAdventureGameEngine
{
    class UIContentManager
    {
        /* -------- Properties -------- */
        public Texture2D CursorTexture { get; private set; }
        public Texture2D InformationIconTexture { get; private set; }
        public Texture2D MoveIconTexture { get; private set; }
        public Texture2D PickUpIconTexture { get; private set; }
        public Texture2D TalkIconTexture { get; private set; }
        public Texture2D CuneiformIconTexture { get; private set; }
        public Texture2D InfoMessageTexture { get; private set; }
        public SpriteFont InfoMessageSpriteFont { get; private set; }
        public Color InfoMessageTextColor { get; private set; }
        public int InfoMessageBoxWidth { get; private set; }
        public int InfoMessageBoxHeight { get; private set; }
        public Texture2D InfoNodeTexture { get; private set; }
        public SpriteFont InfoNodeSpriteFont { get; private set; }
        public Color InfoNodeTextColor { get; private set; }
        public int InfoNodeBoxWidth { get; private set; }
        public int InfoNodeBoxHeight { get; private set; }
        public SoundEffect IconActivateSoundEffect { get; private set; }
        public SoundEffect RightChoiceSoundEffect { get; private set; }
        public SoundEffect WrongChoiceSoundEffect { get; private set; }
        public Texture2D ScoreIconTexture { get; private set; }
        public SpriteFont ScoreSpriteFont { get; private set; }

        /* -------- Public Methods -------- */
        public void LoadData(UIContentData data)
        {
            if (data == null)
                return;

            CursorTexture = ContentLoader.LoadTexture(data.CursorTextureIDOrPath);
            InformationIconTexture = ContentLoader.LoadTexture(data.InformationIconTextureIDOrPath);
            MoveIconTexture = ContentLoader.LoadTexture(data.MoveIconTextureIDOrPath);
            PickUpIconTexture = ContentLoader.LoadTexture(data.PickUpIconTextureIDOrPath);
            TalkIconTexture = ContentLoader.LoadTexture(data.TalkIconTextureIDOrPath);
            CuneiformIconTexture = ContentLoader.LoadTexture(data.CuneiformIconTextureIDOrPath);
            InfoMessageTexture = ContentLoader.LoadTexture(data.InfoMessageTextureIDOrPath);
            InfoMessageSpriteFont = ContentLoader.LoadSpriteFont(data.InfoMessageSpriteFontPath);
            InfoMessageTextColor = data.InfoMessageTextColor;
            InfoMessageBoxWidth = data.InfoMessageBoxWidth;
            InfoMessageBoxHeight = data.InfoMessageBoxHeight;
            InfoNodeTexture = ContentLoader.LoadTexture(data.InfoNodeTextureIDOrPath);
            InfoNodeSpriteFont = ContentLoader.LoadSpriteFont(data.InfoNodeSpriteFontPath);
            InfoNodeTextColor = data.InfoNodeTextColor;
            InfoNodeBoxWidth = data.InfoNodeBoxWidth;
            InfoNodeBoxHeight = data.InfoNodeBoxHeight;
            if (!string.IsNullOrEmpty(data.IconActivateSoundEffectIDOrPath))
                IconActivateSoundEffect = ContentLoader.LoadSoundEffect(data.IconActivateSoundEffectIDOrPath);
            if (!string.IsNullOrEmpty(data.RightChoiceSoundEffectIDOrPath))
                RightChoiceSoundEffect = ContentLoader.LoadSoundEffect(data.RightChoiceSoundEffectIDOrPath);
            if (!string.IsNullOrEmpty(data.WrongChoiceSoundEffectIDOrPath))
                WrongChoiceSoundEffect = ContentLoader.LoadSoundEffect(data.WrongChoiceSoundEffectIDOrPath);
            ScoreIconTexture = ContentLoader.LoadTexture(data.ScoreIconTextureIDOrPath);
            ScoreSpriteFont = ContentLoader.LoadSpriteFont(data.ScoreSpriteFontPath);
        }
    }
}
